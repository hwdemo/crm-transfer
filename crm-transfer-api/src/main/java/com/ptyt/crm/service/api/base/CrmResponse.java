package com.ptyt.crm.service.api.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CrmResponse <T> implements Serializable  {
    private static final long serialVersionUID = -3250310723115961929L;

    @ApiModelProperty(value = "响应吗")
    private int code;
    @ApiModelProperty(value = "响应信息")
    private String message;
    @ApiModelProperty(value = "是否成功")
    public boolean success = true;
    @ApiModelProperty(value = "响应数据")
    private T data;

    public static CrmResponse ok() {
        return new CrmResponse();
    }

    public CrmResponse<T> ok(T data) {
        CrmResponse<T> r = new CrmResponse<T>();
        r.setData(data);
        return r;
    }

    /**
     * 返回失败响应对象，echoCode设置为500
     *
     * @param echoMessage
     * @return
     */
    public static CrmResponse fail(String echoMessage) {
        CrmResponse r = new CrmResponse();
        r.setSuccess(false);
        r.setCode(500);
        r.setMessage(echoMessage);
        return r;
    }

    public static CrmResponse fail(int echoCode, String echoMessage) {
        if (echoCode == 0) {
            throw new IllegalArgumentException("echoCode不能为0");
        }
        CrmResponse r = new CrmResponse();
        r.setSuccess(false);
        r.setCode(echoCode);
        r.setMessage(echoMessage);
        return r;
    }
}
