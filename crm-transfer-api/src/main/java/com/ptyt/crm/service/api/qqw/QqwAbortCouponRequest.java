package com.ptyt.crm.service.api.qqw;

import java.io.Serializable;

import lombok.Data;

@Data
public class QqwAbortCouponRequest implements Serializable {
  private static final long serialVersionUID = -7531316766270886296L;
  private String couponNo;
}
