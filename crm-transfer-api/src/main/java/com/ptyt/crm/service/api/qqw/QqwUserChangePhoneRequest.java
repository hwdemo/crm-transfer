package com.ptyt.crm.service.api.qqw;

import java.io.Serializable;

import lombok.Data;

@Data
public class QqwUserChangePhoneRequest implements Serializable {
    private static final long serialVersionUID = -7531316766270886296L;
    private String offlineId;
    private String oldPhone;
    private String newPhone;
}
