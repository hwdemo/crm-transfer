package com.ptyt.crm.service.api.quartz;

import java.util.ArrayList;
import java.util.List;

public class QuatrzTaskInfoVoPaging {

	private int total;
	private List<QuartzTaskInfoVo> rows = new ArrayList();

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<QuartzTaskInfoVo> getRows() {
		return rows;
	}

	public void setRows(List<QuartzTaskInfoVo> rows) {
		this.rows = rows;
	}

}
