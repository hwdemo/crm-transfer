package com.ptyt.crm.service.api.demo;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Demo {
    private String id;
    private String tenantId;
}
