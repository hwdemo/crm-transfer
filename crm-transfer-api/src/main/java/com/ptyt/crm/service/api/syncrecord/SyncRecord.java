package com.ptyt.crm.service.api.syncrecord;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "同步成功记录")
public class SyncRecord {
    @ApiModelProperty(value = "同步类型")
    private String type;
    @ApiModelProperty(value = "业务关键字")
    private String businessKey;
    @ApiModelProperty(value = "同步内容")
    private String content;
    @ApiModelProperty(value = "同步时间")
    private String time;
}
