package com.ptyt.crm.service.api.qqw;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class QqwUserAcquireCouponRequest implements Serializable {
  private static final long serialVersionUID = -7531316766270886296L;
  private String couponNo;
  private Integer storeScopeType;
  private Integer commodityScopeType;
  private List<String> couponDetailNos;
  private String phone;
  private List<String> storeNos;
  private List<String> commodityNos;
}
