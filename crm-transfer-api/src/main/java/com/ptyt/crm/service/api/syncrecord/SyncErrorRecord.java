package com.ptyt.crm.service.api.syncrecord;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(description = "同步错误记录")
public class SyncErrorRecord {
    @ApiModelProperty(value = "唯一标识")
    private String id;
    @ApiModelProperty(value = "同步类型")
    private String type;
    @ApiModelProperty(value = "业务关键字")
    private String businessKey;
    @ApiModelProperty(value = "同步内容")
    private String content;
    @ApiModelProperty(value = "同步时间")
    private Date time;
    @ApiModelProperty(value = "同步失败原因")
    private String message;
    @ApiModelProperty(value = "重试次数")
    private int retries;
}
