package com.ptyt.crm.service.api.coupon;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "券映射")
public class CouponMapping {
  private String phone;
  private String sourceTplId;
  private String sourcePlatformId;
  private String sourceCode;
  private String targetTplId;
  private String targetPlatformId;
  private String targetCode;
  private String targetStatus;
  private Date created;
}
