package com.ptyt.crm.service.api.demo;

import com.ptyt.crm.service.api.base.CrmResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

@Api(tags = "demoApi")
@RequestMapping(value = "{tenant}/service/demo",
        produces = "application/json;charset=utf-8")
public interface DemoApi {
    @ApiOperation(value = "save")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    CrmResponse<String> save(@ApiParam(required = true) @PathVariable("tenant") String tenantId,
                             @ApiParam(required = true, value = "demo") @RequestBody Demo demo,
                             @ApiParam(required = true, value = "操作人") @RequestParam("operator") String operator);
}
