package com.ptyt.crm.service.api.quartz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Api(tags = "Quartz任务管理")
@RequestMapping(value = "{tenant}/quartzservice", produces = "application/json;charset=utf-8")
public interface QuartzJobApi {

  @ApiOperation(value = "获取JOB列表", notes = "获取JOB列表", produces = "application/json")
  @RequestMapping(value = "/query", method = RequestMethod.POST)
  public QuatrzTaskInfoVoPaging list(
          @ApiParam(required = true) @PathVariable("tenant") String tenantId, @RequestParam int page,
          @RequestParam(name = "page_size") int pageSize);

  @ApiOperation(value = "添加JOB", notes = "添加JOB", produces = "application/json")
  @RequestMapping(value = "/add", method = RequestMethod.POST)
  public void addJob(@ApiParam(required = true) @PathVariable("tenant") String tenantId,
                     @RequestParam(name = "jobName") String jobName,
                     @RequestParam(name = "jobClass") String jobClass,
                     @RequestParam(name = "cronExpression") String cronExpression,
                     @RequestParam(name = "jobDescription") String jobDescription) throws Exception;

  @ApiOperation(value = "修改JOB", notes = "修改JOB", produces = "application/json")
  @RequestMapping(value = "/modify", method = RequestMethod.POST)
  public void edit(@ApiParam(required = true) @PathVariable("tenant") String tenantId,
                   @RequestParam(name = "jobName") String jobName,
                   @RequestParam(name = "jobClass") String jobClass,
                   @RequestParam(name = "cronExpression") String cronExpression,
                   @RequestParam(name = "jobDescription") String jobDescription) throws Exception;

  @ApiOperation(value = "删除JOB", notes = "删除JOB", produces = "application/json")
  @RequestMapping(value = "/delete", method = RequestMethod.POST)
  public void delete(@ApiParam(required = true) @PathVariable("tenant") String tenantId,
                     @RequestParam(name = "jobName") String jobName) throws Exception;

  @ApiOperation(value = "暂停JOB", notes = "暂停JOB", produces = "application/json")
  @RequestMapping(value = "/pause", method = RequestMethod.POST)
  public void pause(@ApiParam(required = true) @PathVariable("tenant") String tenantId,
                    @RequestParam(name = "jobName") String jobName) throws Exception;

  @ApiOperation(value = "重启JOB", notes = "重启JOB", produces = "application/json")
  @RequestMapping(value = "/resume", method = RequestMethod.POST)
  public void resume(@ApiParam(required = true) @PathVariable("tenant") String tenantId,
                     @RequestParam(name = "jobName") String jobName) throws Exception;

  @ApiOperation(value = "执行JOB", notes = "执行JOB", produces = "application/json")
  @RequestMapping(value = "/trigger", method = RequestMethod.POST)
  public void trigger(@ApiParam(required = true) @PathVariable("tenant") String tenantId,
                      @RequestParam(name = "jobName") String jobName) throws Exception;

}
