package com.ptyt.crm.service.api.callback;

import com.ptyt.crm.service.api.base.CrmResponse;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Api(tags = "CallbackApi")
@RequestMapping(value = "{tenant}/service/callback",
        produces = "application/json;charset=utf-8")
public interface CallbackApi {

    @ApiOperation(value = "回调接口健康检查", produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", name = "request", value = "请求Body", required = false, dataType = "String")})
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    CrmResponse<Void> test(
            @ApiParam(required = true) @PathVariable("tenant") String tenantId, HttpServletRequest request);

    @ApiOperation(value = "接收回调接口")
    @RequestMapping(value = "/heading/crm/message", method = RequestMethod.POST)
    CrmResponse<Void> receiveMessage(
            @ApiParam(required = true) @PathVariable("tenant") String tenantId, HttpServletRequest request);

}
