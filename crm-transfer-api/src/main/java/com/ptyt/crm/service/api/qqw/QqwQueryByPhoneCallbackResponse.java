package com.ptyt.crm.service.api.qqw;

import lombok.Data;

@Data
public class QqwQueryByPhoneCallbackResponse {
  private String code = "0";
  private String message = "成功";

  private QqwQueryByPhoneCallbackData data;

  public static QqwQueryByPhoneCallbackResponse fail(String message2) {
    QqwQueryByPhoneCallbackResponse response = new QqwQueryByPhoneCallbackResponse();
    response.setCode("-1");
    response.setMessage(message2);
    return response;
  }
}
