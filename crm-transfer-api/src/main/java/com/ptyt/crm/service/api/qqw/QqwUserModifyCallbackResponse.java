package com.ptyt.crm.service.api.qqw;

import lombok.Data;

@Data
public class QqwUserModifyCallbackResponse {
  private String code = "0";
  private String message = "成功";

  private Boolean data = true;

  public static QqwUserModifyCallbackResponse fail(String message2) {
    QqwUserModifyCallbackResponse response = new QqwUserModifyCallbackResponse();
    response.setCode("-1");
    response.setMessage(message2);
    response.setData(false);
    return response;
  }
}
