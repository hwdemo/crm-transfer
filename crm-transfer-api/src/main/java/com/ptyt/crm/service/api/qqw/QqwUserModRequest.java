package com.ptyt.crm.service.api.qqw;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class QqwUserModRequest implements Serializable {
    private static final long serialVersionUID = -7531316766270886296L;
    private String offlineId;
    private String phone;
    private String nickName;
    private String fullName;
    private String sex;
    private Date birthday;
    private String email;
    private String province;
    private String city;
    private String county;
    private String address;
    private String logoUrl;
}
