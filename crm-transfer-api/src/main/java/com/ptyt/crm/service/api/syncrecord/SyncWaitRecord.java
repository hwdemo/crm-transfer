package com.ptyt.crm.service.api.syncrecord;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(description = "等待同步记录")
public class SyncWaitRecord {
  public static final String TYPE_MEMBER = "member";
  public static final String TYPE_SCORE = "score";
  public static final String TYPE_QQW_MEMBER = "qqw.member";
  public static final String TYPE_QQW_SCORE = "qqw.score";
  @ApiModelProperty(value = "唯一标识")
  private String id;
  @ApiModelProperty(value = "同步类型")
  private String type;
  @ApiModelProperty(value = "业务关键字")
  private String businessKey;
  @ApiModelProperty(value = "同步内容")
  private String content;
  @ApiModelProperty(value = "创建时间")
  private Date created;
  @ApiModelProperty(value = "重试次数")
  private Integer retries;
  @ApiModelProperty(value = "同步记录说明")
  private String remark;

}
