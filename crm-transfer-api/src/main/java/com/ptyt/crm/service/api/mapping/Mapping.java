package com.ptyt.crm.service.api.mapping;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "券映射")
public class Mapping {
  private String type;
  private String sourceType;
  private String sourceId;
  private String targetType;
  private String targetId;
  private Date created;
  private String extra;
}
