package com.ptyt.crm.service.api.qqw;

import java.util.Date;

import lombok.Data;

@Data
public class QqwQueryByPhoneCallbackData {
    private String offlineId;
    private String offlineMasterCardNo;
    private String phone;
    private String nickName;
    private String fullName;
    private String sex;
    private Date birthday;
    private String email;
    private String province;
    private String city;
    private String county;
    private String address;
    private String logoUrl;
    private Date registryTime;
}
