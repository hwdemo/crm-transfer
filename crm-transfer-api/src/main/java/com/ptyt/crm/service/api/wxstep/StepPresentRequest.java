package com.ptyt.crm.service.api.wxstep;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class StepPresentRequest implements Serializable {
  private static final long serialVersionUID = -7531316766270886296L;

  private String openId;
  private String nickName;
  private BigDecimal step;
  private Date time;
}
