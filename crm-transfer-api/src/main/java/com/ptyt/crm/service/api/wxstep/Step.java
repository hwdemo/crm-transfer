package com.ptyt.crm.service.api.wxstep;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "步数")
public class Step {
  private String tenantId;

  private String id;

  private String platformId;

  private String userId;
  private String userName;

  private BigDecimal step = BigDecimal.ZERO;

  private BigDecimal present;

  private Date time;
}
