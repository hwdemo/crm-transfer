package com.ptyt.crm.service.api.syncrecord;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(description = "同步结果记录")
public class SyncRecordResult {
  @ApiModelProperty(value = "记录ID")
  private String id;
  @ApiModelProperty(value = "业务关键字")
  private String businessKey;
  @ApiModelProperty(value = "同步时间")
  private Date time;
  @ApiModelProperty(value = "同步结果")
  private boolean success = true;
  @ApiModelProperty(value = "失败说明")
  private String message;
  @ApiModelProperty(value = "是否忽略")
  private boolean ignore = false;

  public SyncRecordResult(String id, String businessKey, Date time, boolean success,
      String message) {
    this.id = id;
    this.businessKey = businessKey;
    this.time = time;
    this.success = success;
    this.message = message;
  }
}
