package com.ptyt.crm.service.api.qqw;

import lombok.Data;

@Data
public class QqwCallbackResponse<T> {

  private String code = "0";
  private String message = "成功";

  private T data;
  // {
  // “code”: “0“,
  // “data”: {
  // “id”: 11001,
  // “phone”:”123566789”
  // },
  // “message”:”成功”
  // }
  //
  // code为0表示成功。
  // code为-1表示失败，通用的错误码。当需要某些错误定制逻辑时，会设计专用的错误码。
  // message为描述信息，会有失败的说明。
  // }
  
}
