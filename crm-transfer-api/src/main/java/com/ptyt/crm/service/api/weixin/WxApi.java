package com.ptyt.crm.service.api.weixin;

import com.ptyt.crm.service.api.base.CrmResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api(tags = "WxApi")
@RequestMapping(value = "{tenant}/service/wx", produces = "application/json;charset=utf-8")
public interface WxApi {

    @ApiOperation(value = "根据APP代码取得openId", produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "tenant", value = "租户代码", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "code", value = "APP代码", required = true, dataType = "String")})
    @RequestMapping(value = "/openId", method = RequestMethod.GET)
    public CrmResponse<WxUserInfo> getOpenId(@PathVariable("tenant") String aTenantId, String code)
            throws Exception;
}
