package com.ptyt.crm.service.api.qqw;

import lombok.Data;

@Data
public class QqwUserCreateCallbackResponse {
  private String code = "0";
  private String message = "成功";

  private QqwUserCreateCallbackData data;

  public static QqwUserCreateCallbackResponse fail(String message2) {
    QqwUserCreateCallbackResponse response = new QqwUserCreateCallbackResponse();
    response.setCode("-1");
    response.setMessage(message2);
    return response;
  }
}
