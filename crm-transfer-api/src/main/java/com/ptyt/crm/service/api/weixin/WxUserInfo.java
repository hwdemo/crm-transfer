package com.ptyt.crm.service.api.weixin;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WxUserInfo {
    private String openId;
    private String sessionKey;
    private String appId;
}
