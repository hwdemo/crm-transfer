package com.ptyt.crm.service.api.syncrecord;

import lombok.Data;

@Data
public class SyncRecordFilter {
  private String typeEq;
  private Integer retriesMax;
  private String businessKeyEq;
}
