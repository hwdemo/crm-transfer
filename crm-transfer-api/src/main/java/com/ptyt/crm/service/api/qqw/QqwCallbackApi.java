package com.ptyt.crm.service.api.qqw;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "CallbackApi")
@RequestMapping(value = "{tenant}/service/qqwcallback", produces = "application/json;charset=utf-8")
public interface QqwCallbackApi {

  @ApiOperation(value = "根据手机号查询用户信息")
  @RequestMapping(value = "/on2off/user/queryByPhone", method = RequestMethod.POST)
  QqwQueryByPhoneCallbackResponse queryByPhone(
      @ApiParam(required = true) @PathVariable("tenant") String tenantId,
      @RequestBody QqwQueryByPhoneRequest request) throws Exception;

  @ApiOperation(value = "创建新用户")
  @RequestMapping(value = "/on2off/user/create", method = RequestMethod.POST)
  QqwUserCreateCallbackResponse createUser(
      @ApiParam(required = true) @PathVariable("tenant") String tenantId,
      @RequestBody QqwUserCreateRequest request) throws Exception;

  @ApiOperation(value = "修改用户基本信息")
  @RequestMapping(value = "/on2off/user/modify", method = RequestMethod.POST)
  QqwUserModifyCallbackResponse modifyUser(
      @ApiParam(required = true) @PathVariable("tenant") String tenantId,
      @RequestBody QqwUserModRequest request) throws Exception;

  @ApiOperation(value = "修改用户手机号")
  @RequestMapping(value = "/on2off/user/changePhone", method = RequestMethod.POST)
  QqwUserModifyCallbackResponse changePhone(
      @ApiParam(required = true) @PathVariable("tenant") String tenantId,
      @RequestBody QqwUserChangePhoneRequest request) throws Exception;

  @ApiOperation(value = "用户领取卡券")
  @RequestMapping(value = "/on2off/coupon/received", method = RequestMethod.POST)
  QqwCallbackResponse acquireCoupon(
      @ApiParam(required = true) @PathVariable("tenant") String tenantId,
      @RequestBody QqwUserAcquireCouponRequest request) throws Exception;

  @ApiOperation(value = "作废卡券")
  @RequestMapping(value = "/on2off/coupon/forbidden", method = RequestMethod.POST)
  QqwCallbackResponse abortCoupon(
      @ApiParam(required = true) @PathVariable("tenant") String tenantId,
      @RequestBody QqwAbortCouponRequest request) throws Exception;
}
