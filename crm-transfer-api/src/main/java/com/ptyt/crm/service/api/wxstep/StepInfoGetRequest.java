package com.ptyt.crm.service.api.wxstep;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

@Data
public class StepInfoGetRequest implements Serializable {
  private static final long serialVersionUID = -7531316766270886296L;

  private String openId;
  private String nickName;
  private BigDecimal step;

}
