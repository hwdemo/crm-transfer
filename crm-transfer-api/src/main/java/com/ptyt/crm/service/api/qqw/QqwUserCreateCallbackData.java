package com.ptyt.crm.service.api.qqw;

import lombok.Data;

@Data
public class QqwUserCreateCallbackData {
  private String offlineId;
  private String offlineMasterCardNo;
}
