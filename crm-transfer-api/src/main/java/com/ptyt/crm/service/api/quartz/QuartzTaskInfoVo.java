package com.ptyt.crm.service.api.quartz;

import lombok.Data;

import java.util.Date;

@Data
public class QuartzTaskInfoVo {
  private String jobName;
  private String tenant;
  private String jobDescription;
  private String jobStatus;
  private String cronExpression;
  private String createTime;

  private Date previousFireTime;
  private Date nextFireTime;

}
