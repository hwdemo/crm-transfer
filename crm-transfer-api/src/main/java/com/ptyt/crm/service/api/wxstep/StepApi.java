package com.ptyt.crm.service.api.wxstep;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ptyt.crm.service.api.base.CrmResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "StepApi")
@RequestMapping(value = "{tenant}/service/wxstep", produces = "application/json;charset=utf-8")
public interface StepApi {

  @ApiOperation(value = "捐赠步数")
  @RequestMapping(value = "/present", method = RequestMethod.POST)
  CrmResponse present(@ApiParam(required = true) @PathVariable("tenant") String tenantId,
      @RequestBody StepPresentRequest request) throws Exception;

  @ApiOperation(value = "今日步数情况")
  @RequestMapping(value = "stepinfo", method = RequestMethod.POST)
  CrmResponse<StepInfo> getStepInfo(@ApiParam(required = true) @PathVariable("tenant") String tenantId,
      @RequestBody StepInfoGetRequest request) throws Exception;

}
