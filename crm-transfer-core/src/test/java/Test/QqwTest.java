package Test;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

import com.alibaba.fastjson.JSON;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwCreditRecord;

public class QqwTest {
 public static void main(String[] args) throws Exception {
    HeadingCrmServices services = new HeadingCrmServices();
    QqwServices qqwServices = new QqwServices();

    AuthInfo headinAuth = new AuthInfo();
    headinAuth.setTenantId("testbn01");
    headinAuth.getConfigs().put("userName", "guest");
    headinAuth.getConfigs().put("password", "guest");
    headinAuth.getConfigs().put("gatewayUrl", "https://mbr-test.qianfan123.com");

    // 会员查询
    // CrmResponse<Member> response = services.getMemberByMobile(headinAuth,
    // "18317022630");

    com.ptyt.crm.service.domain.adapter.commons.AuthInfo qqwAuthInfo = new com.ptyt.crm.service.domain.adapter.commons.AuthInfo();
    qqwAuthInfo.setTenantId("");
    // authInfo.setMock(true);
    // authInfo.getConfigs().put("channelNo", config.getChannelNo());
    qqwAuthInfo.getConfigs().put("gatewayUrl", "http://test-fresh-stdapi.quanqiuwa.com");
    qqwAuthInfo.getConfigs().put("appKey", "YITAIDAOJIA");
    qqwAuthInfo.getConfigs().put("secret",
        "xb%xcrxBrSXDqvtRcrAavG!adWq1WRtS5CWt2QWt$5BB$xed!r4RZWsDG#R@zB1f");
    // authInfo.getConfigs().put("systemNo", config.getSystemNo());
    // authInfo.getConfigs().put("tokenTime",
    // config.getTokenTime());d28a93ebdf9a4bc988c1ac94cf99570a
    // 1264717383554102bd7896661595614b
    // QqwToken aa=qqwServices.getToken(authInfo);
    // System.out.println(JSON.toJSONString(aa));
    // authInfo.getConfigs().put("token", "a4d880933ee548acadcd833684723b2c");
    // System.out.println(JSON.toJSONString(aa));
    qqwAuthInfo.getConfigs().put("token", "f889c548140d47038ca15f1dad942d7d");

    // member.setMemberId();
    // qqwServices.saveUser(qqwAuthInfo,response.getData());

    // 查询会员
    // QqwUser qqwMember = qqwServices.getUser(qqwAuthInfo, "18317022630");
    // System.out.println(JSON.toJSONString(qqwMember));

    Date startDate = DateUtils.addDays(new Date(), -7);
    int page = 1;
    int pageSize = 100;
    List<QqwCreditRecord> records = qqwServices.getCreditRecord(qqwAuthInfo, startDate, page,
        pageSize);

    System.out.println(JSON.toJSONString(records));

  }
}
