package Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import com.alibaba.fastjson.JSON;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.commons.Member;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmConfig;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Score;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.ScoreHst;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse;
import com.ptyt.crm.service.domain.adapter.qqw.QqwConfig;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwScore;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwUser;
import com.ptyt.crm.service.domain.adapter.util.AuthInfoUtil;

public class QqwTool {

  public static void main(String[] args) {
    QqwConfig qqwConfig = new QqwConfig();
    qqwConfig.setChannelNo(null);
    qqwConfig.setSystemNo(null);
    qqwConfig.setGatewayUrl("https://fresh-stdapi.quanqiuwa.com");
    qqwConfig.setClientId("YITAI");
    qqwConfig.setSecret("qV%xcS41BqWW4bbZZBz%BZcGZRZ!d5Srr3#V#dzSefXgDVrFcFtFbqEf4QVbRxfx");
    qqwConfig.setToken("424c38c0bfff4b8fa8784b46b0325e52");
    AuthInfo authInfo = AuthInfoUtil.buildAuthInfo("testbn01", qqwConfig);

    HeadingCrmConfig headingCrmConfig = new HeadingCrmConfig();
    headingCrmConfig.setGatewayUrl("https://api.u.hd123.com");
    headingCrmConfig.setUserName("putianyitai");
    headingCrmConfig.setPassword("R3PqSejkpntmJPs3mI5ONU0w");
    headingCrmConfig.setTenantId("araufywc");
    AuthInfo headingAuth = AuthInfoUtil.buildCrmAuthInfo(headingCrmConfig);
    HeadingCrmServices headingCrmServices = new HeadingCrmServices();

    QqwServices services = new QqwServices();

    // changeOfflineId(authInfo, services, headingAuth, headingCrmServices,
    // "15160468102");

    checkScore(authInfo, services, headingAuth, headingCrmServices);

  }

  public static void changeOfflineId(AuthInfo authInfo, QqwServices services, AuthInfo headingAuth,
      HeadingCrmServices headingCrmServices, String phone) {
    // 15605011827
    QqwUser member = services.getUser(authInfo, phone);

    CrmResponse<Member> headMbr = headingCrmServices.getMemberByMobile(headingAuth,
        member.getPhone());

    if (org.apache.commons.lang3.ObjectUtils.equals(member.getOfflineId(),
        headMbr.getData().getMemberId()) == false)
      services.updateUserOfflineId(authInfo, member, headMbr.getData().getMemberId());

  }

  public static void checkScore(AuthInfo authInfo, QqwServices services, AuthInfo headingAuth,
      HeadingCrmServices headingCrmServices) {
    // 核销券
    // String orderNo = String.valueOf(System.currentTimeMillis());
    // services.consumeCounpon(AuthInfoUtil.buildAuthInfo("testbn01",
    // qqwConfig), "YMUN358PQ0HSC",
    // orderNo, null);

    List<String> memberIds = read();
    List<String> memberIds2 = read();

    Map<String, String> error = new HashMap<String, String>();

    for (String memberId : memberIds) {
      CrmResponse<Member> memberObj = headingCrmServices.getMemberId(headingAuth, memberId);
      if (memberObj.getData() == null) {
        memberObj = headingCrmServices.getMemberByMobile(headingAuth, memberId);
      }

      CrmResponse<Score> hdscore = headingCrmServices.getScore(headingAuth,
          memberObj.getData().getMemberId());

      if (hdscore.isSuccess() == false || hdscore.getData() == null) {
        continue;
      }
      // 初始化全球蛙积分
      Member member = memberObj.getData();
      member.setMobile(member.getMobile());

      QqwUser qqwMember = services.getUser(authInfo, memberObj.getData().getMobile());
      if (qqwMember == null)
        continue;
      if (StringUtils.isBlank(qqwMember.getOfflineId())) {
        System.out.println("###### 线上会员：" + member.getMobile() + "的offlineId线下会员为空...");
        try {
          changeOfflineId(authInfo, services, headingAuth, headingCrmServices,
              memberObj.getData().getMobile());
          qqwMember = services.getUser(authInfo, memberObj.getData().getMobile());
        } catch (Exception e) {
          System.err.println(e.getMessage());
        }
        if (StringUtils.isBlank(qqwMember.getOfflineId())) {
          error.put(member.getMobile(), qqwMember == null ? "线上不存在会员"
              : "###### 线上会员：" + member.getMobile() + "的offlineId线下会员为空.../n");
          continue;
        }
      }

      QqwScore qqwScore = services.getScore(authInfo, qqwMember.getOfflineId());

      BigDecimal score = hdscore.getData().getBalance()
          .subtract(qqwScore == null ? BigDecimal.ZERO : qqwScore.getCurrentCredit());

      System.out.println("###### 会员：" + member.getMobile() + "线下积分："
          + hdscore.getData().getBalance() + "线上积分："
          + (qqwScore == null ? BigDecimal.ZERO : qqwScore.getCurrentCredit()) + "，校准积分：" + score);

      if (score.compareTo(BigDecimal.ZERO) == 0) {
        memberIds2.remove(memberId);
        continue;
      }

      try {
        if (qqwScore != null) {
          ScoreHst scoreHst = new ScoreHst();
          scoreHst.setAction("人工"); // OFFLINE_INTEGRAL_PRODUCE
          scoreHst.setBalance(null);
          scoreHst.setOccur(score);
          scoreHst.setOrderNo("CHECK" + DateFormatUtils.format(new Date(), "yyyyMMddHHmmss"));
          scoreHst.setOriginal(null);
          scoreHst.setRemark("线下积分校准");
          scoreHst.setTran_time(new Date());
          services.saveScore(authInfo, qqwMember.getOfflineId(), member, scoreHst);
        } else
          services.initialScore(authInfo, qqwMember.getOfflineId(), member, score);
        memberIds2.remove(memberId);
      } catch (Exception e) {
        System.err.println(e.getMessage());
        error.put(memberId, e.getMessage());
      }
    }

    System.out.println(JSON.toJSON(memberIds2));
    System.out.println(JSON.toJSON(error));
  }

  private static List<String> read() {
    List<String> storeIds = new ArrayList<String>();
    String[] str = {};
    File inFile = new File(
        "D:/Work/ideawork/crm-transfer/crm-transfer-core/src/test/java/test/member.txt");
    String inString = "";
    try {
      InputStreamReader isr = new InputStreamReader(new FileInputStream(inFile), "UTF-8");
      BufferedReader reader = new BufferedReader(isr);
      while ((inString = reader.readLine()) != null) {
        char[] c = inString.toCharArray();
        String[] value = new String[c.length];
        String result = "";
        for (int i = 0; i < c.length; i++) {
          value[i] = String.valueOf(c[i]);
          for (int j = 0; j < str.length; j++) {
            if (value[i].equals(str[j])) {
              String tmp = value[i];
              value[i] = "," + tmp + ",";
            }
          }
          result += value[i];
        }
        String[] r = result.split(",");
        storeIds.add(r[0]);
      }
      reader.close();
    } catch (FileNotFoundException ex) {
      System.out.println("没找到文件！");
    } catch (IOException ex) {
      System.out.println("读写文件出错！");
    }
    return storeIds;
  }
}
