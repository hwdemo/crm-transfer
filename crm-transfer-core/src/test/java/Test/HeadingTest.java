package Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.commons.Member;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmConfig;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.HeadingMember;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse;
import com.ptyt.crm.service.domain.adapter.util.AuthInfoUtil;

public class HeadingTest {
  public static void main(String[] args) throws Exception {
    HeadingCrmServices headingCrmServices = new HeadingCrmServices();

    HeadingCrmConfig headingCrmConfig = new HeadingCrmConfig();
    headingCrmConfig.setGatewayUrl("https://api.u.hd123.com");
    headingCrmConfig.setUserName("putianyitai");
    headingCrmConfig.setPassword("R3PqSejkpntmJPs3mI5ONU0w");
    headingCrmConfig.setTenantId("araufywc");
    AuthInfo headingAuth = AuthInfoUtil.buildCrmAuthInfo(headingCrmConfig);

    // headingCrmServices.getMemberByMobile(headingAuth, "15892055333");

    Member member = new Member();
    member.setMobile("15205932266");
    member.setName("");
    // member.setRegisterTime(1653474182000L);
    CrmResponse<HeadingMember> r = headingCrmServices.registerMemberByMobile(headingAuth, member);

    System.out.println(JSON.toJSON(r));

    // List<String> records = read();
    //
    // System.out.println(records);
    //
    // QqwConfig qqwConfig = new QqwConfig();
    // qqwConfig.setChannelNo(null);
    // qqwConfig.setSystemNo(null);
    // qqwConfig.setGatewayUrl("https://fresh-stdapi.quanqiuwa.com");
    // qqwConfig.setClientId("YITAI");
    // qqwConfig.setSecret("qV%xcS41BqWW4bbZZBz%BZcGZRZ!d5Srr3#V#dzSefXgDVrFcFtFbqEf4QVbRxfx");
    // qqwConfig.setToken("231c4fab5829458f9b11bbb1e052af93");
    // QqwServices services = new QqwServices();
    //
    // for (String r : records) {
    // JSONObject o = JSON.parseObject(r);
    // QqwUser qqwMember = services.getUser(AuthInfoUtil.buildAuthInfo("",
    // qqwConfig),
    // o.getString("phone"));
    //
    // if (StringUtils.isBlank(qqwMember.getOfflineId()))
    // continue;
    //
    // BigDecimal occer = o.getBigDecimal("credit");
    // if (Arrays.asList(5, 12, 26).contains(o.getString("integralObtainType")))
    // {
    // occer = occer.negate();
    // }
    //
    // headingCrmServices.adjustScore(headingAuth, qqwMember.getOfflineId(),
    // o.getString("flowNo"),
    // new Date(o.getLongValue("occurrenceTime")), occer);
    //
    // }

  }

  private static List<String> read() {
    List<String> storeIds = new ArrayList<String>();
    String[] str = {};
    File inFile = new File(
        "D:/Work/ideawork/crm-transfer/crm-transfer-core/src/test/java/test/qqwscore.txt");
    String inString = "";
    try {
      InputStreamReader isr = new InputStreamReader(new FileInputStream(inFile), "UTF-8");
      BufferedReader reader = new BufferedReader(isr);
      while ((inString = reader.readLine()) != null) {
        char[] c = inString.toCharArray();
        String[] value = new String[c.length];
        String result = "";
        for (int i = 0; i < c.length; i++) {
          value[i] = String.valueOf(c[i]);
          for (int j = 0; j < str.length; j++) {
            if (value[i].equals(str[j])) {
              String tmp = value[i];
              value[i] = "," + tmp + ",";
            }
          }
          result += value[i];
        }
        String[] r = result.split(",");
        storeIds.add(result);
      }
      reader.close();
    } catch (FileNotFoundException ex) {
      System.out.println("没找到文件！");
    } catch (IOException ex) {
      System.out.println("读写文件出错！");
    }
    return storeIds;
  }
}
