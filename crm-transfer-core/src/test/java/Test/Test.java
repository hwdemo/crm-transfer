package Test;

import java.util.Arrays;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.commons.Member;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Coupon;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.CouponActivity;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Score;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.ScoreHst;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponGetByStateRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponQueryActivityRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponQueryRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.ScoreHstRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;

public class Test {
  public static void main(String[] args) throws Exception {
    HeadingCrmServices services = new HeadingCrmServices();
    QqwServices qqwServices = new QqwServices();

    AuthInfo headinAuth = new AuthInfo();
    headinAuth.setTenantId("testbn01");
    headinAuth.getConfigs().put("userName", "guest");
    headinAuth.getConfigs().put("password", "guest");
    headinAuth.getConfigs().put("gatewayUrl", "https://mbr-test.qianfan123.com");

    // 会员查询
    CrmResponse<Member> response = services.getMemberByMobile(headinAuth, "18317022630");
    System.out.println(JSON.toJSONString(response));

    // qqwServices.saveUser(authInfo, response.getData());

    // 积分
    CrmResponse<Score> score = services.getScore(headinAuth, response.getData().getMobile());
    System.out.println(JSON.toJSONString(score));
//    // 积分同步
//    ScoreHstRequest scoreHstRequest = new ScoreHstRequest();
//    scoreHstRequest.setMember_id(response.getData().getMemberId());
//    CrmResponse<List<ScoreHst>> scoreHst = services.getScoreHsts(headinAuth, scoreHstRequest);
//    for (ScoreHst hst : scoreHst.getData()) {
//      // qqwServices.saveScore(authInfo, response.getData(), hst);
//    }
//    System.out.println(JSON.toJSONString(scoreHst));

//    CouponQueryActivityRequest activityRequest = new CouponQueryActivityRequest();
//    // activityRequest.setActivity_id("202112050004");
//    activityRequest.setPlatform_id("03110501");
//    CrmResponse<List<CouponActivity>> act = services.queryCouponActivity(headinAuth,
//        activityRequest);
//    System.out.println(JSON.toJSONString(act));

    // // 领券
    // String memberId = response.getData().getMemberId();
    // CouponAcquireRequest acquireRequest = new CouponAcquireRequest();
    // acquireRequest.setActivity_id("202112050001");
    // acquireRequest.setMember_id(memberId);
    // acquireRequest.setTran_id(memberId + "2021120501");
    // CrmResponse<Coupon> coupon = services.acquireCoupon(headinAuth,
    // acquireRequest);
    // System.out.println(JSON.toJSONString(coupon));
    // 查询会员券
//    CouponQueryRequest couponRequest = new CouponQueryRequest();
//    couponRequest.setMember_id(response.getData().getMemberId());
//    couponRequest.setActivity_ids(Arrays.asList("202112050001"));
//    CrmResponse<List<Coupon>> coupons = services.queryCoupon(headinAuth, couponRequest);
//    System.out.println(JSON.toJSONString(coupons));
//
//    CouponGetByStateRequest couponGetByStateRequest = new CouponGetByStateRequest();
//    couponGetByStateRequest.setMember_id(response.getData().getMemberId());
//    coupons = services.getMemberCoupons(headinAuth, couponGetByStateRequest);
//    System.out.println(JSON.toJSONString(coupons));

    // // 查询券详情
    // CouponGetRequest gets = new CouponGetRequest();
    // gets.setCoupon_codes(Arrays.asList(coupons.getData().get(0).getCode()));
    // coupons = services.getCoupons(headinAuth, gets);
    // System.out.println(JSON.toJSONString(coupons));

  }
}
