package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberModifyResponse extends HeadingBaseResponse<Object>{
}
