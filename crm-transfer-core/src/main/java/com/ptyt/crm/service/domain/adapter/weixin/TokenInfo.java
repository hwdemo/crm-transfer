package com.ptyt.crm.service.domain.adapter.weixin;

public class TokenInfo {
  private String accessToken;// string 获取到的凭证
  private Integer expiresIn;// number 凭证有效时间，单位：秒。目前是7200秒之内的值。

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public Integer getExpiresIn() {
    return expiresIn;
  }

  public void setExpiresIn(Integer expiresIn) {
    this.expiresIn = expiresIn;
  }
}
