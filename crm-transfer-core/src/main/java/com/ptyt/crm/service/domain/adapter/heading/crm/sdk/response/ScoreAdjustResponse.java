package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ScoreAdjustResponse extends HeadingBaseResponse<Object>{
  
}
