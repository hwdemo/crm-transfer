package com.ptyt.crm.service.domain.dao.tenant;

import com.ptyt.crm.service.domain.dao.base.BaseDao;
import com.ptyt.crm.service.domain.dao.config.PConfig;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class TenantDao extends BaseDao {
    public List<PTenant> getAll() {
        String hql = "from " + PTenant.class.getName();
        Query query = em.createQuery(hql);
        return query.getResultList();
    }
}
