package com.ptyt.crm.service.domain.adapter.qqw.sdk.client;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwBaseRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwBaseResponse;
import com.ptyt.crm.service.domain.util.UnicodeUtil;
import com.ptyt.crm.service.domain.util.http.HttpResult;
import com.ptyt.crm.service.domain.util.http.HttpsClient;

import io.swagger.models.HttpMethod;

public class QqwCrmClient {
  private static Logger logger = LoggerFactory.getLogger(QqwCrmClient.class);

  private String gatewayUrl = "http://standard-api.quanqiuwa.com";
  private String appKey;
  private String systemNo;
  private String tokenTime;
  private String token;

  public QqwCrmClient(String url, String token) {
    if (url != null)
      this.gatewayUrl = url;
    this.token = token;
  }

  public QqwCrmClient(String url, String appKey, String systemNo, String tokenTime) {
    if (url != null)
      this.gatewayUrl = url;
    this.appKey = appKey;
    this.systemNo = systemNo;
    this.tokenTime = tokenTime;
  }

  public <T extends QqwBaseResponse> T excute2(QqwBaseRequest<T> request) throws Exception {
    request.check();
    T response = null;
    String requestBody = request.getBody() == null ? "{}"
        : JSONObject.toJSONString(request.getBody());
    try {
      HttpsClient httpsClient = new HttpsClient();
      Long clientTime = System.currentTimeMillis();

      // header 参数
      HashMap<String, String> headers = new HashMap<>();
      headers.put("Authorization", "Bearer " + token);
      headers.put("Content-Type", "application/json");
      HttpResult resp = null;
      if (HttpMethod.GET.equals(request.getHttpRequestMethod())) {
        String url = gatewayUrl + request.getApiMethod();
        if (request.getQueryParam().isEmpty() == false) {
          StringBuffer queryParams = new StringBuffer();
          queryParams.append("?");
          for (String key : request.getQueryParam().keySet()) {
            queryParams.append(key).append("=").append(request.getQueryParam().get(key));
            queryParams.append("&");
          }
          // String s =
          // queryParams.toString().substring(0,queryParams.toString().Length -
          // 1)
          url = url + queryParams.toString();
        }
        resp = httpsClient.doGet(url, headers);
      } else if (HttpMethod.POST.equals(request.getHttpRequestMethod())) {
        String url = gatewayUrl + request.getApiMethod();
        resp = httpsClient.doPost(url, requestBody, headers);
      }

      String responseBody = resp.getBody();

      JSONObject json = JSONObject.parseObject(responseBody);

      print(request.getApiMethod(), requestBody, responseBody);

      response = (T) JSONObject.toJavaObject(json, request.getResponseClass());

      return response;

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      throw new Exception(e.getMessage(), e);
    }
  }

  public <T extends QqwBaseResponse> T excute(QqwBaseRequest<T> request) throws Exception {
    request.check();
    T response = null;
    String requestBody = request.getBody() == null ? "{}"
        : JSONObject.toJSONString(request.getBody());
    try {
      HttpsClient httpsClient = new HttpsClient();
      Long clientTime = System.currentTimeMillis();
      String token = QqwUtils.generate(appKey, systemNo, tokenTime, clientTime);

      // header 参数
      HashMap<String, String> headers = new HashMap<>();
      headers.put("open-api-token", token);
      headers.put("system-no", systemNo);
      headers.put("system-data", clientTime.toString());

      HttpResult resp = null;
      if (HttpMethod.GET.equals(request.getHttpRequestMethod())) {
        String url = gatewayUrl + request.getApiMethod();
        if (request.getQueryParam().isEmpty() == false) {
          StringBuffer queryParams = new StringBuffer();
          queryParams.append("?");
          for (String key : request.getQueryParam().keySet()) {
            queryParams.append(key).append("=").append(request.getQueryParam().get(key));
            queryParams.append("&");
          }
          url = url + queryParams.toString();
        }
        resp = httpsClient.doGet(url, headers);
      } else if (HttpMethod.POST.equals(request.getHttpRequestMethod())) {
        String url = gatewayUrl + request.getApiMethod();
        resp = httpsClient.doPost(url, requestBody, headers);
      }

      String responseBody = resp.getBody();

      JSONObject json = JSONObject.parseObject(responseBody);

      print(request.getApiMethod(), requestBody, responseBody);

      response = (T) JSONObject.toJavaObject(json, request.getResponseClass());

      return response;

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      throw new Exception(e.getMessage(), e);
    }
  }

  private void print(String apiMethod, String requestBody, String reponse) {
    StringBuffer sb = new StringBuffer();
    sb.append("\r\n====================\r\n");
    sb.append("apiMethod：\r\n");
    sb.append("内容：").append(apiMethod).append("\r\n");
    sb.append("--------------------\r\n");
    sb.append("request：\r\n");
    sb.append("内容：").append(requestBody).append("\r\n");
    sb.append("--------------------\r\n");
    sb.append("response：\r\n");
    sb.append("内容：").append(UnicodeUtil.unicodeDecode(reponse)).append("\r\n");
    sb.append("====================\r\n");
    logger.info(sb.toString());
    System.out.println(sb.toString());
  }

}
