package com.ptyt.crm.service.domain.core.config;

import com.ptyt.crm.service.domain.dao.config.ConfigDao;
import com.ptyt.crm.service.domain.dao.config.PConfig;
import com.ptyt.crm.service.domain.dao.config.PConfigLog;
import com.ptyt.crm.service.domain.util.DefaultSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ConfigService {
    private final static String ENV = "default";
    private final static String APP_NAME = "com.ptyt.crm";

    public static final String ALL = "*";

    @Autowired
    private ConfigDao configDao;

    public <T> void put(String tenantId, String name, T config) {
        Map<String, String> configs = DefaultSerializer.getInstance().serialize(config);
        saveConfig(ENV, APP_NAME, null, tenantId, name, configs);
    }

    public <T> T getConfig(String tenantId, String name, Class<T> clazz) {
        if (tenantId == null) {
            tenantId = ALL;
        }
        List<PConfig> configs = new ArrayList();
        if (tenantId == null)
            configs = configDao.queryConfigByTenantIdIsNull(name);
        else
            configs = configDao.findByTenantIdAndName(tenantId, name);
        Map<String, String> result = new HashMap<String, String>();
        for (PConfig c : configs) {
            result.put(c.getFkey(), c.getData());
        }
        T config = DefaultSerializer.getInstance().deserialize(result, clazz);
        return config;
    }


    public void saveConfig(String env, String appName, String appVersion, String tenantId, String name,
                           Map<String, String> configs) {
        if (tenantId == null) {
            tenantId = ALL;
        }
        List<PConfig> configList = new ArrayList<>(configs.size());
        for (Map.Entry<String, String> entry : configs.entrySet()) {
            PConfig c = new PConfig();
            c.setTenantId(tenantId);
            c.setName(name);
            c.setFkey(entry.getKey());
            c.setData(entry.getValue());
            c.setModified(new Date());
            configList.add(c);
        }
        // 记录日志
        PConfigLog log = new PConfigLog();
        log.setEnv(env);
        log.setAppName(appName);
        log.setName(name);
        StringBuilder sb = new StringBuilder().append("appVersion: ").append(appVersion).append(", tenantId: ")
                .append(tenantId);
        for (Map.Entry<String, String> entry : configs.entrySet()) {
            sb.append(", ").append(entry.getKey()).append("=").append(entry.getValue());
        }
        String message = sb.toString();
        if (message != null && message.length() > 1024)
            message = message.substring(1, 1024);
        log.setMessage(message);
        log.setFtime(new Date());

        configDao.save(tenantId, name, configList, log);
    }
}
