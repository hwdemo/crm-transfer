package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponAbortResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponAbortRequest implements HeadingBaseRequest<CouponAbortResponse> {
  private String coupon_code;

  @Override
  public String getApiMethod() {
    return "/crm/couponservice/coupon/abort";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("coupon_code", coupon_code);
    return query;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<CouponAbortResponse> getResponseClass() {
    return CouponAbortResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    return query;
  }

}
