package com.ptyt.crm.service.domain.dao.config;

import com.ptyt.crm.service.domain.dao.base.PEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "ct_config", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "tenant_id", "name", "fkey" }) }, indexes = {
				@Index(name = "index_ct_config_1", columnList = "name") })
public class PConfig extends PEntity {

	private static final long serialVersionUID = 4518033052010019177L;
	private String tenantId;
	private String name;
	private String fkey;
	private String data;
	private Date modified;

	/** 租户ID */
	@Column(name = "tenant_id", length = 128, nullable = false)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/** 配置名 */
	@Column(name = "name", length = 128, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/** 配置项名 */
	@Column(name = "fkey", length = 255, nullable = false)
	public String getFkey() {
		return fkey;
	}

	public void setFkey(String fkey) {
		this.fkey = fkey;
	}

	/** 配置项值 */
	@Column(name = "data", length = 2048)
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	/** 修改时间 */
	@Column(name = "modified", nullable = false)
	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

}
