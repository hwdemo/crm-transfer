package com.ptyt.crm.service.domain.adapter.qqw.sdk.response;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwUser;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwOff2onUserGetResponse extends QqwBaseResponse<QqwUser>{
}
