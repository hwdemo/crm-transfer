package com.ptyt.crm.service.domain.core.weixin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ptyt.crm.service.api.weixin.WxUserInfo;
import com.ptyt.crm.service.domain.adapter.weixin.WeiXinServices;
import com.ptyt.crm.service.domain.adapter.weixin.sdk.response.WxGetUserResponse;
import com.ptyt.crm.service.domain.core.config.ConfigService;

@Service
public class WxService {
    @Autowired
    WeiXinServices weiXinServices;
    @Autowired
    ConfigService configService;

    public WxUserInfo getOpenId(String tenantId, String code) throws Exception {
        WxConfig wxConfig = configService.getConfig(tenantId, WxConfig.NAME, WxConfig.class);
        if (wxConfig == null || wxConfig.getAppId() == null) {
            return null;
        }
        WxGetUserResponse response=weiXinServices.getOpenId(wxConfig.getAppId(), wxConfig.getSecret(), code);
        WxUserInfo info=new WxUserInfo();
        info.setAppId(wxConfig.getAppId());
        info.setOpenId(response.getOpenid());
        info.setSessionKey(response.getSessionKey());
        return info;
    }
}
