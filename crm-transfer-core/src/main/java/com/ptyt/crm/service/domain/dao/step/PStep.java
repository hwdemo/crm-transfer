package com.ptyt.crm.service.domain.dao.step;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.ptyt.crm.service.domain.dao.base.PBaseEntity;

@Entity
@Table(name = "ct_step", uniqueConstraints = {
    @UniqueConstraint(columnNames = { "tenant_id", "id"}) },indexes = {
    @Index(name = "index_ct_step_1", columnList = "tenant_id,user_id"),
    @Index(name = "index_ct_step_2", columnList = "time") })
public class PStep extends PBaseEntity {
  private static final long serialVersionUID = 2493200163960991791L;

  private String tenantId;
  
  private String id;

  private String platformId;

  private String userId;
  private String userName;

  private BigDecimal step;

  private BigDecimal present;

  private Date time;

  /**
   * 租户
   */
  @Column(name = "tenant_id", length = 20, nullable = false)
  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }
  
  
  @Column(name = "id", length = 128, nullable = false)
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Column(name = "platform_id", length = 64, nullable = false)
  public String getPlatformId() {
    return platformId;
  }

  public void setPlatformId(String platformId) {
    this.platformId = platformId;
  }

  @Column(name = "user_id", length = 64, nullable = false)
  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  @Column(name = "user_name", length = 128, nullable = true)
  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  @Column(name = "step",nullable=false)
  public BigDecimal getStep() {
    return step;
  }

  public void setStep(BigDecimal step) {
    this.step = step;
  }

  public BigDecimal getPresent() {
    return present;
  }

  public void setPresent(BigDecimal present) {
    this.present = present;
  }

  @Column(name = "time")
  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

}
