package com.ptyt.crm.service.domain.dao.config;

import java.util.List;

import javax.persistence.Query;

import com.ptyt.crm.service.domain.dao.base.BaseDao;
import com.ptyt.crm.service.domain.dao.base.EntityQuery;
import com.ptyt.crm.service.domain.dao.base.TX;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;


@Repository
public class ConfigDao extends BaseDao {

  public List<PConfig> findByTenantIdAndName(String tenantId, String name) {
    String hql = "from " + PConfig.class.getName()
        + " o where o.tenantId =:tenantId and o.name=:name";
    Query query = em.createQuery(hql);
    query.setParameter("tenantId", tenantId);
    query.setParameter("name", name);
    return query.getResultList();
  }

  public List<PConfig> queryConfigByTenantIdIsNull(String name) {
    String hql = "from " + PConfig.class.getName() + " o where o.tenantId is null and o.name=:name";
    Query query = em.createQuery(hql);
    query.setParameter("name", name);
    return query.getResultList();
  }

  public List<String> getTenantIds() {
    String hql = "select distinct o.tenantId from " + PConfig.class.getName()
        + " o  where o.tenantId is not null";
    Query query = em.createQuery(hql);
    return query.getResultList();
  }

  public List<PConfig> gets(String tenantId, List<String> names) {
    EntityQuery q = new EntityQuery("gets").from(PConfig.class.getName(), "o");

    if (StringUtils.isNotEmpty(tenantId)) {
      q.where("o.tenantId = :tenantId").p("tenantId", tenantId);
    } else {
      q.where("o.tenantId is null");
    }
    if (CollectionUtils.isNotEmpty(names)) {
      q.where("o.name in (:names)").p("names", names);
    }
    return q.list(em);
  }

  @TX
  public void save(String tenantId, String name, List<PConfig> configList, PConfigLog configLog) {
    String hql = "delete from " + PConfig.class.getName() + " o where o.name = :name";
    if (StringUtils.isNotEmpty(tenantId)) {
      hql += " and o.tenantId = :tenantId";
    } else {
      hql += " and o.tenantId is null";
    }
    Query query = em.createQuery(hql);
    query.setParameter("name", name);
    if (StringUtils.isNotEmpty(tenantId)) {
      query.setParameter("tenantId", tenantId);
    }
    query.executeUpdate();
    configList.forEach(config -> em.persist(config));
    em.persist(configLog);
    em.flush();
  }
}
