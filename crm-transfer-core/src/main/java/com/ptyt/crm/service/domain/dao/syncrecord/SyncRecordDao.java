package com.ptyt.crm.service.domain.dao.syncrecord;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.ptyt.crm.service.api.syncrecord.SyncRecordFilter;
import com.ptyt.crm.service.domain.dao.base.BaseDao;
import com.ptyt.crm.service.domain.dao.base.EntityQuery;
import com.ptyt.crm.service.domain.dao.base.TX;

@Repository
public class SyncRecordDao extends BaseDao {
  @TX
  public void saveSyncRecord(List<PSyncRecord> records) {
    for (PSyncRecord record : records) {
      em.persist(record);
    }
    em.flush();
  }

  @TX
  public void saveSyncWaitRecord(PSyncWaitRecord record) {
    record.setCreated(new Date());
    em.persist(record);
    em.flush();
  }

  @TX
  public void saveSyncWaitRecords(List<PSyncWaitRecord> records) {
    for (PSyncWaitRecord record : records) {
      em.persist(record);
    }
    em.flush();
  }

  @TX
  public void saveSyncErrorRecord(List<PSyncErrorRecord> records) {
    for (PSyncErrorRecord record : records) {
      if (record.getUuid() == null)
        em.persist(record);
      else {
        em.merge(record);
      }
    }
    em.flush();
  }

  public List<PSyncWaitRecord> queryWaitRecord(String tenantId, SyncRecordFilter filter, int page,
      int pageSize, String sortKey, boolean desc) {
    EntityQuery q = getQueryWaitRecord("query", tenantId, filter, sortKey, desc);
    List<PSyncWaitRecord> templates = q.list(em, page, pageSize);
    if (templates.isEmpty())
      return new ArrayList<>();
    List<PSyncWaitRecord> result = new ArrayList<>();
    for (PSyncWaitRecord m : templates) {
      result.add(m);
    }
    return result;
  }

  private EntityQuery getQueryWaitRecord(String name, String tenantId, SyncRecordFilter filter,
      String sortKey, boolean desc) {
    EntityQuery q = new EntityQuery(name).from(PSyncWaitRecord.class.getName(), "s");
    q.where("s.tenantId =:tenantId").p("tenantId", tenantId);
    if (StringUtils.isNotEmpty(filter.getTypeEq())) {
      q.where("s.type =:type").p("type", filter.getTypeEq());
    }
    if (sortKey != null) {
      if (desc) {
        q.orderBy(sortKey, "desc");
      } else {
        q.orderBy(sortKey, "asc");
      }
    }
    return q;
  }

  public List<PSyncErrorRecord> queryErrorRecord(String tenantId, SyncRecordFilter filter, int page,
      int pageSize, String sortKey, boolean desc) {
    EntityQuery q = getQueryErrorRecord("query", tenantId, filter, sortKey, desc);
    List<PSyncErrorRecord> templates = q.list(em, page, pageSize);
    if (templates.isEmpty())
      return new ArrayList<>();
    List<PSyncErrorRecord> result = new ArrayList<>();
    for (PSyncErrorRecord m : templates) {
      result.add(m);
    }
    return result;
  }

  private EntityQuery getQueryErrorRecord(String name, String tenantId, SyncRecordFilter filter,
      String sortKey, boolean desc) {
    EntityQuery q = new EntityQuery(name).from(PSyncErrorRecord.class.getName(), "s");
    q.where("s.tenantId =:tenantId").p("tenantId", tenantId);
    if (StringUtils.isNotEmpty(filter.getTypeEq())) {
      q.where("s.type =:type").p("type", filter.getTypeEq());
    }
    if (filter.getRetriesMax() != null) {
      q.where("s.retries <=:retriesMax").p("retriesMax", filter.getRetriesMax());
    }
    if (filter.getBusinessKeyEq() != null) {
      q.where("s.businessKey =:businessKey").p("businessKey", filter.getBusinessKeyEq());
    }
    q.orderBy("retries", "asc");
    if (sortKey != null) {
      if (desc) {
        q.orderBy(sortKey, "desc");
      } else {
        q.orderBy(sortKey, "asc");
      }
    }
    return q;
  }

  @TX
  public void removeErrorRecord(List<String> uuids) {
    em.createQuery("delete from " + PSyncErrorRecord.class.getName() + " o where o.uuid in :uuids")
        .setParameter("uuids", uuids).executeUpdate();
    em.flush();
  }

  public PSyncRecord get(String tenantId, String type, String businessKey) {
    EntityQuery q = new EntityQuery("query").from(PSyncRecord.class.getName(), "o");
    q.where("o.tenantId= :tenantId").p("tenantId", tenantId);
    q.where("o.type= :type").p("type", type);
    q.where("o.businessKey= :businessKey").p("businessKey", businessKey);
    List<PSyncRecord> records = q.list(em, 0, 1);
    return records.isEmpty() ? null : records.get(0);
  }

  public PSyncErrorRecord getErrorRecordById(String tenantId, String type, String id) {
    EntityQuery q = new EntityQuery("query").from(PSyncErrorRecord.class.getName(), "o");
    q.where("o.tenantId= :tenantId").p("tenantId", tenantId);
    q.where("o.type= :type").p("type", type);
    q.where("o.uuid= :uuid").p("uuid", id);
    List<PSyncErrorRecord> records = q.list(em, 0, 1);
    return records.isEmpty() ? null : records.get(0);
  }

  public PSyncErrorRecord getErrorRecord(String tenantId, String type, String businessKey) {
    EntityQuery q = new EntityQuery("query").from(PSyncErrorRecord.class.getName(), "o");
    q.where("o.tenantId= :tenantId").p("tenantId", tenantId);
    q.where("o.type= :type").p("type", type);
    q.where("o.businessKey= :businessKey").p("businessKey", businessKey);
    List<PSyncErrorRecord> records = q.list(em, 0, 1);
    return records.isEmpty() ? null : records.get(0);
  }

  @TX
  public void removeWaitRecord(List<String> uuids) {
    em.createQuery("delete from " + PSyncWaitRecord.class.getName() + " o where o.uuid in :uuids")
        .setParameter("uuids", uuids).executeUpdate();
    em.flush();
  }

  @TX
  public void removeWaitRecord(String tenantId, String type, List<String> keys) {
    em.createQuery("delete from " + PSyncWaitRecord.class.getName()
        + " o where o.tenantId =:tenantId" + " AND  o.type =:type and o.businessKey in :keys")
        .setParameter("tenantId", tenantId).setParameter("type", type).setParameter("keys", keys)
        .executeUpdate();
    em.flush();
  }

  @TX
  public void removeErrorRecord(String tenantId, String type, List<String> keys) {
    em.createQuery("delete from " + PSyncErrorRecord.class.getName()
        + " o where o.tenantId =:tenantId" + " AND  o.type =:type and o.businessKey in :keys")
        .setParameter("tenantId", tenantId).setParameter("type", type).setParameter("keys", keys)
        .executeUpdate();
    em.flush();
  }

}
