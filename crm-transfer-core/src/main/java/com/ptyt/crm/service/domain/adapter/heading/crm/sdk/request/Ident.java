package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import lombok.Data;

@Data
public class Ident {
  private String type;
  private String code;
}
