package com.ptyt.crm.service.domain.core.step;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StepConfig implements Serializable {
  private static final long serialVersionUID = 1L;

  public static final String NAME = "step";

  private Boolean auto = false;
  private Integer seed = 1700;
  private Integer max = 350000;
  private String timeStrs = "08:30~10:30";
  private String endTime = "11:30";
}
