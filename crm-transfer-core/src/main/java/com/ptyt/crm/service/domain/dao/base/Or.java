package com.ptyt.crm.service.domain.dao.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Or {

  public Or where(String cond) {
    if (wheres.contains(cond) == false)
      wheres.add(cond);
    return this;
  }

  public Or where(String cond, EntityQuery sub) {
    this.params.putAll(sub.getParams()); // 合并子查询参数，只考虑一级子查询
    wheres.add(new SubQuery(cond, sub));
    return this;
  }

  public Or p(String name, Object value) {
    this.params.put(name, value);
    return this;
  }

  public List<Object> getWheres() {
    return wheres;
  }

  public Map<String, Object> getParams() {
    return params;
  }

  private List<Object> wheres = new ArrayList();
  private Map<String, Object> params = new HashMap();

}
