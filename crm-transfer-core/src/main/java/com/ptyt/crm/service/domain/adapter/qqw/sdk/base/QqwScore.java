package com.ptyt.crm.service.domain.adapter.qqw.sdk.base;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class QqwScore {
  private BigDecimal currentCredit;
  private BigDecimal showIntegralNum;
  private BigDecimal surplusIntegralNum;

}
