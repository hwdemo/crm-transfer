package com.ptyt.crm.service.domain.infrastructure.filter;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.UUID;

@Component("traceFilter")
public class TraceFilter implements Filter {

    private static final String TRACE_ID = "trace_id";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        String traceId = UUID.randomUUID().toString().replace("-", "");
        MDC.put(TRACE_ID, traceId);
        TracingPool.setTracingId(traceId);

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {

    }
}
