
package com.ptyt.crm.service.domain.util.http;

import java.io.InputStream;

public interface HttpCallback {

  void handle(InputStream is) throws Exception;
}
