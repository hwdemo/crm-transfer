package com.ptyt.crm.service.domain.adapter.commons;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Member {
  private String mobile;
  private String name;
  private String state;
  @JsonProperty("member_id")
  private String memberId;
  private String validate;
  private Date birthday;
  private String grade;
  private Integer age;
  @JsonProperty("register_time")
  private Date registerTime;
  private String gender;
  private String email;
}
