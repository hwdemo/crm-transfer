package com.ptyt.crm.service.domain.core.job;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ptyt.crm.service.domain.adapter.weixin.TokenInfo;
import com.ptyt.crm.service.domain.adapter.weixin.WeiXinServices;
import com.ptyt.crm.service.domain.core.config.ConfigService;
import com.ptyt.crm.service.domain.core.weixin.WxConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WXRefreshTokenJob extends TenantJob {
    private static final Logger logger = LoggerFactory.getLogger(WXRefreshTokenJob.class);

    public static final String TASK_ID = "wx-refresh-token";

    @Autowired
    ConfigService configService;
    @Autowired
    WeiXinServices weiXinServices;

    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
        super.executeInternal(arg0);
        try {
            log.info("租户：" + getTenantId() + "开始执行：" + this.getClass().getSimpleName() + "...");
            process();
            log.info("租户：" + getTenantId() + "结束执行：" + this.getClass().getSimpleName() + "...");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void process() throws Exception {
        String tenantId = getTenantId();
        WxConfig wxConfig = configService.getConfig(tenantId, WxConfig.NAME, WxConfig.class);
        if (wxConfig == null || wxConfig.getAppId() == null) {
            return;
        }
        if (isExpire(wxConfig.getTokenExpireTime())) {
            try {
                doRefreshToken(tenantId, wxConfig);
            } catch (Exception e) {

            }
        }
    }

    private void doRefreshToken(String tenantId, WxConfig wxConfig) {
        try {
            TokenInfo tokenInfo = weiXinServices.getToken(tenantId, wxConfig.getAppId(), wxConfig.getSecret());
            wxConfig.setToken(tokenInfo.getAccessToken());
            wxConfig.setTokenExpireTime(DateUtils.addSeconds(new Date(), tokenInfo.getExpiresIn()));
            configService.put(getTenantId(), WxConfig.NAME, wxConfig);
        } catch (Exception e) {
            logger.error("", e);
        }

    }

    private boolean isExpire(Date expireDate) {
        if (expireDate == null) {
            return true;
        }
        // expire_time>=(当前时间-15分钟)，请求刷新令牌
        if (expireDate.getTime() / 1000 - 15 * 60 < new Date().getTime() / 1000) {
            return true;
        } else {
            return false;
        }
    }
}
