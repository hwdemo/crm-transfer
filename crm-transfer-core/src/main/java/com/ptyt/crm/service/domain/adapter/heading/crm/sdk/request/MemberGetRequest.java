package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.MemberGetResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class MemberGetRequest implements HeadingBaseRequest<MemberGetResponse> {
  private String ident_code;
  private String ident_type;

  @Override
  public String getApiMethod() {
    return "/crm/identservice/identify";
  }

  @Override
  public Object getBody() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<MemberGetResponse> getResponseClass() {
    return MemberGetResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.GET;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query=new HashMap<String, Object>();
    query.put("ident_code", ident_code);
    if(StringUtils.isNotBlank(ident_type)) {
      query.put("ident_type", ident_type);
    }
    return query;
  }

}
