package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwBaseResponse;

import io.swagger.models.HttpMethod;

public interface QqwBaseRequest<T extends QqwBaseResponse> {
  public abstract HttpMethod getHttpRequestMethod();

  /** 命令 */
  public abstract String getApiMethod();

  /** 数据 */
  public abstract Object getBody();

  /** 检查数据的合法性 */
  public abstract void check();

  /** 查询参数 */
  public abstract Map<String, Object> getQueryParam();

  /** 获取响应对象类型 */
  public abstract Class<T> getResponseClass();


  public abstract T getMockResponse();
}