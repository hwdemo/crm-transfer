package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwUser;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwOff2onUserCreateResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwOff2onUserGetResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwOff2onUserGetRequest implements QqwBaseRequest<QqwOff2onUserGetResponse> {

  private String phone;

  @Override
  public String getApiMethod() {
    return "/off2on/user/queryUserByPhone";
  }

  @Override
  public Object getBody() {
    Map<String, Object> map = new HashMap<String, Object>();
    return map;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<QqwOff2onUserGetResponse> getResponseClass() {
    return QqwOff2onUserGetResponse.class;
  }

  @Override
  public QqwOff2onUserGetResponse getMockResponse() {
    QqwOff2onUserGetResponse response=new QqwOff2onUserGetResponse();
    response.setData(new QqwUser());
    response.getData().setOfflineId(UUID.randomUUID().toString());
    response.getData().setPhone(phone);
    response.setCode("0");
    return response;
  }
  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.GET;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("phone", phone);
    return map;
  }

}
