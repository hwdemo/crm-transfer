package com.ptyt.crm.service.domain.adapter.qqw.sdk.response;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwToken;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwTokenGetResponse extends QqwBaseResponse<QqwToken>{
}
