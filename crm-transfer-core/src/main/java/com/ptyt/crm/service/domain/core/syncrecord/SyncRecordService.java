package com.ptyt.crm.service.domain.core.syncrecord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ptyt.crm.service.api.syncrecord.SyncErrorRecord;
import com.ptyt.crm.service.api.syncrecord.SyncRecord;
import com.ptyt.crm.service.api.syncrecord.SyncRecordFilter;
import com.ptyt.crm.service.api.syncrecord.SyncRecordResult;
import com.ptyt.crm.service.api.syncrecord.SyncWaitRecord;
import com.ptyt.crm.service.domain.dao.base.TX;
import com.ptyt.crm.service.domain.dao.syncrecord.PSyncErrorRecord;
import com.ptyt.crm.service.domain.dao.syncrecord.PSyncRecord;
import com.ptyt.crm.service.domain.dao.syncrecord.PSyncWaitRecord;
import com.ptyt.crm.service.domain.dao.syncrecord.SyncRecordDao;

@Service
public class SyncRecordService {

  @Autowired
  SyncRecordDao syncRecordDao;

  public SyncRecord getSyncRecord(String tenantId, String type, String businessKey) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(type, type);
    Assert.notNull(businessKey, businessKey);
    PSyncRecord pSyncRecord = syncRecordDao.get(tenantId, type, businessKey);
    if (pSyncRecord == null) {
      return null;
    }
    SyncRecord record = new SyncRecord();
    BeanUtils.copyProperties(pSyncRecord, record);
    return record;
  }

  public void saveSyncErrorRecord(String tenantId, SyncErrorRecord record) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(record, "record");

    PSyncErrorRecord errorRecord = null;
    if (record.getId() != null) {
      errorRecord = syncRecordDao.getErrorRecordById(tenantId, record.getType(), record.getId());
    }
    if (errorRecord == null)
      errorRecord = new PSyncErrorRecord();
    BeanUtils.copyProperties(record, errorRecord);
    errorRecord.setTenantId(tenantId);
    syncRecordDao.saveSyncErrorRecord(Arrays.asList(errorRecord));
  }

  public void saveSyncWaitRecord(String tenantId, SyncWaitRecord record) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(record, "record");

    PSyncWaitRecord pSyncWaitRecord = new PSyncWaitRecord();
    BeanUtils.copyProperties(record, pSyncWaitRecord);
    pSyncWaitRecord.setTenantId(tenantId);
    syncRecordDao.saveSyncWaitRecord(pSyncWaitRecord);
  }

  public SyncErrorRecord getErrorRecordByKey(String tenantId, String type, String businessKey) {
    List<SyncErrorRecord> result = new ArrayList<>();
    SyncRecordFilter filter = new SyncRecordFilter();
    filter.setTypeEq(type);
    filter.setBusinessKeyEq(businessKey);
    List<SyncErrorRecord> records = queryErrorRecord(tenantId, filter, 0, 100, null, false);
    if (records.isEmpty())
      return null;
    return result.get(0);
  }

  public List<SyncErrorRecord> queryErrorRecord(String tenantId, SyncRecordFilter filter, int page,
      int pageSize, String sortKey, boolean desc) {
    List<SyncErrorRecord> result = new ArrayList<>();
    List<PSyncErrorRecord> records = syncRecordDao.queryErrorRecord(tenantId, filter, page,
        pageSize, sortKey, desc);
    for (PSyncErrorRecord errorRecord : records) {
      SyncErrorRecord record = new SyncErrorRecord();
      BeanUtils.copyProperties(errorRecord, record);
      record.setId(errorRecord.getUuid());
      result.add(record);
    }
    return result;
  }

  @TX
  public void recoverErrorRecord(String tenantId, String type, List<SyncErrorRecord> errorRecords) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(type, "type");

    List<String> uuids = new ArrayList<>();
    List<PSyncWaitRecord> syncRecords = new ArrayList<>();
    for (SyncErrorRecord errorRecord : errorRecords) {
      uuids.add(errorRecord.getId());
      PSyncWaitRecord record = new PSyncWaitRecord();
      BeanUtils.copyProperties(errorRecord, record);
      record.setTenantId(tenantId);
      record.setCreated(errorRecord.getTime());
      syncRecords.add(record);
    }
    if (CollectionUtils.isNotEmpty(syncRecords)) {
      syncRecordDao.saveSyncWaitRecords(syncRecords);
    }
    if (CollectionUtils.isNotEmpty(uuids)) {
      syncRecordDao.removeErrorRecord(uuids);
    }
  }

  @TX
  public void deleteErrorRecord(String tenantId, String type, List<SyncErrorRecord> errorRecords) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(type, "type");

    List<String> uuids = new ArrayList<>();
    for (SyncErrorRecord errorRecord : errorRecords) {
      uuids.add(errorRecord.getId());
    }
    if (CollectionUtils.isNotEmpty(uuids)) {
      syncRecordDao.removeErrorRecord(uuids);
    }
  }

  public List<SyncWaitRecord> queryWaitRecord(String tenantId, SyncRecordFilter filter, int page,
      int pageSize, String sortKey, boolean desc) {
    List<SyncWaitRecord> result = new ArrayList<>();
    List<PSyncWaitRecord> pSyncWaitRecords = syncRecordDao.queryWaitRecord(tenantId, filter, page,
        pageSize, sortKey, desc);

    for (PSyncWaitRecord pSyncWaitRecord : pSyncWaitRecords) {
      SyncWaitRecord record = new SyncWaitRecord();
      BeanUtils.copyProperties(pSyncWaitRecord, record);
      record.setId(pSyncWaitRecord.getUuid());
      result.add(record);
    }
    return result;
  }

  @TX
  public void saveSyncRecordResult(String tenantId, String type, List<SyncWaitRecord> records,
      List<SyncRecordResult> results) {
    Map<String, SyncRecordResult> resultMap = new HashMap<>();
    for (SyncRecordResult result : results) {
      resultMap.put(result.getId(), result);
    }
    Set<String> uuids = new HashSet<>();
    List<PSyncRecord> syncRecords = new ArrayList<>();
    List<PSyncErrorRecord> syncErrorRecords = new ArrayList<>();
    for (SyncWaitRecord record : records) {
      if (record.getId() != null) {
        uuids.add(record.getId());
      }
      SyncRecordResult result = resultMap.get(record.getId());
      if (result == null) {
        PSyncErrorRecord syncErrorRecord = new PSyncErrorRecord();
        BeanUtils.copyProperties(record, syncErrorRecord);
        syncErrorRecord.setTime(new Date());
        syncErrorRecord.setMessage("同步结果不存在");
        syncErrorRecord.setTenantId(tenantId);
        if (syncErrorRecord.getRetries() == null) {
          syncErrorRecord.setRetries(1);
        } else {
          syncErrorRecord.setRetries(syncErrorRecord.getRetries() + 1);
        }
        syncErrorRecords.add(syncErrorRecord);
        continue;
      }
      if (result.isSuccess()) {
        if (result.isIgnore()) {
          continue;
        }
        PSyncRecord syncSuccessRecord = new PSyncRecord();
        BeanUtils.copyProperties(record, syncSuccessRecord);
        BeanUtils.copyProperties(result, syncSuccessRecord);
        syncSuccessRecord.setTenantId(tenantId);
        syncRecords.add(syncSuccessRecord);
      } else {
        PSyncErrorRecord syncErrorRecord = new PSyncErrorRecord();
        BeanUtils.copyProperties(record, syncErrorRecord);
        BeanUtils.copyProperties(result, syncErrorRecord);
        syncErrorRecord.setTenantId(tenantId);
        if (syncErrorRecord.getRetries() == null) {
          syncErrorRecord.setRetries(1);
        } else {
          syncErrorRecord.setRetries(syncErrorRecord.getRetries() + 1);
        }
        syncErrorRecords.add(syncErrorRecord);
      }
    }
    if (CollectionUtils.isNotEmpty(uuids)) {
      syncRecordDao.removeWaitRecord(new ArrayList<>(uuids));
    }
    if (CollectionUtils.isNotEmpty(syncErrorRecords)) {
      syncRecordDao.saveSyncErrorRecord(syncErrorRecords);
    }
    if (CollectionUtils.isNotEmpty(syncRecords)) {
      syncRecordDao.saveSyncRecord(syncRecords);
    }
  }

}
