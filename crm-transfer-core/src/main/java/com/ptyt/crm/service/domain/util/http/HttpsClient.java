package com.ptyt.crm.service.domain.util.http;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * @author zhangyanbo
 */
public class HttpsClient extends HttpClient {
  private int connectTimeout = 10000;
  private int readTimeout = 5000;

  public HttpsClient() {

  }

  public HttpsClient(int connectTimeout, int readTimeout) {
    this.connectTimeout = connectTimeout;
    this.readTimeout = readTimeout;
  }

  public String posts(String url, String body, Map<String, String> headers) throws Exception {
    HttpURLConnection conn = null;
    OutputStream out = null;
    try {

      conn = getConnections(new URL(url), "POST", headers);
      out = conn.getOutputStream();
      if (body != null)
        out.write(body.getBytes("UTF-8"));
      out.close();
      String s = getResponseBodyAsString(conn);
      if (conn.getResponseCode() >= 400) {
        printRequest(url, body);
        printResponse(conn.getResponseCode(), s);
        throw new Exception(conn.getResponseCode() + " " + s);
      }
      return s;
    } finally {
      if (out != null) {
        out.close();
      }
      if (conn != null) {
        conn.disconnect();
      }
    }
  }

  protected void printResponse(int code, String body) {
    StringBuilder sb = new StringBuilder();
    sb.append("Response: \n");
    sb.append("==========================\n");
    sb.append(code).append("\n");
    if (body != null) {
      sb.append(body).append("\n");
    }
    sb.append("==========================");
    logger.info(sb.toString());
  }

  protected void printRequest(String url, String body) {
    StringBuilder sb = new StringBuilder();
    sb.append("Request:\n");
    sb.append("==========================\n");
    sb.append(" ").append(url).append("\n");
    if (body != null) {
      sb.append(body).append("\n");
    }
    sb.append("==========================");
    logger.info(sb.toString());
  }

  public String gets(String url, Map<String, String> headers) throws Exception {
    HttpURLConnection conn = null;
    OutputStream out = null;
    try {
      printRequest(url, null);
      conn = getConnections(new URL(url), "POST", headers);
      out = conn.getOutputStream();
      out.close();
      String s = getResponseBodyAsString(conn);
      if (conn.getResponseCode() >= 400)
        throw new Exception(conn.getResponseCode() + " " + s);
      return s;
    } finally {
      if (out != null) {
        out.close();
      }
      if (conn != null) {
        conn.disconnect();
      }
    }

  }

  private HttpsURLConnection getConnections(URL url, String method, Map<String, String> headers)
      throws Exception {
    SSLContext ctx = SSLContext.getInstance("TLS");
    ctx.init(new KeyManager[0], new TrustManager[] {
        new DefaultTrustManager() }, new SecureRandom());
    SSLContext.setDefault(ctx);

    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
    conn.setRequestMethod(method);
    conn.setDoInput(true);
    conn.setDoOutput(true);
    conn.setConnectTimeout(connectTimeout);
    conn.setReadTimeout(readTimeout);
    if (headers != null) {
      for (Entry<String, String> header : headers.entrySet())
        conn.setRequestProperty(header.getKey(), header.getValue());
    }
    conn.setHostnameVerifier(new HostnameVerifier() {
      @Override
      public boolean verify(String hostname, SSLSession session) {
        return true;
      }
    });

    return conn;
  }

  private static class DefaultTrustManager implements X509TrustManager {

    @Override
    public void checkClientTrusted(X509Certificate[] arg0, String arg1)
        throws CertificateException {
    }

    @Override
    public void checkServerTrusted(X509Certificate[] arg0, String arg1)
        throws CertificateException {
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
      return null;
    }

  }

}
