package com.ptyt.crm.service.domain.adapter.qqw.sdk.response;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwScore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwQueryUserCreditRecordResponse extends QqwBaseResponse<QqwScore> {

}
