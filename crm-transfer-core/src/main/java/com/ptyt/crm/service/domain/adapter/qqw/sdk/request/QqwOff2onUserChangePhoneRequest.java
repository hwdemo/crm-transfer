package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwBaseResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwOff2onUserChangePhoneRequest implements QqwBaseRequest<QqwBaseResponse> {

  private String offlineId;
  private String oldPhone;
  private String newPhone;

  @Override
  public String getApiMethod() {
    return "/off2on/user/changePhone";
  }

  @Override
  public Object getBody() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("offlineId", offlineId);
    map.put("oldPhone", oldPhone);
    map.put("newPhone", newPhone);
    return map;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<QqwBaseResponse> getResponseClass() {
    return QqwBaseResponse.class;
  }

  @Override
  public QqwBaseResponse getMockResponse() {
    QqwBaseResponse response=new QqwBaseResponse();
    response.setCode("0");
    return response;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    // TODO Auto-generated method stub
    return null;
  }

}
