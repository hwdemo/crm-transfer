package com.ptyt.crm.service.domain.adapter.weixin.sdk.response;

public class WxBaseResponse {
    private Integer errcode; // 微信侧错误码，下单失败时返回
    private String errmsg; // 微信侧错误信息，下单失败时返回

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
}
