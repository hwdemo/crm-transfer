package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberGetResponse extends HeadingBaseResponse<Object>{
  
  private String mobile;
  private String name;
  private String state;
  private String member_id;
  private Integer age;
  private String grade;
  private Date birthday;
}
