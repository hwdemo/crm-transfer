package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Coupon {
  private String activity_id;
  private String code;
  private String name;
  
  private String status;
  
  private String member_id;
  private String member_name;
  private String member_code;
  
  private String platform_id;
  private String platform_name;
  private String send_platform_id;
  private String send_platform_name;
  
  private Date begin_date;
  private Date end_date;
  private Date created;
  private Date modified;
  
  private String use_rule;
}
