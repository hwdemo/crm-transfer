package com.ptyt.crm.service.domain.adapter.qqw.sdk.base;

import lombok.Data;

@Data
public class QqwToken {
    private String accessToken;
}
