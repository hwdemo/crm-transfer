package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.MemberModifyResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberModifyRequest implements HeadingBaseRequest<MemberModifyResponse> {
  private String mobile;
  private String gender;
  private String name;
  private String birthday;
  private String email;
  
  private String memberId;
  private String operator;

  @Override
  public String getApiMethod() {
    return "/crm/identservice/ident/modify";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("mobile", mobile);
    query.put("gender", gender);
    query.put("email", email);
    query.put("name", name);
    query.put("birthday", birthday);
    return query;
  }

  @Override
  public void check() {

  }

  @Override
  public Class<MemberModifyResponse> getResponseClass() {
    return MemberModifyResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("member_id", memberId);
    query.put("operator", operator);
    return query;
  }

}
