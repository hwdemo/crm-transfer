package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HeadingCouponHst {
  // dispatch, verification, restoration, abort, expire, present,
  // receive_present, expire_present, re_acquire, bind, print, receiveBack,
  // recovery, lock, unlock
  private String action;
  private String coupon_code;
  private String tranId;
  private String xid;
  private String createPlatform;
  private String verify_store_id;

}
