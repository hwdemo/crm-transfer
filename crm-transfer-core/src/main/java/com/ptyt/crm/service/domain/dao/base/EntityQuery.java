package com.ptyt.crm.service.domain.dao.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityQuery {
	private static final Logger LOG = LoggerFactory.getLogger(EntityQuery.class);
	private static final boolean DEBUG = LOG.isDebugEnabled();

	public EntityQuery() {
	}

	public EntityQuery(String name) {
		this.name = name;
	}

	public EntityQuery select(String... fields) {
		for (String field : fields)
			addToList(selects, field);
		return this;
	}

	public EntityQuery from(String table) {
		addToList(froms, table);
		return this;
	}

	public EntityQuery from(String table, Object alias) {
		return from(table + " " + alias);
	}

	public EntityQuery where(String cond) {
		if (wheres.contains(cond) == false)
			wheres.add(cond);
		return this;
	}

	public EntityQuery where(Or or) {
		if (or.getWheres().isEmpty() == false) {
			this.params.putAll(or.getParams());
			wheres.add(or);
		}
		return this;
	}

	public EntityQuery where(String cond, EntityQuery sub) {
		this.params.putAll(sub.getParams()); // 合并子查询参数，只考虑一级子查询
		wheres.add(new SubQuery(cond, sub));
		return this;
	}

	public EntityQuery orderBy(String field) {
		addToList(orderBys, field);
		return this;
	}

	public EntityQuery orderBy(String field, String dir) {
		return orderBy(field + " " + dir);
	}

	public EntityQuery groupBy(String... fields) {
		for (String field : fields)
			addToList(groupBys, field);
		return this;
	}

	public EntityQuery p(String name, Object value) {
		params.put(name, value);
		return this;
	}

	public List list(EntityManager em) {
		return list(em, 0, MAX_COUNT);
	}

	public List list(EntityManager em, int count) {
		return list(em, 0, count);
	}

	public long count(EntityManager em) {
		List<String> oldSelects = selects;
		List<String> oldOrderBys = orderBys;

		selects.clear();
		selects.add("count(*)");
		orderBys.clear();
		try {
			List list = list(em);
			return list.size() > 0 ? ((Number) list.get(0)).longValue() : 0;
		} finally {
			selects = oldSelects;
			orderBys = oldOrderBys;
		}
	}

	public List list(EntityManager em, int page, int pageSize) {
		long start = System.currentTimeMillis();
		String hql = buildQl(0, false);
		StringBuilder sb = new StringBuilder();
		if (DEBUG)
			sb.append(name).append("-------------------------\r\n").append(buildQl(0, true));
		Query query = em.createQuery(hql);
		if (DEBUG) {
			if (params.isEmpty() == false)
				sb.append("\r\n参数：");
		}
		for (Entry<String, Object> param : params.entrySet()) {
			query.setParameter(param.getKey(), param.getValue());
			if (DEBUG)
				sb.append("\r\n").append(param.getKey()).append("=").append(param.getValue());
		}
		if (DEBUG)
			LOG.trace(sb.append("\r\n-------------------------").toString());
		query.setFirstResult(page * pageSize);
		query.setMaxResults(pageSize);
		List result = query.getResultList();
		long end = System.currentTimeMillis();
		LOG.debug("{} 耗时 {} ms。", name, (end - start));
		return result;
	}

	// ident>0表示带换行和缩进的打印方式，用于调试
	private String buildQl(int level, boolean debug) {
		String prefix = debug ? StringUtils.repeat("  ", level) : " ";
		String ident = debug ? StringUtils.repeat("  ", level + 1) : " ";
		String newLine = debug ? "\r\n" : "";
		StringBuilder sb = new StringBuilder();
		if (selects.isEmpty() == false) {
			sb.append(prefix).append("select ");
			String delim = newLine + ",";
			sb.append(StringUtils.join(selects, delim));
		}
		if (froms.isEmpty() == false) {
			sb.append(newLine).append(prefix).append("from ");
			String delim = "," + newLine + ident;
			sb.append(StringUtils.join(froms, delim));
		}
		if (wheres.isEmpty() == false) {
			sb.append(newLine).append(prefix).append("where ");
			List<String> temp = new ArrayList();
			for (Object cond : wheres) {
				temp.add(getCondQl(cond, debug, level));
			}
			String delim = newLine + ident + "and ";
			sb.append(StringUtils.join(temp, delim));
		}
		if (groupBys.isEmpty() == false) {
			sb.append(newLine).append(prefix).append("group by ");
			String delim = newLine + ident + ",";
			sb.append(StringUtils.join(groupBys, delim));
		}
		if (orderBys.isEmpty() == false) {
			sb.append(newLine).append(prefix).append("order by ");
			String delim = newLine + ident + ",";
			sb.append(StringUtils.join(orderBys, delim));
		}
		return sb.toString();
	}

	private String getCondQl(Object cond, boolean debug, int level) {
		String ident = debug ? StringUtils.repeat("  ", level + 1) : " ";
		String newLine = debug ? "\r\n" : "";
		if (cond instanceof SubQuery) {
			SubQuery sq = (SubQuery) cond;
			String subQl = "(" + newLine + sq.getQuery().buildQl(level + 2, debug) + newLine + ident + ")";
			return sq.getCond().replaceAll("\\?", subQl);
		} else if (cond instanceof Or) {
			Or or = (Or) cond;
			List<String> temp = new ArrayList();
			for (Object c : or.getWheres()) {
				temp.add("(" + getCondQl(c, debug, level + 1) + ")");
			}
			String ident2 = debug ? StringUtils.repeat("  ", level + 2) : " ";
			String delim = newLine + ident2 + "or ";
			return "(" + StringUtils.join(temp, delim) + newLine + ident + ")";
		} else {
			return cond.toString();
		}
	}

	private void addToList(List<String> list, String s) {
		if (list.contains(s) == false)
			list.add(s);
	}

	public List<String> getSelects() {
		return selects;
	}

	public List<String> getFroms() {
		return froms;
	}

	public List<Object> getWheres() {
		return wheres;
	}

	public List<String> getOrderBys() {
		return orderBys;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	private String name = "";
	private List<String> selects = new ArrayList();
	private List<String> froms = new ArrayList();
	private List<Object> wheres = new ArrayList();
	private List<String> orderBys = new ArrayList();
	private List<String> groupBys = new ArrayList();
	private Map<String, Object> params = new HashMap();

	private static final int MAX_COUNT = 1000;
}
