package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import java.util.ArrayList;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.ScoreHst;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScoreHstResponse extends HeadingBaseResponse<java.util.List<ScoreHst>> {
  private java.util.List<ScoreHst> hsts = new ArrayList<ScoreHst>();
}
