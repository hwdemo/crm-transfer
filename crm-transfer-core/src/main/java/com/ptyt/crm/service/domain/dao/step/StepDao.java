package com.ptyt.crm.service.domain.dao.step;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import com.ptyt.crm.service.domain.dao.base.EntityQuery;
import com.ptyt.crm.service.domain.dao.syncrecord.PSyncRecord;
import org.springframework.stereotype.Repository;

import com.ptyt.crm.service.domain.dao.base.BaseDao;
import com.ptyt.crm.service.domain.dao.base.TX;

@Repository
public class StepDao extends BaseDao {

  @TX
  public void save(PStep stepPresent, String operator) {
    if (stepPresent.getUuid() == null) {
      stepPresent.onCreated(operator);
      stepPresent.onModified(operator);
      em.persist(stepPresent);
    } else {
      stepPresent.onModified(operator);
      em.merge(stepPresent);
    }
    em.flush();
  }
  
  public PStep getById(String tenantId, String id) {
    EntityQuery q = new EntityQuery("query").from(PStep.class.getName(), "o");
    q.where("o.tenantId= :tenantId").p("tenantId", tenantId);
    q.where("o.id= :id").p("id", id);
    List<PStep> records = q.list(em, 0, 1);
    return records.isEmpty() ? null : records.get(0);
  }

  public List<PStep> query(String tenantId, String userId, Date startTime, Date endTime) {
    String hql = "from " + PStep.class.getName() + " o where o.tenantId =:tenantId";

    if (userId != null) {
      hql += " and o.userId=:userId";
    }

    if (startTime != null) {
      hql += " and time>=:startTime";
    }

    if (endTime != null) {
      hql += " and time<=:endTime";
    }
    Query query = em.createQuery(hql);
    query.setParameter("tenantId", tenantId);
    if(userId!=null) {
      query.setParameter("userId", userId);
    }
    if (startTime != null) {
      query.setParameter("startTime", startTime);
    }
    if (endTime != null) {
      query.setParameter("endTime", endTime);
    }
    return query.getResultList();
  }

}
