package com.ptyt.crm.service.domain.util.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HttpClient {

  protected int connectTimeout = 10000;
  protected int readTimeout = 10000;

  public HttpClient() {
  }

  public HttpClient(String user, String password) {
    this.user = user;
    this.password = password;
  }

  protected Logger logger = LoggerFactory.getLogger(this.getClass());

  public HttpResult doPut(String url, String body, Map<String, String> headers) throws Exception {
    return doPutOrPost(url, body, "POST", headers);
  }

  public HttpResult doPost(String url, String body, Map<String, String> headers) throws Exception {
    return doPutOrPost(url, body, "POST", headers);
  }

  public HttpResult doDelete(String url, Map<String, String> headers) throws Exception {
    return doGetOrDelete(url, "DELETE", headers);
  }

  public HttpResult doGet(String url, Map<String, String> headers) throws Exception {
    return doGetOrDelete(url, "GET", headers);
  }

  protected void printRequest(String url, String method, String body) {
    StringBuilder sb = new StringBuilder();
    sb.append("Request:\n");
    sb.append("==========================\n");
    sb.append(method).append(" ").append(url).append("\n");
    if (body != null) {
      sb.append(body).append("\n");
    }
    sb.append("==========================");
    logger.info(sb.toString());
  }

  protected void printResponse(HttpResult hr) {
    StringBuilder sb = new StringBuilder();
    sb.append("Response: \n");
    sb.append("==========================\n");
    sb.append(hr.getStatusCode()).append("\n");
    if (hr.getBody() != null) {
      sb.append(hr.getBody()).append("\n");
    }
    sb.append("==========================");
    logger.info(sb.toString());
  }

  protected HttpResult doGetOrDelete(String url, String method, Map<String, String> headers)
      throws Exception {
    HttpURLConnection conn = null;
    try {
      printRequest(url, method, null);
      conn = getConnection(new URL(url), method, headers);
      String s = getResponseBodyAsString(conn);

      HttpResult hr = new HttpResult(conn.getResponseCode(), s);
      printResponse(hr);
      return hr;
    } finally {
      if (conn != null) {
        conn.disconnect();
      }
    }
  }

  protected HttpResult doPutOrPost(String url, String body, String method,
      Map<String, String> headers) throws Exception {
    HttpURLConnection conn = null;
    OutputStream out = null;
    try {
      printRequest(url, method, body);
      conn = getConnection(new URL(url), method, headers);

      out = conn.getOutputStream();
      if (body != null)
        out.write(body.getBytes("UTF-8"));
      out.close();
      String s = getResponseBodyAsString(conn);
      HttpResult hr = new HttpResult(conn.getResponseCode(), s);
      printResponse(hr);
      return hr;
    } finally {
      if (out != null) {
        out.close();
      }
      if (conn != null) {
        conn.disconnect();
      }
    }
  }

  private HttpURLConnection getConnection(URL url, String method, Map<String, String> headers)
      throws IOException {
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod(method);
    conn.setDoInput(true);
    conn.setDoOutput(true);
    conn.setConnectTimeout(getConnectTimeout());
    conn.setReadTimeout(getReadTimeout());
    if (headers != null) {
      for (Entry<String, String> header : headers.entrySet())
        conn.setRequestProperty(header.getKey(), header.getValue());
    }
    if (user != null) {
      String s = user + ":" + password;
      conn.setRequestProperty("Authorization",
          "Basic " + new String(Base64.encodeBase64(s.getBytes())));
    }
    return conn;
  }

  protected String getResponseBodyAsString(HttpURLConnection conn) throws IOException {
    InputStream is = null;
    if (conn.getResponseCode() >= 400)
      is = conn.getErrorStream();
    else
      is = conn.getInputStream();
    if (is == null)
      return null;

    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
      StringWriter writer = new StringWriter();

      char[] chars = new char[256];
      int count = 0;
      while ((count = reader.read(chars)) > 0) {
        writer.write(chars, 0, count);
      }

      return writer.toString();
    } finally {
      is.close();
    }
  }

  public int getConnectTimeout() {
    return connectTimeout;
  }

  public void setConnectTimeout(int connectTimeout) {
    this.connectTimeout = connectTimeout;
  }

  public int getReadTimeout() {
    return readTimeout;
  }

  public void setReadTimeout(int readTimeout) {
    this.readTimeout = readTimeout;
  }

  protected String user;
  protected String password;

}
