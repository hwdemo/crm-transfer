package com.ptyt.crm.service.domain.adapter.qqw.sdk.base;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwUser {
  private String offlineId;
  private String phone;
  private String nickName;
  private String fullName;
  private Integer sex;
  private Date birthday;
  private String email;
  private String province;
  private String city;
  private String county;
  private String address;
  private String logoUrl;
  private Date registryTime;
}
