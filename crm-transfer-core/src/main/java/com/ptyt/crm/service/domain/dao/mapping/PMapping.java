package com.ptyt.crm.service.domain.dao.mapping;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import com.ptyt.crm.service.domain.dao.base.PEntity;

@Entity
@Table(name = "ct_mapping", indexes = {
    @Index(name = "index_ct_mapping_1", columnList = "tenant_id,type,source_type,source_id"),
    @Index(name = "index_ct_mapping_2", columnList = "tenant_id,type,target_type,target_id") })
public class PMapping extends PEntity {
  private static final long serialVersionUID = -6347861086924401865L;

  private String tenantId;
  private String type;
  private String sourceType;
  private String sourceId;
  private String targetType;
  private String targetId;
  private Date created;
  private String extra;

  @Column(name = "tenant_id", length = 128, nullable = false)
  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }

  @Column(name = "type", length = 64)
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Column(name = "source_type", length = 64)
  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  @Column(name = "source_id", length = 64)
  public String getSourceId() {
    return sourceId;
  }

  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  @Column(name = "target_type", length = 64)
  public String getTargetType() {
    return targetType;
  }

  public void setTargetType(String targetType) {
    this.targetType = targetType;
  }

  @Column(name = "target_id", length = 64)
  public String getTargetId() {
    return targetId;
  }

  public void setTargetId(String targetId) {
    this.targetId = targetId;
  }

  @Column(name = "created")
  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  @Column(name = "extra", length = 1024)
  public String getExtra() {
    return extra;
  }

  public void setExtra(String extra) {
    this.extra = extra;
  }

}
