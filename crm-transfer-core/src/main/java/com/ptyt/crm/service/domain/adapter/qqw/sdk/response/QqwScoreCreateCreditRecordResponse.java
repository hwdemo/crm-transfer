package com.ptyt.crm.service.domain.adapter.qqw.sdk.response;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwUploadScoreResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwScoreCreateCreditRecordResponse extends QqwBaseResponse<QqwUploadScoreResponse> {

}
