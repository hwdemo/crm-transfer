package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponQueryActivityResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponQueryActivityRequest implements HeadingBaseRequest<CouponQueryActivityResponse> {
  private String activity_id;
  private List<String> activity_ids;
  private String platform_id;
  private Integer page = 0;
  private Integer page_size = 100;
  private String sort_key;
  private Boolean desc = false;

  @Override
  public String getApiMethod() {
    return "/crm/couponservice/activities";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("activity_id", activity_id);
    if (CollectionUtils.isEmpty(activity_ids) == false)
      query.put("activity_ids", activity_ids.toArray());
    query.put("platform_id", platform_id);
    query.put("page", page);
    query.put("page_size", page_size);
    query.put("sort_key", sort_key);
    query.put("desc", desc);
    return query;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<CouponQueryActivityResponse> getResponseClass() {
    return CouponQueryActivityResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("activity_ids", activity_ids.toArray());
    return query;
  }

}
