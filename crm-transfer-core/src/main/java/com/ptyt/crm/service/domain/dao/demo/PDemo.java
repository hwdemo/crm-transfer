package com.ptyt.crm.service.domain.dao.demo;

import com.ptyt.crm.service.domain.dao.base.PEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "demo")
public class PDemo extends PEntity {

    private String tenantId;

    /**
     * 租户
     */
    @Column(name = "tenant_id", length = 20, nullable = false)
    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }
}
