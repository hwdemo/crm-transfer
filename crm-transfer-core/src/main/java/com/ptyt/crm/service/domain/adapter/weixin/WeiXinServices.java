package com.ptyt.crm.service.domain.adapter.weixin;

import com.alibaba.fastjson.JSONObject;
import com.ptyt.crm.service.domain.adapter.weixin.sdk.response.WxGetTokenResponse;
import com.ptyt.crm.service.domain.adapter.weixin.sdk.response.WxGetUserResponse;
import com.ptyt.crm.service.domain.util.http.HttpClient;
import com.ptyt.crm.service.domain.util.http.HttpResult;
import org.springframework.stereotype.Service;

@Service
public class WeiXinServices {

    public TokenInfo getToken(String tenantId, String appid, String secret)
            throws Exception {
        HttpClient client = new HttpClient();
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
        url = url.replace("APPID", appid);
        url = url.replace("APPSECRET", secret);
        HttpResult resp = client.doGet(url, null);
        JSONObject json = JSONObject.parseObject(resp.getBody());
        WxGetTokenResponse response = (WxGetTokenResponse) JSONObject.toJavaObject(json,
                WxGetTokenResponse.class);
        TokenInfo tokenInfo = new TokenInfo();
        tokenInfo.setAccessToken(response.getAccessToken());
        tokenInfo.setExpiresIn(response.getExpiresIn());
        return tokenInfo;
    }

    public WxGetUserResponse getOpenId(String appid, String secret, String code)
            throws Exception {
        HttpClient client = new HttpClient();
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code";
        url = url.replace("APPID", appid);
        url = url.replace("SECRET", secret);
        url = url.replace("JSCODE", code);
        HttpResult resp = client.doGet(url, null);
        JSONObject json = JSONObject.parseObject(resp.getBody());
        WxGetUserResponse response = (WxGetUserResponse) JSONObject.toJavaObject(json,
                WxGetUserResponse.class);
        return response;
    }
}
