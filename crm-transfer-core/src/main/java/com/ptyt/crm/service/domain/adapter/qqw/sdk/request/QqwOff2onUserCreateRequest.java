package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwBaseResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwOff2onUserCreateResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwOff2onUserCreateRequest
    implements QqwBaseRequest<QqwOff2onUserCreateResponse> {

  private String offlineId;
  private String phone;
  private String nickName;
  private String fullName;
  private Integer sex;
  private Date birthday;
  private String email;
  private String province;
  private String city;
  private String county;
  private String address;
  private String logoUrl;
  private Date registryTime;


  @Override
  public String getApiMethod() {
    return "/off2on/user/create";
  }

  @Override
  public Object getBody() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("offlineId", offlineId);
    map.put("phone", phone);
    map.put("nickName", nickName);
    map.put("fullName", fullName);
    map.put("sex",sex);
    map.put("birthday", birthday);
    map.put("email", email);
    map.put("province", province);
    map.put("city", city);
    map.put("county", county);
    map.put("address", address);
    map.put("logoUrl", logoUrl);
    map.put("registryTime", registryTime);
    return map;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<QqwOff2onUserCreateResponse> getResponseClass() {
    return QqwOff2onUserCreateResponse.class;
  }

  @Override
  public QqwOff2onUserCreateResponse getMockResponse() {
    QqwOff2onUserCreateResponse response=new QqwOff2onUserCreateResponse();
    response.setCode("0");
    return response;
  }
  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    // TODO Auto-generated method stub
    return null;
  }

}
