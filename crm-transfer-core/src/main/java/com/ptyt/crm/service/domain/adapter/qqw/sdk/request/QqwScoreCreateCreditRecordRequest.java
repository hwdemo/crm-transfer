package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwAddOrModifyCustomerResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwOff2onUserModifyResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwScoreCreateCreditRecordResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwScoreCreateCreditRecordRequest
    implements QqwBaseRequest<QqwScoreCreateCreditRecordResponse> {

  private String offlineId;
  private String phone;
  private String flowCode;
  private String transactionCode;
  private String storeNo;
  private String integralObtainType;
  private String integralObtainDesc;
  private BigDecimal credit;
  private Double showNum;
  private Double surplusNum;
  private Date occurrenceTime;
  private String rule;

  @Override
  public String getApiMethod() {
    return "/off2on/credit/createCreditRecord";
  }

  @Override
  public Object getBody() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("offlineId", offlineId);
    map.put("phone", phone);
    map.put("flowCode", flowCode);
    map.put("transactionCode", transactionCode);
    map.put("storeNo", storeNo);
    map.put("integralObtainType", integralObtainType);
    map.put("integralObtainDesc", integralObtainDesc);
    map.put("credit", credit);
    map.put("showNum", showNum);
    map.put("surplusNum", surplusNum);
    map.put("occurrenceTime", occurrenceTime);
    map.put("rule", rule);
    return map;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<QqwScoreCreateCreditRecordResponse> getResponseClass() {
    return QqwScoreCreateCreditRecordResponse.class;
  }

  @Override
  public QqwScoreCreateCreditRecordResponse getMockResponse() {
    QqwScoreCreateCreditRecordResponse response=new QqwScoreCreateCreditRecordResponse();
    response.setCode("0");
    return response;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    // TODO Auto-generated method stub
    return null;
  }

}
