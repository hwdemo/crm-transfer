package com.ptyt.crm.service.domain.adapter.weixin.sdk.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WxGetUserResponse extends WxBaseResponse {
  @JsonProperty("openid")
  private String openid;// string 获取到的凭证
  @JsonProperty("session_key")
  private String sessionKey;// number 凭证有效时间，单位：秒。目前是7200秒之内的值。

}
