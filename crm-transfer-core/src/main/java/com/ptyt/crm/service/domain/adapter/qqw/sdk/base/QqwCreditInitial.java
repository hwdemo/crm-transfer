package com.ptyt.crm.service.domain.adapter.qqw.sdk.base;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwCreditInitial {
  private String flowCode;
  private String transactionCode;
  private String offlineId;
  private String phone;
  private String storeNo;
  private Double credit;
  private Double showNum;
  private Double surplusNum;
  private Integer integralObtainType;
  private String integralObtainDesc;
  private Date occurrenceTime;
}
