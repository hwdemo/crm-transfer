package com.ptyt.crm.service.domain.core.coupon;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ptyt.crm.service.api.coupon.CouponMapping;
import com.ptyt.crm.service.domain.dao.coupon.CouponDao;
import com.ptyt.crm.service.domain.dao.coupon.PCouponMapping;

@Service
public class CouponService {

  @Autowired
  CouponDao couponDao;

  public CouponMapping getSrcCode(String tenantId, String platformId, String code) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(platformId, platformId);
    Assert.notNull(code, code);
    PCouponMapping perz = couponDao.getSrcCode(tenantId, platformId, code);
    if (perz == null) {
      return null;
    }
    CouponMapping record = new CouponMapping();
    BeanUtils.copyProperties(perz, record);
    return record;
  }

  public CouponMapping getByTarCode(String tenantId, String platformId, String code) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(platformId, platformId);
    Assert.notNull(code, code);
    PCouponMapping perz = couponDao.getByTarCode(tenantId, platformId, code);
    if (perz == null) {
      return null;
    }
    CouponMapping record = new CouponMapping();
    BeanUtils.copyProperties(perz, record);
    return record;
  }

  public List<CouponMapping> getsByTplId(String tenantId, String platformId, String tplId) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(platformId, platformId);
    Assert.notNull(tplId, tplId);
    List<PCouponMapping> perzs = couponDao.getsByTplId(tenantId, platformId, tplId);
    if (perzs == null) {
      return null;
    }

    List<CouponMapping> result = new ArrayList<>();
    for (PCouponMapping p : perzs) {
      CouponMapping record = new CouponMapping();
      BeanUtils.copyProperties(p, record);
      result.add(record);
    }
    return result;
  }

  public void save(String tenantId, List<CouponMapping> records) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(records, "records");

    List<PCouponMapping> perzs = new ArrayList<PCouponMapping>();
    for (CouponMapping record : records) { // TODO 批量接口
      PCouponMapping perz = couponDao.getSrcCode(tenantId, record.getSourcePlatformId(),
          record.getSourceCode());
      if (perz == null)
        perz = new PCouponMapping();
      BeanUtils.copyProperties(record, perz);
      perz.setTenantId(tenantId);
      perzs.add(perz);
    }
    couponDao.save(perzs);
  }
}
