package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwBaseResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwCounponConsumRequest implements QqwBaseRequest<QqwBaseResponse> {

  private String couponDetailNo;
  private String phone;
  private String orderNo;
  private String verificateStoreNo;

  @Override
  public String getApiMethod() {
    return "/off2on/coupon/verify";
  }

  @Override
  public Object getBody() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("couponDetailNo", couponDetailNo);
    if (phone != null)
      map.put("phone", phone);
    map.put("orderNo", orderNo);
    map.put("verificateStoreNo", verificateStoreNo);
    return map;
  }

  @Override
  public void check() {

  }

  @Override
  public Class<QqwBaseResponse> getResponseClass() {
    return QqwBaseResponse.class;
  }

  @Override
  public QqwBaseResponse getMockResponse() {
    QqwBaseResponse response = new QqwBaseResponse();
    response.setCode("0");
    return response;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> map = new HashMap<String, Object>();
    return map;
  }

}
