package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponQueryResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponQueryRequest implements HeadingBaseRequest<CouponQueryResponse> {
  private List<String> activity_ids;
  private String member_id;

  private String platform_id;
  private String state;
  private String store_id;
  private List<String> include_state;
  private String coupon_type;
  
  private Integer page = 0;
  private Integer page_size = 100;
  private String sort_key;
   private   Boolean desc=false;

  @Override
  public String getApiMethod() {
    return "/crm/couponservice/query";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    if(CollectionUtils.isEmpty(activity_ids)==false)
    query.put("activity_ids", activity_ids.toArray());
    query.put("member_id", member_id);
    query.put("platform_id", platform_id);
    query.put("state", state);
    query.put("store_id", store_id);
    if(CollectionUtils.isEmpty(include_state)==false)
    query.put("include_state", include_state.toArray());
    query.put("coupon_type", coupon_type);
    query.put("page", page);

    query.put("page_size", page_size);

    query.put("sort_key", sort_key);
    query.put("desc", desc);
    return query;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<CouponQueryResponse> getResponseClass() {
    return CouponQueryResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    return query;
  }

}
