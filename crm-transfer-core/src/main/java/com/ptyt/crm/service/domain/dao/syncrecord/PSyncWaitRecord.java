package com.ptyt.crm.service.domain.dao.syncrecord;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.ptyt.crm.service.domain.dao.base.PEntity;

@Entity
@Table(name = "ct_sync_wait_record", indexes = {
    @Index(name = "index_ct_sync_wait_record_1", columnList = "created"),
    @Index(name = "index_ct_sync_wait_record_2", columnList = "tenant_id,type,business_key") })
public class PSyncWaitRecord extends PEntity {
  private static final long serialVersionUID = -6347861086924401865L;

  private String tenantId;
  private String type;
  private String businessKey;
  private String content;
  private Date created;
  private Integer retries;
  private String remark;

  /**
   * 租户ID
   */
  @Column(name = "tenant_id", length = 128, nullable = false)
  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }

  @Column(name = "type", length = 64, nullable = false)
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Column(name = "business_key", length = 64, nullable = false)
  public String getBusinessKey() {
    return businessKey;
  }

  public void setBusinessKey(String businessKey) {
    this.businessKey = businessKey;
  }

  @Lob
  @Column(name = "content")
  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  @Column(name = "created")
  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  @Column(name = "retries")
  public Integer getRetries() {
    return retries;
  }

  public void setRetries(Integer retries) {
    this.retries = retries;
  }

  @Column(name = "remark", length = 255)
  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

}
