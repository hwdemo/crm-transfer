package com.ptyt.crm.service.domain.adapter.heading.crm;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.commons.Member;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Coupon;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.CouponActivity;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.HeadingCouponHst;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.HeadingMember;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Score;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.ScoreHst;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.client.HeadingCrmClient;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponAbortRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponAcquireRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponGetByStateRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponGetRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponHstQueryRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponQueryActivityRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponQueryRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.HeadingBaseRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.Ident;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.MemberGetRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.MemberModifyRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.MemberRegistrationRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.ScoreAdjustRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.ScoreGetRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.ScoreHstRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponAbortResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponAcquireResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponGetByStateResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponGetResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponHstQueryResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponQueryActivityResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponQueryResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.HeadingBaseResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.MemberGetResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.MemberModifyResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.MemberRegistrationResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.ScoreAdjustResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.ScoreGetResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.ScoreHstResponse;

@Service
public class HeadingCrmServices {
  private static Logger logger = LoggerFactory.getLogger(HeadingCrmServices.class);

  public static final String defalt_gatewayUrl = "http://api.u.hd123.com.com";

  public CrmResponse<Member> getMemberId(AuthInfo authInfo, String memberId) {
    CrmResponse<Member> result = new CrmResponse<Member>();
    MemberGetRequest request = new MemberGetRequest();
    request.setIdent_type("member_code");
    request.setIdent_code(memberId);
    try {
      MemberGetResponse response = excute(authInfo, request);
      Member member = new Member();
      member.setMemberId(response.getMember_id());
      member.setMobile(response.getMobile());
      member.setName(response.getName());
      member.setState(response.getName());
      result.setSuccess(true);
      result.setData(member);
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<Void> adjustScore(AuthInfo authInfo, String memberId, String tranId,
      Date tranTime, BigDecimal occur) {
    CrmResponse<Void> result = new CrmResponse<Void>();
    ScoreAdjustRequest request = new ScoreAdjustRequest();
    request.setAction("adjust");
    request.setMemberId(memberId);
    request.setXid(tranId);
    request.setTranId(tranId);
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSSZ");
    if (tranTime != null) {
      request.setTranTime(tranTime);
    }
    request.setOccur(occur);
    try {
      ScoreAdjustResponse response = excute(authInfo, request);
      result.setSuccess(true);
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<HeadingMember> registerMemberByMobile(AuthInfo authInfo, Member member) {
    CrmResponse<HeadingMember> result = new CrmResponse<HeadingMember>();
    MemberRegistrationRequest request = new MemberRegistrationRequest();
    request.setIdent(new Ident());
    request.getIdent().setType("mobile");
    request.getIdent().setCode(member.getMobile());
    request.setMobile(member.getMobile());
    request.setGender(member.getGender());
    request.setName(member.getName());
    request.setStore_id(null);

    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    if (member.getBirthday() != null) {
      try {
        request.setBirthday(
            DateUtils.parseDate(format.format(member.getBirthday()), "yyyy-MM-dd HH:mm:ss"));
      } catch (ParseException e) {
        logger.error(e.getMessage());
      }
    }
    if (member.getRegisterTime() != null) {
      try {
        request.setRegisterTime(
            DateUtils.parseDate(format.format(member.getRegisterTime()), "yyyy-MM-dd HH:mm:ss"));
      } catch (ParseException e) {
        logger.error(e.getMessage());
      }
    }
    try {
      MemberRegistrationResponse response = excute(authInfo, request);

      HeadingMember headingMember = new HeadingMember();
      headingMember.setMember_id(response.getMember_id());
      headingMember.setScore(response.getScore());
      result.setData(headingMember);
      result.setSuccess(true);
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<HeadingMember> modifyMember(AuthInfo authInfo, Member member,
      String operator) {
    CrmResponse<HeadingMember> result = new CrmResponse<HeadingMember>();
    MemberModifyRequest request = new MemberModifyRequest();
    request.setMobile(member.getMobile());
    request.setGender(member.getGender());
    request.setName(member.getName());
    request.setEmail(member.getEmail());
    request.setMemberId(member.getMemberId());
    request.setOperator(operator);
    // SimpleDateFormat format2 = new
    // SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSSZ");
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    if (member.getBirthday() != null) {
      // try {
      request.setBirthday(format.format(member.getBirthday()));
      // } catch (ParseException e) {
      // logger.error(e.getMessage());
      // }
    }

    try {
      MemberModifyResponse response = excute(authInfo, request);
      result.setSuccess(Boolean.TRUE.equals(response.getSuccess()));
      result.setMessage(response.getMessage());
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<Member> getMemberByMobile(AuthInfo authInfo, String mobile) {
    CrmResponse<Member> result = new CrmResponse<Member>();
    MemberGetRequest request = new MemberGetRequest();
    request.setIdent_code(mobile);
    request.setIdent_type("mobile");
    try {
      MemberGetResponse response = excute(authInfo, request);
      result.setSuccess(true);
      if (response.getMember_id() != null) {
        Member member = new Member();
        member.setMemberId(response.getMember_id());
        member.setMobile(response.getMobile());
        member.setName(response.getName());
        member.setState(response.getState());
        member.setBirthday(response.getBirthday());
        member.setAge(response.getAge());
        result.setData(member);
      }
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<Score> getScore(AuthInfo authInfo, String memberCode) {
    CrmResponse<Score> result = new CrmResponse<Score>();
    ScoreGetRequest request = new ScoreGetRequest();
    request.setMember_code(memberCode);
    request.setQuery_expire(true);
    try {
      ScoreGetResponse response = excute(authInfo, request);
      if (Boolean.TRUE.equals(response.getSuccess())) {
        Score member = new Score();
        member.setBalance(response.getBalance());
        member.setMemberId(null);
        result.setSuccess(true);
        result.setData(member);
      } else {
        logger.error(response.getMessage());

        result.setSuccess(false);
        result.setMessage(response.getMessage());
      }
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<List<ScoreHst>> getScoreHsts(AuthInfo authInfo, ScoreHstRequest request) {
    CrmResponse<List<ScoreHst>> result = new CrmResponse<List<ScoreHst>>();
    try {
      ScoreHstResponse response = excute(authInfo, request);
      if (Boolean.TRUE.equals(response.getSuccess())) {
        result.setSuccess(true);
        result.setData(response.getHsts());
      } else {
        logger.error(response.getMessage());
        result.setSuccess(false);
        result.setMessage(response.getMessage());
      }
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<Coupon> acquireCoupon(AuthInfo authInfo, CouponAcquireRequest request) {
    CrmResponse<Coupon> result = new CrmResponse<Coupon>();
    try {
      CouponAcquireResponse response = excute(authInfo, request);
      if (Boolean.TRUE.equals(response.getSuccess())) {
        result.setSuccess(true);
        result.setData(response.getCoupon());
      } else {
        logger.error(response.getMessage());
        result.setSuccess(false);
        result.setMessage(response.getMessage());
      }
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }
  
  public CrmResponse<Void> abortCoupon(AuthInfo authInfo, CouponAbortRequest request) {
    CrmResponse<Void> result = new CrmResponse<Void>();
    try {
      CouponAbortResponse response = excute(authInfo, request);
      if (Boolean.TRUE.equals(response.getSuccess())) {
        result.setSuccess(true);
        result.setData(null);
      } else {
        logger.error(response.getMessage());
        result.setSuccess(false);
        result.setMessage(response.getMessage());
      }
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<List<Coupon>> queryCoupon(AuthInfo authInfo, CouponQueryRequest request) {
    CrmResponse<List<Coupon>> result = new CrmResponse<List<Coupon>>();
    try {
      CouponQueryResponse response = excute(authInfo, request);
      if (Boolean.TRUE.equals(response.getSuccess())) {
        result.setSuccess(true);
        result.setData(response.getCoupons());
      } else {
        logger.error(response.getMessage());
        result.setSuccess(false);
        result.setMessage(response.getMessage());
      }
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<List<Coupon>> getCouponsByCodes(AuthInfo authInfo, CouponGetRequest request) {
    CrmResponse<List<Coupon>> result = new CrmResponse<List<Coupon>>();
    try {
      CouponGetResponse response = excute(authInfo, request);
      if (Boolean.TRUE.equals(response.getSuccess())) {
        result.setSuccess(true);
        result.setData(response.getCoupons());
      } else {
        logger.error(response.getMessage());
        result.setSuccess(false);
        result.setMessage(response.getMessage());
      }
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<List<HeadingCouponHst>> getCouponsHst(AuthInfo authInfo, String phone,
      String code) {
    CrmResponse<List<HeadingCouponHst>> result = new CrmResponse<List<HeadingCouponHst>>();
    try {
      CouponHstQueryRequest request = new CouponHstQueryRequest();
      request.setCoupon_codes(Arrays.asList(code));
      request.setMember_code(phone);
      request.setCount(100);
      request.setTran_begin_date(DateUtils.addDays(new Date(), -7));
      request.setTran_end_date(DateUtils.addDays(new Date(), +7));
      CouponHstQueryResponse response = excute(authInfo, request);
      if (Boolean.TRUE.equals(response.getSuccess())) {
        result.setSuccess(true);
        result.setData(response.getHsts());
      } else {
        logger.error(response.getMessage());
        result.setSuccess(false);
        result.setMessage(response.getMessage());
      }
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<List<Coupon>> getMemberCoupons(AuthInfo authInfo,
      CouponGetByStateRequest request) {
    CrmResponse<List<Coupon>> result = new CrmResponse<List<Coupon>>();
    try {
      CouponGetByStateResponse response = excute(authInfo, request);
      result.setSuccess(true);
      result.setData(response.getCoupons());
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage());
    }
    return result;
  }

  public CrmResponse<List<CouponActivity>> queryCouponActivity(AuthInfo authInfo,
      CouponQueryActivityRequest request) {
    CrmResponse<List<CouponActivity>> result = new CrmResponse<List<CouponActivity>>();
    try {
      CouponQueryActivityResponse response = excute(authInfo, request);
      if (Boolean.TRUE.equals(response.getSuccess())) {
        result.setSuccess(true);
        result.setData(response.getActivities());
      } else {
        logger.error(response.getMessage());
        result.setSuccess(false);
        result.setMessage(response.getMessage());
      }
    } catch (Exception e) {
      result.setSuccess(false);
      result.setMessage(e.getMessage());
      logger.error(e.getMessage(), e);
    }
    return result;
  }

  public <T extends HeadingBaseResponse> T excute(AuthInfo authInfo, HeadingBaseRequest<T> request)
      throws Exception {
    String gatewayUrl = authInfo.getConfigs().get("gatewayUrl");
    if (gatewayUrl == null) {
      gatewayUrl = defalt_gatewayUrl;
    }
    String url = gatewayUrl + "/" + authInfo.getTenantId();
    HeadingCrmClient client = new HeadingCrmClient(url, authInfo.getConfigs().get("userName"),
        authInfo.getConfigs().get("password"));
    return client.excute(request);
  }
}
