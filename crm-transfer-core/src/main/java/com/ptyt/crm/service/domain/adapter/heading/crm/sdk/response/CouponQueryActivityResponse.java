package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import java.util.ArrayList;
import java.util.List;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.CouponActivity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponQueryActivityResponse extends HeadingBaseResponse<Object> {

  private List<CouponActivity> activities=new ArrayList<CouponActivity>();

}
