package com.ptyt.crm.service.domain.dao.coupon;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import com.ptyt.crm.service.domain.dao.base.PEntity;

@Entity
@Table(name = "ct_counpon_mapping", indexes = {
    @Index(name = "index_ct_counpon_mapping_1", columnList = "tenant_id,src_ptm_id,src_code"),
    @Index(name = "index_ct_counpon_mapping_2", columnList = "tenant_id,tar_ptm_id,tar_code"),
    @Index(name = "index_ct_counpon_mapping_3", columnList = "tenant_id,src_ptm_id,source_tpl_id"),
    @Index(name = "index_ct_counpon_mapping_4", columnList = "created") })
public class PCouponMapping extends PEntity {
  private static final long serialVersionUID = -6347861086924401865L;

  private String tenantId;
  private String phone;
  private String sourceTplId;
  private String sourcePlatformId;
  private String sourceCode;
  private String targetTplId;
  private String targetPlatformId;
  private String targetCode;
  private String targetStatus;
  private Date created;

  @Column(name = "tenant_id", length = 128, nullable = false)
  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }

  @Column(name = "phone", length = 40)
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Column(name = "src_ptm_id", length = 40)
  public String getSourcePlatformId() {
    return sourcePlatformId;
  }

  public void setSourcePlatformId(String sourcePlatformId) {
    this.sourcePlatformId = sourcePlatformId;
  }

  @Column(name = "src_code", length = 64)
  public String getSourceCode() {
    return sourceCode;
  }

  public void setSourceCode(String sourceCode) {
    this.sourceCode = sourceCode;
  }

  @Column(name = "tar_ptm_id", length = 64)
  public String getTargetPlatformId() {
    return targetPlatformId;
  }

  public void setTargetPlatformId(String targetPlatformId) {
    this.targetPlatformId = targetPlatformId;
  }

  @Column(name = "tar_code", length = 64)
  public String getTargetCode() {
    return targetCode;
  }

  public void setTargetCode(String targetCode) {
    this.targetCode = targetCode;
  }

  @Column(name = "created")
  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  @Column(name = "source_tpl_id", length = 64)
  public String getSourceTplId() {
    return sourceTplId;
  }

  public void setSourceTplId(String sourceTplId) {
    this.sourceTplId = sourceTplId;
  }

  @Column(name = "target_tpl_id", length = 64)
  public String getTargetTplId() {
    return targetTplId;
  }

  public void setTargetTplId(String targetTplId) {
    this.targetTplId = targetTplId;
  }

  @Column(name = "target_status", length = 20)
  public String getTargetStatus() {
    return targetStatus;
  }

  public void setTargetStatus(String targetStatus) {
    this.targetStatus = targetStatus;
  }

}
