package com.ptyt.crm.service.domain.core.job;

import com.ptyt.crm.service.domain.infrastructure.context.TenantContext;
import com.ptyt.crm.service.domain.infrastructure.context.TracingContext;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.MDC;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.UUID;

@Slf4j
public abstract class TenantJob extends QuartzJobBean {

	private static final String TRACE_ID = "trace_id";

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		String tenantId = (String) context.getJobDetail().getJobDataMap().get("tenant");

		TenantContext.setTenantId(tenantId);
		String traceId = UUID.randomUUID().toString().replace("-", "");
		MDC.put(TRACE_ID, traceId);
		TracingContext.setTracingId(traceId);
	}

	protected String getTenantId() {
		return TenantContext.getTenantId();
	}



}
