package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Coupon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponAcquireResponse extends HeadingBaseResponse<Object> {

  private Coupon coupon;

}
