package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Score {
  private String memberId;
  private BigDecimal balance;
}
