package com.ptyt.crm.service.domain.core.job;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;

import com.ptyt.crm.service.domain.adapter.qqw.QqwConfig;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwToken;
import com.ptyt.crm.service.domain.adapter.util.AuthInfoUtil;
import com.ptyt.crm.service.domain.core.config.ConfigService;

import lombok.extern.slf4j.Slf4j;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
public class QqwTokenRefreshJob extends TenantJob {
  public static final String TASK_ID = "qqw-token-refresh";
  @Autowired
  ConfigService configService;

  @Autowired
  QqwServices qqwServices;

  @Override
  protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
    super.executeInternal(arg0);
    try {
      log.info("租户：" + getTenantId() + "开始执行：" + this.getClass().getSimpleName());
      process();
      log.info("租户：" + getTenantId() + "结束执行：" + this.getClass().getSimpleName());
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
  }

  private void process() {
    QqwConfig qqwConfig = configService.getConfig(getTenantId(), QqwConfig.NAME, QqwConfig.class);
    if (qqwConfig == null || StringUtils.isBlank(qqwConfig.getGatewayUrl())) {
      log.error("qqw配置未配置");
      return;
    }
    if (qqwConfig.getTokenExpireTime() == null
        || qqwConfig.getTokenExpireTime().compareTo(DateUtils.addMinutes(new Date(), 5)) < 0) {
      QqwToken token = qqwServices.getToken(AuthInfoUtil.buildAuthInfo(getTenantId(), qqwConfig));
      if (token == null) {
        throw new IllegalArgumentException("刷新授权失败...");
      }
      qqwConfig.setToken(token.getAccessToken());
      qqwConfig.setTokenExpireTime(DateUtils.addMinutes(new Date(), 20));
      configService.put(getTenantId(), QqwConfig.NAME, qqwConfig);
    }
  }
}
