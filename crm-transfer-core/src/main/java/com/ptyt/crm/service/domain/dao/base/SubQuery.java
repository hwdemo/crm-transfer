package com.ptyt.crm.service.domain.dao.base;


public class SubQuery {
  private String cond;
  private EntityQuery query;

  public SubQuery(String cond, EntityQuery query) {
    this.cond = cond;
    this.query = query;
  }

  public String getCond() {
    return cond;
  }

  public EntityQuery getQuery() {
    return query;
  }

}
