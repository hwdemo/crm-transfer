package com.ptyt.crm.service.domain.adapter.qqw.sdk.client;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

public class QqwUtils {
  private static final String PATTERN = "OPEN-ROAD-API-{0}-{1}-{2}";
  private static final MessageFormat messageFormat = new MessageFormat(PATTERN);

  /**
   * 生成token
   * 
   * @param str
   * @param key
   * @return
   */
  public static String generate(String appKey, String systemNo, String tokenTime, Long clientTime) {
    Object[] args = {
        systemNo, clientTime + tokenTime, appKey };
    return Base64.getEncoder().encodeToString(messageFormat.format(args).getBytes());
  }

  public static String getEncryptPhone(String phone) {
    StringBuilder sbStr = new StringBuilder();
    String nowStr = LocalDateTime.now(ZoneId.of("GMT+08:00"))
        .format(DateTimeFormatter.ofPattern("MMddHHmm"));
    String cardNoNew = nowStr + phone;
    char[] arr = cardNoNew.toCharArray();
    char[] arrNew = new char[arr.length];
    for (int i = 0; i < arr.length; i++) {
      if (i % 2 != 0) {
        arrNew[i] = arr[i - 1];
      } else {
        if (i + 1 == arr.length) {
          arrNew[i] = arr[i];
        } else {
          arrNew[i] = arr[i + 1];
        }
      }
      sbStr.append(arrNew[i]);
    }

    return "215" + sbStr.toString();
  }

  /**
   * 解密手机号
   */
  public static String getDecryptPhone(String phone) {
    StringBuilder sbStr = new StringBuilder();
    char[] arr = phone.substring(3).toCharArray();
    char[] arrNew = new char[arr.length];
    for (int i = 0; i < arr.length; i++) {
      if (i % 2 != 0) {
        arrNew[i] = arr[i - 1];
      } else {
        if (i + 1 == arr.length) {
          arrNew[i] = arr[i];
        } else {
          arrNew[i] = arr[i + 1];
        }
      }
      sbStr.append(arrNew[i]);
    }
    return sbStr.toString().substring(8);
  }
}
