package com.ptyt.crm.service.domain.adapter.weixin.sdk.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WxGetTokenResponse extends WxBaseResponse {
  @JsonProperty("access_token")
  private String accessToken;// string 获取到的凭证
  @JsonProperty("expires_in")
  private Integer expiresIn;// number 凭证有效时间，单位：秒。目前是7200秒之内的值。

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public Integer getExpiresIn() {
    return expiresIn;
  }

  public void setExpiresIn(Integer expiresIn) {
    this.expiresIn = expiresIn;
  }
}
