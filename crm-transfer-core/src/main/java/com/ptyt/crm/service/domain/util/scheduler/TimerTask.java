package com.ptyt.crm.service.domain.util.scheduler;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

public abstract class TimerTask<T> implements Runnable {
    private long seqNumber;
    private Date firstRunTime;

    public TimerTask(Date firstRunTime) {
        this.seqNumber = sequencer.incrementAndGet();
        this.firstRunTime = firstRunTime;
    }

    /**
     * 全局唯一的序号
     */
    public long getId() {
        return seqNumber;
    }

    /**
     * 任务名称
     */
    public String getName() {
        return "task-" + getId();
    }

    /**
     * 首次运行时间
     */
    public Date getFirstRunTime() {
        return firstRunTime;
    }

    /**
     * 比较时间，找最大的时间，时间null比任何时间都小。
     *
     * @param ds 时间序列
     * @return 最大时间。
     */
    protected Date maxTime(Date... ds) {
        Date max = null;
        for (Date d : ds) {
            if (d != null) {
                if (max != null && d.after(max))
                    max = d;
                else if (max == null)
                    max = d;
            }
        }
        return max;
    }

    /**
     * 计算下次运行时间。
     *
     * @param current 当前时间，not null
     * @param lastRun 上次运行时间
     * @return 下次运行时间，如果返回null表示不运行。
     */
    public abstract Date getNextTimeAfter(Date current, Date lastRun);

    // 全局序号发生器
    private static AtomicLong sequencer = new AtomicLong(1);

}
