package com.ptyt.crm.service.domain.core.weixin;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
@Getter
@Setter
public class WxConfig implements Serializable {
    public static final String NAME = "weixin";
    private static final long serialVersionUID = 4744840406384739871L;

    private String appId;// 小程序唯一标识
    private String secret;// 小程序的 app secret
    private String token;
    private Date tokenExpireTime;
}
