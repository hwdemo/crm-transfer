package com.ptyt.crm.service.domain.util.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.*;

public class Scheduler {

    public static final Logger LOG = LoggerFactory.getLogger(Scheduler.class);

    public Scheduler() {
        this.executor = new SchedExecutor(this.maxThreads, new SchedThreadFactory());
    }

    /**
     * 指定最多可以并发执行的线程数，如果调度的任务超过此数量，将进行排队处理。
     *
     * @param maxThreads
     *          最多并发线程数，必须大于0
     */
    public Scheduler(int maxThreads) {
        this.maxThreads = maxThreads;
        this.executor = new SchedExecutor(this.maxThreads, new SchedThreadFactory());
    }

    /**
     * 调度指定的任务运行。
     *
     * @param task
     *          定时任务，not null
     */
    public void sched(TimerTask task) throws InterruptedException {
        if (task == null)
            throw new IllegalArgumentException("task不能为null");

        SchedTask st = new SchedTask(task, null);
        if (st.canExecute()) {
            Future future = submit(st);
            holders.put(task.getId(), new TaskHolder(st, future));
            // 如果已经执行完了，则移除
            if (future.isDone() || future.isCancelled())
                holders.remove(task.getId());
        } else {
            LOG.debug("sched: {}, ignored", task.getName());
        }
    }

    /**
     * 停止所有的任务执行，将导致正在运行的程序停止。
     */
    public void stop() {
        LOG.debug("stop");
        // 先停止调度新任务
        executor.shutdown();

        try {
            // 等待10秒钟，如果仍然没有停止，则强制停止
            for (int index = 0; index < 10; index++) {
                if (executor.awaitTermination(2, TimeUnit.SECONDS)) {
                    return;
                }
            }
            executor.shutdownNow();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * 取消指定的任务。
     *
     * @param taskId
     *          任务ID
     */
    public void cancel(long taskId) {
        TaskHolder holder = holders.remove(taskId);
        if (holder == null) {
            LOG.trace("cancel: {} ignored", taskId);
            return;
        }

        synchronized (holder) {
            LOG.trace("cancel: {}", holder.getTaskName());
            holder.getFuture().cancel(true);
        }
    }

    /**
     * 取得当前正在运行的任务数。
     *
     * @return 运行任务数。
     */
    public int getActiveCount() {
        return ((SchedExecutor) executor).getActiveCount();
    }

    /**
     * 取得累计完成的任务数。
     *
     * @return 累计完成任务数。
     */
    public long getCompletedCount() {
        return ((SchedExecutor) executor).getCompletedTaskCount();
    }

    /**
     * 取得队列中的任务数。
     *
     * @return 队列任务数。
     */
    public int getTaskCount() {
        return holders.size();
    }

    /**
     * 取得调度器名称。
     *
     * @return 名称。
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private Future submit(SchedTask st) {
        LOG.debug("submit: {}, {}", st.getTask().getName(), st.getDelay());
        return executor.schedule(st, st.getDelay(), TimeUnit.MILLISECONDS);
    }

    private void resched(long taskId) {
        LOG.trace("resched: {}", taskId);

        TaskHolder holder = holders.get(taskId);
        if (holder == null)
            return;
        resched(holder);
    }

    private void resched(TaskHolder holder) {
        assert holder != null;
        LOG.trace("resched: {}", holder.getTaskName());

        // 如果线程池中断，则不再调度
        if (executor.isShutdown()) {
            LOG.trace("resched: {}, shutdown, ignored", holder.getTaskName());
            return;
        }

        // 如果任务取消，则移除
        if (holder.getFuture().isCancelled()) {
            LOG.trace("remove canceled task: {}", holder.getTaskName());
            holders.remove(holder.getTaskId());
            return;
        }

        // 如果任务还在运行，跳过
        if (holder.getFuture().isDone() == false) {
            return;
        }

        SchedTask st = new SchedTask(holder.getSchedTask().getTask(), holder.getSchedTask()
                .getLastRun());
        holder.setSchedTask(st);
        synchronized (holder) {
            if (st.canExecute()) { // 如果还要运行，重新调度
                holder.setFuture(submit(st));
            } else { // 否则，移除已完成的任务
                LOG.trace("remove completed task: {}", st.getTask().getName());
                holders.remove(holder.getTaskId());
            }
        }
    }

    // 调度任务
    class SchedTask implements Callable<Long> {
        private TimerTask task;
        private Date lastRun = null;
        private long trigger;

        SchedTask(TimerTask task, Date lastRun) {
            this.task = task;
            this.lastRun = lastRun;
            refreshTrigger();
        }

        private void refreshTrigger() {
            Date current = new Date(System.currentTimeMillis());
            Date nextTime = task.getNextTimeAfter(current, lastRun);
            if (nextTime != null) {
                this.trigger = nextTime.getTime();
                LOG.trace("trigger: {}, {}", task.getName(), toTimeStr(nextTime));
            } else {
                this.trigger = Long.MAX_VALUE;
                LOG.trace("trigger: {}, null", task.getName());
            }
        }

        public TimerTask getTask() {
            return task;
        }

        public Date getLastRun() {
            return lastRun;
        }

        public boolean canExecute() {
            return this.trigger != Long.MAX_VALUE;
        }

        public long getDelay() {
            return trigger - System.currentTimeMillis();
        }

        @Override
        public Long call() throws Exception {
            try {
                LOG.trace("begin run: {}", task.getName());
                lastRun = new Date();
                task.run();
            } catch (Throwable t) {
                LOG.error("", t);
            } finally {
                LOG.trace("end run: {}", task.getName());
            }
            return task.getId();
        }

    }

    // 调度线程工厂
    class SchedThreadFactory implements ThreadFactory {

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setName("DDPS-Scheduler-" + t.getName());
            LOG.trace("new thread: {}", t.getName());
            return t;
        }

    }

    // 调度多线程执行器
    class SchedExecutor extends ScheduledThreadPoolExecutor {
        public SchedExecutor(int corePoolSize, ThreadFactory threadFactory) {
            super(corePoolSize, threadFactory);
        }

        // 在任务执行后进行下一次任务调度
        protected void afterExecute(Runnable r, Throwable t) {
            super.afterExecute(r, t);
            LOG.trace("afterExecute");

            Long taskId = null;
            if (t == null && r instanceof Future) {
                Future<Long> future = (Future) r;
                try {
                    if (future.isDone())
                        taskId = future.get();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (ExecutionException e) {
                    LOG.warn("***unexpected exception***");
                    t = e.getCause();
                } catch (CancellationException e) {
                    LOG.info("canceled: " + Thread.currentThread().getName());
                }
            }
            if (t != null) {
                LOG.error("", t);
            }
            if (taskId != null) {
                LOG.trace("finished: {}", Thread.currentThread().getName());
                resched(taskId);
            }
        }
    }

    // 任务跟踪对象
    class TaskHolder {
        private SchedTask schedTask;
        private Future future;

        public TaskHolder(SchedTask schedTask, Future future) {
            this.schedTask = schedTask;
            this.future = future;
        }

        public SchedTask getSchedTask() {
            return schedTask;
        }

        public void setSchedTask(SchedTask schedTask) {
            this.schedTask = schedTask;
        }

        public Future getFuture() {
            return future;
        }

        public void setFuture(Future future) {
            this.future = future;
        }

        public String getTaskName() {
            return schedTask.getTask().getName();
        }

        public long getTaskId() {
            return schedTask.getTask().getId();
        }

    }

    private String toTimeStr(Date d) {
        if (d == null)
            return "";
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
    }

    private String name = DEFAULT_NAME;
    private int maxThreads = 1;

    private Map<Long, TaskHolder> holders = new ConcurrentHashMap();
    private ScheduledExecutorService executor = null;

    private static final String DEFAULT_NAME = "DefaultScheduler";

}
