package com.ptyt.crm.service.domain.adapter.qqw.sdk.response;

import java.util.List;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwCreditRecord;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwPullOnlineCreditRecordResponse extends QqwBaseResponse<List<QqwCreditRecord>> {
}
