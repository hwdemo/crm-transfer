package com.ptyt.crm.service.domain.core.job;

import lombok.Data;

import java.io.Serializable;

@Data
public class SyncRecordConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String NAME = "sync.record";

    private Boolean threads = false;
    private Integer maxMemberSyncThreads = 2;
    private Integer maxScoreSyncThreads = 2;
}
