package com.ptyt.crm.service.domain.adapter.qqw;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwConfig implements Serializable {
  private static final long serialVersionUID = 1L;

  public static final String NAME = "qqw";

  private boolean mock;
  private String gatewayUrl;
  private String systemNo;
  private String tokenTime;
  private String clientId;
  private String channelNo;
  private String secret;
  private String token;
  private Date tokenExpireTime;
}
