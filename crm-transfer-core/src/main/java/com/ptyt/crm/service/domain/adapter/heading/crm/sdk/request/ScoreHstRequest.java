package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.ScoreHstResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScoreHstRequest implements HeadingBaseRequest<ScoreHstResponse> {
  private String member_id;
  private String tran_id;
  private int page = 0;
  private int page_size = 100;
  private Date begin_date;
  private Date end_date;
  private List<String> sort_keys;

  @Override
  public String getApiMethod() {
    return "/crm/scoreservice/scorehst/query";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("member_id", member_id);
    if (tran_id != null)
      query.put("tran_id", tran_id);
    query.put("page", page);
    query.put("page_size", page_size);
    query.put("begin_date", begin_date);
    query.put("end_date", end_date);
    if (sort_keys != null) {
      query.put("sort_keys", sort_keys.toArray());
    }
    return query;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<ScoreHstResponse> getResponseClass() {
    return ScoreHstResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("member_id", member_id);
    if (tran_id != null)
      query.put("tran_id", tran_id);
    query.put("page", page);
    query.put("page_size", page_size);
    return query;
  }

}
