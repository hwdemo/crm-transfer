package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwTokenGetResponse;
import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class QqwTokenGetRequest
        implements QqwBaseRequest<QqwTokenGetResponse> {

    private String clientId;
    private String timestamp;
    private String algorithm;
    private String checksum;

    @Override
    public String getApiMethod() {
        return "/auth/login";
    }

    @Override
    public Object getBody() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("clientId", clientId);
        map.put("timestamp", timestamp);
        map.put("algorithm", algorithm);
        map.put("checksum", checksum);
        return map;
    }

    @Override
    public void check() {
        // TODO Auto-generated method stub

    }

    @Override
    public Class<QqwTokenGetResponse> getResponseClass() {
        return QqwTokenGetResponse.class;
    }

    @Override
    public QqwTokenGetResponse getMockResponse() {
        QqwTokenGetResponse response = new QqwTokenGetResponse();
        response.setCode("0");
        return response;
    }

    @Override
    public HttpMethod getHttpRequestMethod() {
        return HttpMethod.POST;
    }

    @Override
    public Map<String, Object> getQueryParam() {
        // TODO Auto-generated method stub
        return null;
    }

}
