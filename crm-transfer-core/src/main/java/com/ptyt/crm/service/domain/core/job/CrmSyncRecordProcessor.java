package com.ptyt.crm.service.domain.core.job;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.ptyt.crm.service.api.syncrecord.SyncRecord;
import com.ptyt.crm.service.api.syncrecord.SyncRecordResult;
import com.ptyt.crm.service.api.syncrecord.SyncWaitRecord;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.commons.Member;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Score;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.ScoreHst;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.ScoreHstRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwScore;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwUser;
import com.ptyt.crm.service.domain.core.config.ConfigService;
import com.ptyt.crm.service.domain.core.syncrecord.SyncRecordService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CrmSyncRecordProcessor {
  @Autowired
  SyncRecordService syncRecordService;
  @Autowired
  HeadingCrmServices headingCrmServices;
  @Autowired
  ConfigService configService;
  @Autowired
  QqwServices qqwServices;

  public SyncRecordResult processSyncScore(String tenantId, AuthInfo authInfo, AuthInfo qqwAuthInfo,
      SyncWaitRecord record) throws Exception {
    try {
      CrmResponse<Member> memberCrmResponse = headingCrmServices.getMemberId(authInfo,
          record.getBusinessKey());
      if (memberCrmResponse.isSuccess() == false) {
        throw new Exception(
            "查询会员" + record.getBusinessKey() + "信息失败" + memberCrmResponse.getMessage());
      }
      if (memberCrmResponse.getData() == null) {
        throw new Exception("会员" + record.getBusinessKey() + "不存在");
      }

      QqwUser qqwMember = qqwServices.getUser(qqwAuthInfo, memberCrmResponse.getData().getMobile());
      if (qqwMember == null) {
        log.error("查询QQW会员" + memberCrmResponse.getData().getMobile() + "不存在");
        SyncRecordResult recordResult = new SyncRecordResult(record.getId(),
            record.getBusinessKey(), new Date(), true,
            "查询QQW会员" + memberCrmResponse.getData().getMobile() + "不存在");
        recordResult.setIgnore(true);
        return recordResult;
      }
      String contentStr = record.getContent();
      boolean isInitial = false; // 是否初始积分
      BigDecimal initalScore = null;
      if (StringUtils.isBlank(contentStr)) { // 兼容历史数据
        isInitial = true;
      } else {
        JSONObject content = JSONObject.parseObject(contentStr);
        String action = content.getString("action");
        // 多保存手机号，方便核查问题
        content.put("mobile", qqwMember.getPhone());
        record.setContent(content.toJSONString());
        if ("initial".equals(action)) {
          isInitial = true;
          initalScore = content.getBigDecimal("occur_value");
        } else if ("check".equals(action)) {
          return checkResultScoreReuslt(tenantId, authInfo, qqwAuthInfo, record,
              memberCrmResponse.getData(), qqwMember);
        }
      }
      if (isInitial) {// 全量同步
        if (initalScore == null) {
          CrmResponse<Score> scoreCrmResponse = headingCrmServices.getScore(authInfo,
              record.getBusinessKey());
          if (scoreCrmResponse.isSuccess() == false) {
            throw new Exception(
                "查询会员" + record.getBusinessKey() + "积分信息失败" + memberCrmResponse.getMessage());
          }
          if (scoreCrmResponse.getData() == null) {
            log.info("会员" + record.getBusinessKey() + "积分海鼎不存在，初始化为0");
            initalScore = BigDecimal.ZERO;
          } else
            initalScore = scoreCrmResponse.getData().getBalance();
        }
        try {
          qqwServices.initialScore(qqwAuthInfo, qqwMember.getOfflineId(),
              memberCrmResponse.getData(), initalScore);
        } catch (Exception e) {
          if (StringUtils.isBlank(contentStr) == false && e.getMessage() != null
              && e.getMessage().contains("线下初始化积分失败：该积分账户或流水号已存在")) {
            ScoreHst scoreHst = new ScoreHst();
            scoreHst.setAction("迁移"); // OFFLINE_INTEGRAL_PRODUCE
            scoreHst.setBalance(null);
            scoreHst.setOccur(initalScore);
            scoreHst.setOrderNo(qqwMember.getOfflineId());
            scoreHst.setOriginal(null);
            scoreHst.setRemark("数据迁移");
            scoreHst.setTran_time(record.getCreated());
            qqwServices.saveScore(qqwAuthInfo, qqwMember.getOfflineId(),
                memberCrmResponse.getData(), scoreHst);
          } else {
            throw e;
          }
        }
      } else {
        JSONObject content = JSONObject.parseObject(record.getContent());
        BigDecimal scoreChangeValue = content.getBigDecimal("occur_value");
        if (scoreChangeValue == null) {
          throw new Exception("积分发生变更为空");
        }
        scoreChangeValue = scoreChangeValue.setScale(2, RoundingMode.HALF_UP);
        ScoreHstRequest scoreHstRequest = new ScoreHstRequest();
        scoreHstRequest.setMember_id(record.getBusinessKey());
        // scoreHstRequest.setTran_id(record.getBusinessKey());
        Date notifyTime = content.getDate("notify_time");
        if (notifyTime != null) {
          scoreHstRequest.setBegin_date(DateUtils.addHours(notifyTime, -1));
          scoreHstRequest.setEnd_date(DateUtils.addSeconds(notifyTime, 3));
        } else if (record.getCreated() != null) {
          scoreHstRequest.setBegin_date(DateUtils.addHours(record.getCreated(), -1));
          scoreHstRequest.setEnd_date(DateUtils.addSeconds(record.getCreated(), 3));
        } else {
          scoreHstRequest.setBegin_date(DateUtils.addHours(new Date(), -1));
          scoreHstRequest.setEnd_date(new Date());
        }
        scoreHstRequest.setPage_size(100);
        CrmResponse<List<ScoreHst>> response = headingCrmServices.getScoreHsts(authInfo,
            scoreHstRequest);
        if (response.isSuccess() == false) {
          throw new Exception("查询积分流水失败" + response.getMessage());
        }
        List<ScoreHst> syncHsts = new ArrayList<>();
        ScoreHst syncHst = null;
        for (ScoreHst hst : response.getData()) {
          if (hst.getOccur().compareTo(scoreChangeValue) == 0) {
            syncHst = hst;
            syncHsts.add(hst);
          }
        }
        if (syncHst == null) {
          throw new IllegalArgumentException("流水不存在");
        }
        for (ScoreHst hst : syncHsts) {
          if (StringUtils.isNotBlank(hst.getOrderNo())) {
            SyncRecord record1 = syncRecordService.getSyncRecord(tenantId,
                SyncWaitRecord.TYPE_QQW_SCORE, hst.getOrderNo());
            if (record1 != null) {
              log.info("积分流水在全球蛙已存在，忽略同步到全球蛙");
              continue;
              // return new SyncRecordResult(record.getBusinessKey(), new
              // Date(), true,
              // "积分流水由全球蛙同步，忽略同步到全球蛙");
            }
          }
          qqwServices.saveScore(qqwAuthInfo, qqwMember.getOfflineId(), memberCrmResponse.getData(),
              hst);
        }
      }
      return new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), true, null);
    } catch (Exception e) {
      log.error("", e);
      return new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), false,
          e.getMessage());
    }

  }

  private SyncRecordResult checkResultScoreReuslt(String tenantId, AuthInfo authInfo,
      AuthInfo qqwAuthInfo, SyncWaitRecord record, Member member, QqwUser qqwMember) {
    log.info("开始校准会员：" + member.getMobile());

    CrmResponse<Score> score = headingCrmServices.getScore(authInfo, member.getMobile());
    if (score.isSuccess() == false) {
      throw new IllegalArgumentException(score.getMessage());
    }

    if (score.getData() == null || score.getData().getBalance() == null) {
      log.info("会员：" + member.getMobile() + "线下积分为空，忽略校准");
      return new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), true,
          "线下会员积分为0，忽略校准");
    }

    BigDecimal headingScore = score.getData().getBalance();

    QqwScore qqwscore = qqwServices.getScore(qqwAuthInfo, qqwMember.getOfflineId());
    if (qqwscore == null) {
      // 积分账户不存在，初始化
      qqwServices.initialScore(qqwAuthInfo, qqwMember.getOfflineId(), member, headingScore);
    } else {
      // 积分账户存在，校准
      BigDecimal qqwscoreBalance = qqwscore.getCurrentCredit();
      BigDecimal diff = headingScore.subtract(qqwscoreBalance);
      log.info("会员：" + member.getMobile() + "线下积分：" + headingScore + "线上积分：" + qqwscore + "，校准积分："
          + diff);

      if (diff.compareTo(BigDecimal.ZERO) == 0) {
        return new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), true,
            "会员：" + member.getMobile() + "线上线下积分没有差异。");
      }

      ScoreHst hst = new ScoreHst();
      hst.setAction("人工");
      hst.setOccur(diff);
      hst.setOrderNo("CHECK" + DateFormatUtils.format(new Date(), "yyyyMMddHH"));
      hst.setRemark("积分校准");
      hst.setTran_time(new Date());
      hst.setBalance(headingScore);
      qqwServices.saveScore(qqwAuthInfo, qqwMember.getOfflineId(), member, hst);
    }

    return new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), true, null);
  }
}
