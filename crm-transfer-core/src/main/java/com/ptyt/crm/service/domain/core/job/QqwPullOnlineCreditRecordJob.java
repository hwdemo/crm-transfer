package com.ptyt.crm.service.domain.core.job;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.ptyt.crm.service.api.syncrecord.SyncWaitRecord;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmConfig;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.qqw.QqwConfig;
import com.ptyt.crm.service.domain.adapter.qqw.QqwJobConfig;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwCreditRecord;
import com.ptyt.crm.service.domain.adapter.util.AuthInfoUtil;
import com.ptyt.crm.service.domain.core.config.ConfigService;
import com.ptyt.crm.service.domain.core.syncrecord.SyncRecordService;

import lombok.extern.slf4j.Slf4j;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
public class QqwPullOnlineCreditRecordJob extends TenantJob {
  public static final String TASK_ID = "qqw-pull-online-credit-record-job";
  @Autowired
  ConfigService configService;

  @Autowired
  QqwServices qqwServices;

  @Autowired
  HeadingCrmServices headingCrmServices;

  @Autowired
  SyncRecordService syncRecordService;

  @Override
  protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
    super.executeInternal(arg0);
    try {
      log.info("租户：" + getTenantId() + "开始执行：" + this.getClass().getSimpleName());
      process();
      log.info("租户：" + getTenantId() + "结束执行：" + this.getClass().getSimpleName());
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
  }

  private void process() {
    QqwConfig qqwConfig = configService.getConfig(getTenantId(), QqwConfig.NAME, QqwConfig.class);
    if (qqwConfig == null || StringUtils.isBlank(qqwConfig.getGatewayUrl())) {
      log.error("qqw配置未配置");
      return;
    }

    HeadingCrmConfig headingCrmConfig = configService.getConfig(getTenantId(),
        HeadingCrmConfig.NAME, HeadingCrmConfig.class);
    if (headingCrmConfig == null || StringUtils.isBlank(headingCrmConfig.getGatewayUrl())) {
      log.error("heading配置未配置");
      return;
    }
    QqwJobConfig qqwJobConfig = configService.getConfig(getTenantId(), QqwJobConfig.NAME,
        QqwJobConfig.class);

    AuthInfo qqwAuthInfo = AuthInfoUtil.buildAuthInfo(getTenantId(), qqwConfig);
    Date startDate = null;
    if (qqwJobConfig == null || qqwJobConfig.getQqwPullOnlineCreditRecordJobLastTime() == null) {
      startDate = DateUtils.addDays(new Date(), -7);
    } else {
      startDate = DateUtils.addMinutes(qqwJobConfig.getQqwPullOnlineCreditRecordJobLastTime(), -5);
    }
    int page = 1;
    int pageSize = 100;
    while (true) {
      List<QqwCreditRecord> records = qqwServices.getCreditRecord(qqwAuthInfo, startDate, page,
          pageSize);
      if (records == null || records.isEmpty())
        break;
      log.info(JSON.toJSONString(records));
      for (QqwCreditRecord record : records) {
        SyncWaitRecord waitRecord = new SyncWaitRecord();
        waitRecord.setType(SyncWaitRecord.TYPE_QQW_SCORE);
        waitRecord.setBusinessKey(record.getPhone());
        waitRecord.setContent(JSON.toJSONString(record));
        syncRecordService.saveSyncWaitRecord(getTenantId(), waitRecord);
      }
      page++;
    }
    if (qqwJobConfig == null) {
      qqwJobConfig = new QqwJobConfig();
    }
    qqwJobConfig.setQqwPullOnlineCreditRecordJobLastTime(new Date());
    configService.put(getTenantId(), QqwJobConfig.NAME, qqwJobConfig);
  }
}
