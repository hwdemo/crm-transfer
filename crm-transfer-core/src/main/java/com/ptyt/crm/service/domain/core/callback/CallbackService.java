package com.ptyt.crm.service.domain.core.callback;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ptyt.crm.service.api.syncrecord.SyncRecordResult;
import com.ptyt.crm.service.api.syncrecord.SyncWaitRecord;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmConfig;
import com.ptyt.crm.service.domain.adapter.qqw.QqwConfig;
import com.ptyt.crm.service.domain.adapter.util.AuthInfoUtil;
import com.ptyt.crm.service.domain.core.config.ConfigService;
import com.ptyt.crm.service.domain.core.job.CrmSyncRecordProcessor;
import com.ptyt.crm.service.domain.core.syncrecord.SyncRecordService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CallbackService {
  @Autowired
  SyncRecordService syncRecordService;
  @Autowired
  com.ptyt.crm.service.domain.core.job.QqwSyncRecordProcessor qqwSyncRecordProcessor;
  @Autowired
  CrmSyncRecordProcessor crmSyncRecordProcessor;
  @Autowired
  ConfigService configService;

  public void processRecord(String tenantId, SyncWaitRecord record) throws Exception {
    HeadingCrmConfig config = configService.getConfig(tenantId, HeadingCrmConfig.NAME,
        HeadingCrmConfig.class);
    if (config == null || StringUtils.isBlank(config.getGatewayUrl())) {
      log.error("crm配置未配置");
      return;
    }
    QqwConfig qqwConfig = configService.getConfig(tenantId, QqwConfig.NAME, QqwConfig.class);
    if (qqwConfig == null || StringUtils.isBlank(qqwConfig.getGatewayUrl())) {
      log.error("qqw配置未配置");
      return;
    }
    AuthInfo authInfo = AuthInfoUtil.buildCrmAuthInfo(config);
    AuthInfo qqwAuthInfo = AuthInfoUtil.buildAuthInfo(tenantId, qqwConfig);
    if (SyncWaitRecord.TYPE_SCORE.equals(record.getType())) {
      SyncRecordResult recordResult = crmSyncRecordProcessor.processSyncScore(tenantId, authInfo,
          qqwAuthInfo, record);
      if (recordResult.isSuccess() == false) {
        if (recordResult.getMessage() != null && recordResult.getMessage().contains("流水不存在")) {
        } else if (recordResult.getMessage() != null
            && recordResult.getMessage().contains("未被授权")) {
        } else if (recordResult.getMessage() != null
            && recordResult.getMessage().contains("流水号已存在")) {
          recordResult.setSuccess(true);
          recordResult.setIgnore(true);
          recordResult.setMessage(recordResult.getMessage());
        } else {
          record.setRetries(3);// 不可重试
        }
        syncRecordService.saveSyncRecordResult(tenantId, record.getType(), Arrays.asList(record),
            Arrays.asList(recordResult));
        if (recordResult.isSuccess() == false) {
          throw new IllegalArgumentException(recordResult.getMessage());
        }
      }
      syncRecordService.saveSyncRecordResult(tenantId, record.getType(), Arrays.asList(record),
          Arrays.asList(recordResult));
    }
  }
}
