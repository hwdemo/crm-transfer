package com.ptyt.crm.service.domain.dao.coupon;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ptyt.crm.service.domain.dao.base.BaseDao;
import com.ptyt.crm.service.domain.dao.base.EntityQuery;
import com.ptyt.crm.service.domain.dao.base.TX;

@Repository
public class CouponDao extends BaseDao {
  @TX
  public void save(List<PCouponMapping> records) {
    for (PCouponMapping record : records) {
      record.setCreated(new Date());
      em.persist(record);
    }
    em.flush();
  }

  public PCouponMapping getSrcCode(String tenantId, String platformId, String code) {
    EntityQuery q = new EntityQuery("query").from(PCouponMapping.class.getName(), "o");
    q.where("o.tenantId= :tenantId").p("tenantId", tenantId);
    q.where("o.sourcePlatformId= :platformId").p("platformId", platformId);
    q.where("o.sourceCode= :code").p("code", code);
    List<PCouponMapping> records = q.list(em, 0, 1);
    return records.isEmpty() ? null : records.get(0);
  }

  public PCouponMapping getByTarCode(String tenantId, String platformId, String outCode) {
    EntityQuery q = new EntityQuery("query").from(PCouponMapping.class.getName(), "o");
    q.where("o.tenantId= :tenantId").p("tenantId", tenantId);
    q.where("o.targetPlatformId= :platformId").p("platformId", platformId);
    q.where("o.targetCode= :outCode").p("outCode", outCode);
    List<PCouponMapping> records = q.list(em, 0, 1);
    return records.isEmpty() ? null : records.get(0);
  }

  public List<PCouponMapping> getsByTplId(String tenantId, String platformId, String tplId) {
    EntityQuery q = new EntityQuery("query").from(PCouponMapping.class.getName(), "o");
    q.where("o.tenantId= :tenantId").p("tenantId", tenantId);
    q.where("o.sourcePlatformId= :platformId").p("platformId", platformId);
    q.where("o.sourceTplId= :sourceTplId").p("sourceTplId", tplId);
    List<PCouponMapping> records = q.list(em, 0, 1);
    return records;
  }
}
