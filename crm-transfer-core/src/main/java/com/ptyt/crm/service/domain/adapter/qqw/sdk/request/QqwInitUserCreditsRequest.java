package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwCreditInitial;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwScoreCreateCreditRecordResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwInitUserCreditsRequest
    implements QqwBaseRequest<QqwScoreCreateCreditRecordResponse> {

  private List<QqwCreditInitial> datas = new ArrayList<QqwCreditInitial>();

  @Override
  public String getApiMethod() {
    return "/off2on/credit/initUserCredits";
  }

  @Override
  public Object getBody() {
    return datas;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<QqwScoreCreateCreditRecordResponse> getResponseClass() {
    return QqwScoreCreateCreditRecordResponse.class;
  }

  @Override
  public QqwScoreCreateCreditRecordResponse getMockResponse() {
    QqwScoreCreateCreditRecordResponse response = new QqwScoreCreateCreditRecordResponse();
    response.setCode("0");
    return response;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    // TODO Auto-generated method stub
    return null;
  }

}
