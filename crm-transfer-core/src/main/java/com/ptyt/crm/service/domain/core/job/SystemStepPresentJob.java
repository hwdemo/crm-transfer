package com.ptyt.crm.service.domain.core.job;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.ptyt.crm.service.api.wxstep.Step;
import com.ptyt.crm.service.domain.core.config.ConfigService;
import com.ptyt.crm.service.domain.core.step.StepConfig;
import com.ptyt.crm.service.domain.core.step.StepService;

import lombok.extern.slf4j.Slf4j;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
public class SystemStepPresentJob extends TenantJob {

  public static final String TASK_ID = "auto-step-present-job";

  @Autowired
  ConfigService configService;

  @Autowired
  StepService stepService;

  @Override
  protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
    super.executeInternal(arg0);
    try {
      log.info("租户：" + getTenantId() + "开始执行：" + this.getClass().getSimpleName() + "...");
      process();
      log.info("租户：" + getTenantId() + "结束执行：" + this.getClass().getSimpleName() + "...");
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
  }

  private void process() throws Exception {
    StepConfig config = configService.getConfig(getTenantId(), StepConfig.NAME, StepConfig.class);
    if (config == null || Boolean.TRUE.equals(config.getAuto()) == false) {
      log.error("未开启捐赠步数...");
      return;
    }

    Date time = new Date();
    String end = config.getEndTime();// 11:30结束
    if (end != null) {
      end = DateFormatUtils.format(time, "yyyy-MM-dd ") + end + ":00";
      if (time.compareTo(DateUtils.parseDate(end, "yyyy-MM-dd HH:mm:ss")) >= 0) {
        log.info("活动结束：" + end);
        return;
      }
    }

    Date startTime = new Date();
    startTime.setHours(0);
    startTime.setMinutes(0);
    startTime.setSeconds(0);
    Date endTime = DateUtils.addDays(startTime, 1);
    endTime = DateUtils.addSeconds(endTime, -1);
    List<Step> steps = stepService.query(getTenantId(), null, startTime, endTime);
    BigDecimal total = BigDecimal.ZERO;
    for (Step s : steps) {
      if (s.getPresent() != null) {
        total = total.add(s.getPresent());
      }
    }

    if (total.compareTo(new BigDecimal(config.getMax())) >= 0) {
      log.info("超过最大上限：" + config.getMax().doubleValue());
      return;
    }

    String[] timeStrs = config.getTimeStrs().split(";");
    boolean check = false;
    for (String timeStr : timeStrs) {
      String[] range = timeStr.split("~");
      String startTimeStr = range[0];
      String endTimeStr = range[1];

      startTimeStr = DateFormatUtils.format(time, "yyyy-MM-dd ") + startTimeStr + ":00";
      endTimeStr = DateFormatUtils.format(time, "yyyy-MM-dd ") + endTimeStr + ":00";
      if (time.compareTo(DateUtils.parseDate(endTimeStr, "yyyy-MM-dd HH:mm:ss")) > 0) { // 大于结束时间
        continue;
      }
      if (time.compareTo(DateUtils.parseDate(startTimeStr, "yyyy-MM-dd HH:mm:ss")) < 0) { // 小于开始时间
        continue;
      }
      check = true;
    }

    if (check == false) {
      log.info("不符合时间段:" + JSON.toJSONString(timeStrs));
      return;
    }

    String id = "system" + DateFormatUtils.format(time, "yyyyMMdd");

    BigDecimal random = new BigDecimal(config.getSeed());
    random = random.add(new BigDecimal(new Date().getSeconds()));

    Step present = stepService.get(getTenantId(), id);

    if (present == null) {
      present = new Step();
    }

    // 1天一条记录
    present.setTenantId(getTenantId());
    present.setId(id);
    present.setPlatformId("system");
    present.setUserId(id);
    present.setUserName("系统");

    present.setStep(present.getStep() == null ? random : random.add(present.getStep()));
    present.setPresent(present.getStep());
    present.setTime(new Date());

    log.info("系统捐赠步数：" + present.getStep().doubleValue() + "，本次捐赠：" + random.doubleValue());

    stepService.save(getTenantId(), present, "system");
  }

  public static void main(String[] args) throws ParseException {
    Date time = new Date();
    String end = "23:10";// 11:30结束
    if (end != null) {
      end = DateFormatUtils.format(time, "yyyy-MM-dd ") + end + ":00";
      if (time.compareTo(DateUtils.parseDate(end, "yyyy-MM-dd HH:mm:ss")) >= 0)
        log.info("活动结束：" + end);
    }
  }

}
