package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;


public class HeadingBaseResponse<T> {

  private Boolean success;

  private String code;
  
  private String message;

  public Boolean getSuccess() {
    return success;
  }


  public void setSuccess(Boolean success) {
    this.success = success;
  }


  public String getCode() {
    return code;
  }


  public void setCode(String code) {
    this.code = code;
  }


  public String getMessage() {
    return message;
  }


  public void setMessage(String message) {
    this.message = message;
  }

}
