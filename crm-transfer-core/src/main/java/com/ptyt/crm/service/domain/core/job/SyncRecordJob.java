package com.ptyt.crm.service.domain.core.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;

import com.ptyt.crm.service.api.syncrecord.SyncErrorRecord;
import com.ptyt.crm.service.api.syncrecord.SyncRecordFilter;
import com.ptyt.crm.service.api.syncrecord.SyncRecordResult;
import com.ptyt.crm.service.api.syncrecord.SyncWaitRecord;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.commons.Member;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmConfig;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse;
import com.ptyt.crm.service.domain.adapter.qqw.QqwConfig;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;
import com.ptyt.crm.service.domain.adapter.util.AuthInfoUtil;
import com.ptyt.crm.service.domain.core.config.ConfigService;
import com.ptyt.crm.service.domain.core.syncrecord.SyncRecordService;
import com.ptyt.crm.service.domain.infrastructure.context.TenantContext;
import com.ptyt.crm.service.domain.infrastructure.context.TracingContext;
import com.ptyt.crm.service.domain.util.scheduler.RunOnceTask;
import com.ptyt.crm.service.domain.util.scheduler.Scheduler;

import lombok.extern.slf4j.Slf4j;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
public class SyncRecordJob extends TenantJob {

  public static final String TASK_ID = "sync-record";

  public static Map<String, String> QQW_MEMBER_MAP = new HashMap<>();
  public static Map<String, String> QQW_SCORE_MAP = new HashMap<>();
  public static Map<String, String> SCORE_MAP = new HashMap<>();

  static {
    QQW_MEMBER_MAP.put("type", SyncWaitRecord.TYPE_QQW_MEMBER);
    SCORE_MAP.put("type", SyncWaitRecord.TYPE_SCORE);
    QQW_SCORE_MAP.put("type", SyncWaitRecord.TYPE_QQW_SCORE);
  }

  @Autowired
  SyncRecordService syncRecordService;
  @Autowired
  HeadingCrmServices headingCrmServices;
  @Autowired
  ConfigService configService;
  @Autowired
  QqwServices qqwServices;
  @Autowired
  QqwSyncRecordProcessor qqwSyncRecordProcessor;
  @Autowired
  CrmSyncRecordProcessor crmSyncRecordProcessor;

  @Override
  protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
    super.executeInternal(arg0);
    try {
      String type = (String) arg0.getJobDetail().getJobDataMap().get("type");
      log.info(
          "租户：" + getTenantId() + "开始执行：" + this.getClass().getSimpleName() + "，类型" + type + "...");
      process(type);
      log.info(
          "租户：" + getTenantId() + "结束执行：" + this.getClass().getSimpleName() + "，类型" + type + "...");
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
  }

  private void process(String type) throws Exception {
    HeadingCrmConfig config = configService.getConfig(getTenantId(), HeadingCrmConfig.NAME,
        HeadingCrmConfig.class);
    if (config == null || StringUtils.isBlank(config.getGatewayUrl())) {
      log.error("crm配置未配置");
      return;
    }
    QqwConfig qqwConfig = configService.getConfig(getTenantId(), QqwConfig.NAME, QqwConfig.class);
    if (qqwConfig == null || StringUtils.isBlank(qqwConfig.getGatewayUrl())) {
      log.error("qqw配置未配置");
      return;
    }

    AuthInfo authInfo = buildAuthInfo(config);
    AuthInfo qqwAuthInfo = AuthInfoUtil.buildAuthInfo(getTenantId(), qqwConfig);
    processSync(type, authInfo, qqwAuthInfo);
  }

  private void processSync(String type, AuthInfo authInfo, AuthInfo qqwAuthInfo) throws Exception {
    processSyncError(type);
    SyncRecordConfig syncRecordConfig = configService.getConfig(getTenantId(),
        SyncRecordConfig.NAME, SyncRecordConfig.class);
    Scheduler scheduler = null;
    if (Boolean.TRUE.equals(syncRecordConfig.getThreads())) {
      if (SyncWaitRecord.TYPE_SCORE.equals(type)) {
        scheduler = new Scheduler(syncRecordConfig.getMaxScoreSyncThreads());
      } else if (SyncWaitRecord.TYPE_MEMBER.equals(type)) {
        scheduler = new Scheduler(syncRecordConfig.getMaxMemberSyncThreads());
      } else if (SyncWaitRecord.TYPE_QQW_MEMBER.equals(type)) {
        scheduler = new Scheduler(syncRecordConfig.getMaxMemberSyncThreads());
      } else if (SyncWaitRecord.TYPE_QQW_SCORE.equals(type)) {
        scheduler = new Scheduler(syncRecordConfig.getMaxScoreSyncThreads());
      }
    }

    List<SyncWaitRecord> records = getSyncWaitRecord(type);
    List<SyncRecordResult> results = new ArrayList<>();
    for (SyncWaitRecord record : records) {
      try {
        if (scheduler != null) {
          scheduler.sched(new ProcessOnceTask(getTenantId(), TracingContext.getTracingId(), type,
              authInfo, qqwAuthInfo, record, results));
        } else {
          processSync(type, authInfo, qqwAuthInfo, record, results);
        }
      } catch (Exception e) {
        log.error("", e);
        results.add(new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), false,
            e.getMessage()));
      }
    }
    if (scheduler != null) {
      try {
        while (scheduler.getTaskCount() > 0 || scheduler.getActiveCount() > 0) {
          Thread.sleep(10);
        }
      } catch (Exception e) {
        log.error("", e);
        throw new Exception(e);
      } finally {
        scheduler.stop();
      }
    }
    syncRecordService.saveSyncRecordResult(getTenantId(), type, records, results);
  }

  private void processSyncError(String type) {
    List<SyncErrorRecord> records = getSyncErrorRecord(type);
    if (CollectionUtils.isEmpty(records)) {
      return;
    }
    syncRecordService.recoverErrorRecord(getTenantId(), type, records);
  }

  protected class ProcessOnceTask extends RunOnceTask {

    private String tenantId;
    private String traceId;
    private String type;
    private AuthInfo authInfo;
    private AuthInfo qqwAuthInfo;
    private SyncWaitRecord record;
    List<SyncRecordResult> results;

    public ProcessOnceTask(String tenantId, String traceId, String type, AuthInfo authInfo,
        AuthInfo qqwAuthInfo, SyncWaitRecord record, List<SyncRecordResult> results) {
      this.tenantId = tenantId;
      this.traceId = traceId;
      this.type = type;
      this.authInfo = authInfo;
      this.qqwAuthInfo = qqwAuthInfo;
      this.record = record;
      this.results = results;
    }

    @Override
    public void run() {
      try {
        TenantContext.setTenantId(tenantId);
        TracingContext.setTracingId(traceId);
        processSync(type, authInfo, qqwAuthInfo, record, results);
      } catch (Exception e) {
        throw new IllegalArgumentException(e);
      }
    }
  }

  private void processSync(String type, AuthInfo authInfo, AuthInfo qqwAuthInfo,
      SyncWaitRecord record, List<SyncRecordResult> results) throws Exception {
    if (SyncWaitRecord.TYPE_MEMBER.equals(type)) {
      processSyncMember(authInfo, qqwAuthInfo, record, results);
    } else if (SyncWaitRecord.TYPE_SCORE.equals(type)) {
      SyncRecordResult recordResult = crmSyncRecordProcessor.processSyncScore(getTenantId(),
          authInfo, qqwAuthInfo, record);
      if (recordResult.isSuccess() == false) {
        if (recordResult.getMessage() != null && recordResult.getMessage().contains("流水不存在")) {
        } else if (recordResult.getMessage() != null
            && recordResult.getMessage().contains("未被授权")) {
        } else if (recordResult.getMessage() != null
            && recordResult.getMessage().contains("流水号已存在")) {
          recordResult.setSuccess(true);
          recordResult.setIgnore(true);
          recordResult.setMessage(recordResult.getMessage());
        }
      }
      results.add(recordResult);
    } else if (SyncWaitRecord.TYPE_QQW_MEMBER.equals(type)) {
      results.add(qqwSyncRecordProcessor.processSyncQqwMember(getTenantId(), authInfo, qqwAuthInfo,
          record));
    } else if (SyncWaitRecord.TYPE_QQW_SCORE.equals(type)) {
      results.add(qqwSyncRecordProcessor.processSyncQqwScoreChanged(getTenantId(), authInfo,
          qqwAuthInfo, record));
    }
  }

  private void processSyncMember(AuthInfo authInfo, AuthInfo qqwAuthInfo, SyncWaitRecord record,
      List<SyncRecordResult> results) {
    try {
      CrmResponse<Member> response = headingCrmServices.getMemberByMobile(authInfo,
          record.getBusinessKey());
      if (!response.isSuccess()) {
        throw new Exception("查询会员" + record.getBusinessKey() + "失败" + response.getMessage());
      }
      if (response.getData() == null) {
        throw new Exception("查询会员" + record.getBusinessKey() + "不存在");
      }
      qqwServices.saveUser(qqwAuthInfo, response.getData());
      results.add(
          new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), true, null));
    } catch (Exception e) {
      log.error("", e);
      results.add(new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), false,
          e.getMessage()));
    }
  }

  private AuthInfo buildAuthInfo(HeadingCrmConfig config) {
    AuthInfo authInfo = new AuthInfo();
    authInfo.setTenantId(config.getTenantId());
    authInfo.getConfigs().put("userName", config.getUserName());
    authInfo.getConfigs().put("password", config.getPassword());
    authInfo.getConfigs().put("gatewayUrl", config.getGatewayUrl());
    return authInfo;
  }

  private List<SyncWaitRecord> getSyncWaitRecord(String type) {
    SyncRecordFilter filter = new SyncRecordFilter();
    filter.setTypeEq(type);
    return syncRecordService.queryWaitRecord(getTenantId(), filter, 0, 1000, "created", false);
  }

  private List<SyncErrorRecord> getSyncErrorRecord(String type) {
    SyncRecordFilter filter = new SyncRecordFilter();
    filter.setTypeEq(type);
    filter.setRetriesMax(3);
    return syncRecordService.queryErrorRecord(getTenantId(), filter, 0, 1000, "time", false);
  }
}
