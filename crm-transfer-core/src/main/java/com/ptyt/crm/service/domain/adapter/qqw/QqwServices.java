package com.ptyt.crm.service.domain.adapter.qqw;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.commons.Member;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.ScoreHst;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwCreditInitial;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwCreditRecord;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwMember;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwScore;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwToken;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwUser;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.client.QqwCrmClient;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.client.QqwUtils;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwAddOrModifyCustomerRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwBaseRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwCounponConsumRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwCustomersGetRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwInitUserCreditsRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwOff2onUserChangePhoneRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwOff2onUserCreateRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwOff2onUserGetRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwOff2onUserModifyRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwPullOnlineCreditRecordRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwQueryUserCreditSummaryRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwScoreCreateCreditRecordRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwTokenGetRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.request.QqwWriteOffCreditRecordRequest;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwAddOrModifyCustomerResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwBaseResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwCustomersGetResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwOff2onUserCreateResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwOff2onUserGetResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwOff2onUserModifyResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwPullOnlineCreditRecordResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwQueryUserCreditRecordResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwScoreCreateCreditRecordResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwTokenGetResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwWriteOffCreditRecordResponse;
import com.ptyt.crm.service.domain.util.http.HttpResult;
import com.ptyt.crm.service.domain.util.http.HttpsClient;

@Service
public class QqwServices {
  protected static Logger logger = LoggerFactory.getLogger(QqwServices.class);

  public void saveMember(AuthInfo authInfo, Member member) {
    String channelNo = authInfo.getConfigs().get("channelNo");

    QqwAddOrModifyCustomerRequest request = new QqwAddOrModifyCustomerRequest();
    request.setChannelNo(channelNo);
    request.setPhone(member.getMobile());
    // request.setStoreNo(storeNo);
    request.setMonthNum(12);// TODO
    QqwAddOrModifyCustomerResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      throw new IllegalArgumentException(response.getMessage());
    }
  }

  @Deprecated
  public QqwMember getMember(AuthInfo authInfo, String mobile) {
    String channelNo = authInfo.getConfigs().get("channelNo");

    QqwCustomersGetRequest request = new QqwCustomersGetRequest();
    request.setChannelNo(channelNo);
    request.setCardNo(QqwUtils.getEncryptPhone(mobile));
    QqwCustomersGetResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      throw new IllegalArgumentException(response.getMessage());
    }

    return response.getData();
  }

  public void consumeCounpon(AuthInfo authInfo, String counponCode, String orderNo,
      String storeNo) {
    QqwCounponConsumRequest request = new QqwCounponConsumRequest();
    request.setCouponDetailNo(counponCode);
    // request.setPhone(phone);
    request.setOrderNo(orderNo);
    request.setVerificateStoreNo(storeNo);
    QqwBaseResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      String errorMsg = response.getMessage() != null ? response.getMessage() : response.getMsg();
      throw new IllegalArgumentException(errorMsg);
    }
  }

  public List<QqwCreditRecord> getCreditRecord(AuthInfo authInfo, Date startDate, int pageNo,
      int pageSize) {
    QqwPullOnlineCreditRecordRequest request = new QqwPullOnlineCreditRecordRequest();
    request.setPullStartTime(startDate.getTime());
    request.setPullIndex(pageNo);
    request.setPullSize(pageSize);
    QqwPullOnlineCreditRecordResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      throw new IllegalArgumentException(response.getMessage());
    }

    return response.getData();
  }

  public String saveScore(AuthInfo authInfo, String offlIneId, Member member, ScoreHst scoreHst) {
    if (BigDecimal.ZERO.compareTo(scoreHst.getOccur()) > 0) {
      return writeoffScore(authInfo, offlIneId, member, scoreHst);
    }
    QqwScoreCreateCreditRecordRequest request = new QqwScoreCreateCreditRecordRequest();
    request.setOfflineId(offlIneId);
    request.setPhone(member.getMobile());
    request.setFlowCode(scoreHst.getOrderNo());
    request.setTransactionCode(scoreHst.getOrderNo());
    request.setStoreNo(null);
    request
        .setIntegralObtainType(converterScoreActionType(scoreHst.getAction(), scoreHst.getOccur()));
    request.setIntegralObtainDesc(scoreHst.getRemark());
    request.setCredit(scoreHst.getOccur());// 发生积分？？
    if (scoreHst.getBalance() != null) {
      request.setSurplusNum(scoreHst.getBalance().doubleValue());// 结余积分
    }
    // request.setShowNum(scoreHst.getBalance());//TODO
    request.setOccurrenceTime(scoreHst.getTran_time());
    // request.setRule(rule);
    QqwScoreCreateCreditRecordResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      // 线下会员ID不一致，查全球蛙的线下会员ID进行更新
      if (response.getMessage() != null && response.getMessage().contains("线下用户唯一标识不能为空！")) {
        logger.error(response.getMessage() + "，查询全球蛙线下会员ID，重试...");
        QqwUser qqwMember = getUser(authInfo, member.getMobile());
        if (qqwMember != null) {
          if (qqwMember.getOfflineId() != null) {
            request.setOfflineId(qqwMember.getOfflineId());
            response = excute(authInfo, request);
          }
          // 尝试修改线上会员ID
          try {
            updateUserOfflineId(authInfo, qqwMember, member.getMemberId());
          } catch (Exception e) {
            logger.error(e.getMessage(), e);
          }
          if (Arrays.asList("200", "0").contains(response.getCode()))
            return response.getData().getFlowNo();
        }
      }
      throw new IllegalArgumentException(response.getMessage());
    }

    return response.getData().getFlowNo();
  }

  private String writeoffScore(AuthInfo authInfo, String offlIneId, Member member,
      ScoreHst scoreHst) {
    QqwWriteOffCreditRecordRequest request = new QqwWriteOffCreditRecordRequest();
    request.setOfflineId(offlIneId);
    request.setPhone(member.getMobile());
    request.setFlowCode(scoreHst.getOrderNo());
    request.setTransactionCode(scoreHst.getOrderNo());
    request.setStoreNo(null);
    request.setIntegralObtainType("29");
    request.setIntegralObtainDesc(scoreHst.getRemark());
    request.setCredit(scoreHst.getOccur().abs());// 发生积分？？
    if (scoreHst.getBalance() != null) {
      request.setSurplusNum(scoreHst.getBalance().doubleValue());// 结余积分
    }
    // request.setShowNum(scoreHst.getBalance());//TODO
    request.setOccurrenceTime(scoreHst.getTran_time());
    // request.setRule(rule);
    QqwWriteOffCreditRecordResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      // 线下会员ID不一致，查全球蛙的线下会员ID进行更新
      if (response.getMessage() != null && response.getMessage().contains("线下用户唯一标识不能为空！")) {
        logger.error(response.getMessage() + "，查询全球蛙线下会员ID，重试...");
        QqwUser qqwMember = getUser(authInfo, member.getMobile());
        if (qqwMember != null && qqwMember.getOfflineId() != null) {
          request.setOfflineId(qqwMember.getOfflineId());
          response = excute(authInfo, request);
          if (Arrays.asList("200", "0").contains(response.getCode()))
            return response.getData().getFlowNo();
        }
      }
      throw new IllegalArgumentException(response.getMessage());
    }

    return response.getData().getFlowNo();
  }

  public String initialScore(AuthInfo authInfo, String offlIneId, Member member, BigDecimal score) {
    if (BigDecimal.ZERO.compareTo(score) >= 0) {
      logger.info("会员：" + member.getMobile() + "积分小于等于0，忽略初始化...");
      return offlIneId;
    }
    QqwInitUserCreditsRequest request = new QqwInitUserCreditsRequest();
    QqwCreditInitial record = new QqwCreditInitial();
    record.setOfflineId(offlIneId);
    record.setPhone(member.getMobile());
    record.setFlowCode(offlIneId);
    record.setTransactionCode(offlIneId);
    record.setStoreNo(null);
    record.setIntegralObtainType(30);
    record.setIntegralObtainDesc("数据迁移");
    record.setCredit(score.doubleValue());
    record.setSurplusNum(score.doubleValue());// 结余积分
    record.setOccurrenceTime(new Date());
    request.getDatas().add(record);
    QqwScoreCreateCreditRecordResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      // 线下会员ID不一致，查全球蛙的线下会员ID进行更新
      if (response.getMessage() != null && response.getMessage().contains("线下用户唯一标识不能为空！")) {
        logger.error(response.getMessage() + "，查询全球蛙线下会员ID，重试...");
        QqwUser qqwMember = getUser(authInfo, member.getMobile());
        if (qqwMember != null) {
          if (qqwMember.getOfflineId() != null) {
            record.setOfflineId(qqwMember.getOfflineId());
            response = excute(authInfo, request);
          }
          // 尝试修改线上会员ID
          try {
            updateUserOfflineId(authInfo, qqwMember, member.getMemberId());
          } catch (Exception e) {
            logger.error(e.getMessage(), e);
          }
          if (Arrays.asList("200", "0").contains(response.getCode()))
            return response.getData().getFlowNo();
        }
      }

      logger.info("全球蛙初始化积分失败：" + response.getMessage());
      throw new IllegalArgumentException(response.getMessage());
    }

    return offlIneId;
  }

  public QqwScore getScore(AuthInfo authInfo, String offlIneId) {
    QqwQueryUserCreditSummaryRequest request = new QqwQueryUserCreditSummaryRequest();
    request.setOfflineUserId(offlIneId);
    QqwQueryUserCreditRecordResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      logger.info("全球蛙查询积分失败：" + response.getMessage());
      throw new IllegalArgumentException(response.getMessage());
    }

    return response.getData();
  }

  private String converterScoreActionType(String action, BigDecimal occurScore) {
    if (action == null)
      return "OFFLINE_INTEGRAL_PRODUCE";
    if ("积分抵现".equals(action))
      return "29";// OFFLINE_INTEGRAL_CONSUME
    else if (action.contains("抽奖"))
      return "20";// LOTTERY
    else if (action.contains("签到"))
      return "0";// CHECK_IN
    else if (action.contains("交易"))
      return "1";// 购物积分
    else if (action.contains("退款"))
      return "5";// RETURN_INTEGRAL_EXCHANGE
    else if (action.contains("生日"))
      return "6";// BIRTHDAY
    else if (action.contains("兑换积分商品"))
      return "10";// EXCHANGE_COMMODITY
    else if (action.contains("人工")) {
      if (occurScore.compareTo(BigDecimal.ZERO) > 0)
        return "11";// MANUALLY_INCREASE_INTEGRAL
      else
        return "12";// MANUALLY_DECREASE_INTEGRAL
    } else if (action.contains("充值")) {
      return "28";// RECHARGE_GIFT_INTEGRAL
    } else if (action.contains("消费")) {
      return "29";// OFFLINE_INTEGRAL_CONSUME
    } else if (action.contains("邀请")) {
      return "INVITE_FRIENDS";
    } else if (action.contains("兑换积分商品")) {
      return "EXCHANGE_COMMODITY";
    } else if (action.contains("评价")) {
      return "EVALUATE";
    } else if (action.contains("迁移")) {
      return "25";// 系统迁移产生
    }

    return "30";// OFFLINE_INTEGRAL_PRODUCE
  }

  public void saveUser(AuthInfo authInfo, Member member) {
    QqwUser user = getUser(authInfo, member.getMobile());
    if (user == null) {
      createUser(authInfo, member);
    } else {
      if (org.apache.commons.lang3.ObjectUtils.equals(user.getPhone(),
          member.getMobile()) == false) {
        logger.info(
            "会员：" + member.getMemberId() + "旧手机：" + user.getPhone() + "新手机：" + member.getMobile());
        modifyUserPhone(authInfo, member, user);
      }
      modifyUser(authInfo, member);
    }
  }

  public void updateUserOfflineId(AuthInfo authInfo, QqwUser member, String newId) {
    logger
        .info("会员：" + member.getPhone() + "新线下会员ID：" + newId + "，旧线下会员ID：" + member.getOfflineId());
    QqwOff2onUserCreateRequest request = new QqwOff2onUserCreateRequest();
    request.setOfflineId(newId);
    request.setPhone(member.getPhone());
    request.setRegistryTime(member.getRegistryTime());
    QqwOff2onUserCreateResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      throw new IllegalArgumentException(response.getMessage());
    }
  }

  private void createUser(AuthInfo authInfo, Member member) {
    QqwOff2onUserCreateRequest request = new QqwOff2onUserCreateRequest();
    request.setOfflineId(member.getMemberId());
    request.setNickName(member.getName());
    request.setFullName(member.getName());
    request.setPhone(member.getMobile());
    request.setRegistryTime(new Date());
    request.setSex(null);
    request.setBirthday(member.getBirthday());
    QqwOff2onUserCreateResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      throw new IllegalArgumentException(response.getMessage());
    }
  }

  private void modifyUser(AuthInfo authInfo, Member member) {
    QqwOff2onUserModifyRequest request = new QqwOff2onUserModifyRequest();
    request.setOfflineId(member.getMemberId());
    request.setNickName(member.getName());
    request.setFullName(member.getName());
    request.setBirthday(member.getBirthday());
    request.setSex(null);
    QqwOff2onUserModifyResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      throw new IllegalArgumentException(response.getMessage());
    }
  }

  private void modifyUserPhone(AuthInfo authInfo, Member member, QqwUser qqwUser) {
    QqwOff2onUserChangePhoneRequest request = new QqwOff2onUserChangePhoneRequest();
    request.setOfflineId(member.getMemberId());
    request.setOldPhone(qqwUser.getPhone());
    request.setNewPhone(member.getMobile());
    QqwBaseResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      throw new IllegalArgumentException(response.getMessage());
    }
  }

  public QqwUser getUser(AuthInfo authInfo, String mobile) {
    QqwOff2onUserGetRequest request = new QqwOff2onUserGetRequest();
    request.setPhone(mobile);
    QqwOff2onUserGetResponse response = excute(authInfo, request);
    if (Arrays.asList("200", "0").contains(response.getCode()) == false) {
      throw new IllegalArgumentException(response.getMessage());
    }

    return response.getData();
  }

  public <T extends QqwBaseResponse> T excute(AuthInfo authInfo, QqwBaseRequest<T> request) {
    if (authInfo.isMock()) {
      logger.info("全球蛙请求测试信息:" + JSON.toJSONString(request));
      return request.getMockResponse();
    }
    String gatewayUrl = authInfo.getConfigs().get("gatewayUrl");
    String token = authInfo.getConfigs().get("token");
    QqwCrmClient client = new QqwCrmClient(gatewayUrl, token);
    try {
      return client.excute2(request);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      throw new IllegalArgumentException(e);
    }
  }

  public QqwToken getToken(AuthInfo authInfo) {
    QqwTokenGetRequest request = new QqwTokenGetRequest();
    String clientId = authInfo.getConfigs().get("appKey");
    String timestamp = String.valueOf(new Date().getTime());
    String secret = authInfo.getConfigs().get("secret");
    String temp = clientId + ":" + timestamp;
    String checksum = null;
    try {
      Mac mac = Mac.getInstance("HmacSHA256");
      mac.init(new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256"));
      byte[] signData = mac.doFinal(temp.getBytes("UTF-8"));
      checksum = new String(Base64.encodeBase64(signData));
      request.setAlgorithm("HmacSHA256");
      request.setChecksum(checksum);
      request.setClientId(clientId);
      request.setTimestamp(timestamp);
      String url = authInfo.getConfigs().get("gatewayUrl") + request.getApiMethod();
      HttpsClient httpsClient = new HttpsClient();
      String requestBody = request.getBody() == null ? "{}"
          : JSONObject.toJSONString(request.getBody());
      HashMap<String, String> headers = new HashMap<>();
      headers.put("Content-Type", "application/json");
      HttpResult resp = httpsClient.doPost(url, requestBody, headers);
      JSONObject json = JSONObject.parseObject(resp.getBody());
      QqwTokenGetResponse response = (QqwTokenGetResponse) JSONObject.toJavaObject(json,
          request.getResponseClass());
      return response.getData();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
      throw new IllegalArgumentException(throwable.getMessage());
    }
  }
}
