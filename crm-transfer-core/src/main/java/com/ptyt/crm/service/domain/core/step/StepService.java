package com.ptyt.crm.service.domain.core.step;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ptyt.crm.service.api.wxstep.Step;
import com.ptyt.crm.service.domain.dao.base.TX;
import com.ptyt.crm.service.domain.dao.step.PStep;
import com.ptyt.crm.service.domain.dao.step.StepDao;

@Service
public class StepService {

  @Autowired
  StepDao presentDao;

  @TX
  public void save(String tenantId, Step step, String operator) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(step, "record");

    PStep perz = presentDao.getById(tenantId, step.getId());
    if (perz == null) {
      perz = new PStep();
    } else {
      if (step.getPresent() == null) { // 如果没有捐赠数量，则保留之前的捐赠数量
        step.setPresent(perz.getPresent());
      }
    }

    BeanUtils.copyProperties(step, perz);
    perz.setTenantId(tenantId);
    presentDao.save(perz, operator);
  }

  public Step get(String tenantId, String id) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(id, "id");

    PStep perz = presentDao.getById(tenantId, id);
    if (perz == null)
      return null;

    Step step = new Step();

    BeanUtils.copyProperties(perz, step);
    step.setTenantId(tenantId);
    return step;
  }

  public List<Step> query(String tenantId, String userId, Date startTime, Date endTime) {
    List<Step> result = new ArrayList<>();
    List<PStep> records = presentDao.query(tenantId, userId, startTime, endTime);
    for (PStep errorRecord : records) {
      Step record = new Step();
      BeanUtils.copyProperties(errorRecord, record);
      result.add(record);
    }
    return result;
  }
}
