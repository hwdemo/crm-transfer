package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.ScoreAdjustResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScoreAdjustRequest implements HeadingBaseRequest<ScoreAdjustResponse> {
  private String action;
  private String memberId;
  private String tranId;
  private Date tranTime;
  private String xid;
  private BigDecimal occur;

  @Override
  public String getApiMethod() {
    return "/crm/scoreservice/adjust?operator=qqw";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("action", action);
    query.put("member_id", memberId);
    query.put("tran_id", tranId);
    query.put("tran_time", tranTime);
    query.put("xid", xid);
    query.put("occur", occur);
    return query;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<ScoreAdjustResponse> getResponseClass() {
    return ScoreAdjustResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("operator", "qqw");
    return query;
  }

}
