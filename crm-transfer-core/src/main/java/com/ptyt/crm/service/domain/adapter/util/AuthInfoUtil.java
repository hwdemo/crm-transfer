package com.ptyt.crm.service.domain.adapter.util;

import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmConfig;
import com.ptyt.crm.service.domain.adapter.qqw.QqwConfig;

public class AuthInfoUtil {

    public static AuthInfo buildCrmAuthInfo(HeadingCrmConfig config) {
        AuthInfo authInfo = new AuthInfo();
        authInfo.setTenantId(config.getTenantId());
        authInfo.getConfigs().put("userName", config.getUserName());
        authInfo.getConfigs().put("password", config.getPassword());
        authInfo.getConfigs().put("gatewayUrl", config.getGatewayUrl());
        return authInfo;
    }

    public static com.ptyt.crm.service.domain.adapter.commons.AuthInfo buildAuthInfo(String tenantId, QqwConfig config) {
        com.ptyt.crm.service.domain.adapter.commons.AuthInfo authInfo = new com.ptyt.crm.service.domain.adapter.commons.AuthInfo();
        authInfo.setTenantId(tenantId);
        authInfo.setMock(config.isMock());
        authInfo.getConfigs().put("channelNo", config.getChannelNo());
        authInfo.getConfigs().put("gatewayUrl", config.getGatewayUrl());
        authInfo.getConfigs().put("appKey", config.getClientId());
        authInfo.getConfigs().put("secret", config.getSecret());
        authInfo.getConfigs().put("systemNo", config.getSystemNo());
        authInfo.getConfigs().put("tokenTime", config.getTokenTime());
        authInfo.getConfigs().put("token", config.getToken());
        return authInfo;
    }
}
