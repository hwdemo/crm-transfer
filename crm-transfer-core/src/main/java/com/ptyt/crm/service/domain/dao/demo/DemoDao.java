package com.ptyt.crm.service.domain.dao.demo;


import com.ptyt.crm.service.domain.dao.base.BaseDao;
import com.ptyt.crm.service.domain.dao.base.TX;
import org.springframework.stereotype.Repository;

@Repository
public class DemoDao extends BaseDao {

    @TX
    public void save(PDemo demo) {
        em.persist(demo);
        em.flush();
    }

}
