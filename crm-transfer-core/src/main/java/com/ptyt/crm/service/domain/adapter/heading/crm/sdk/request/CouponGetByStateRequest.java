package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponGetByStateResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponGetByStateRequest implements HeadingBaseRequest<CouponGetByStateResponse> {
  private String member_id;

  @Override
  public String getApiMethod() {
    return "/crm/couponservice/cashcoupons/get_by_state";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("member_id", member_id);
    return query;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<CouponGetByStateResponse> getResponseClass() {
    return CouponGetByStateResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.GET;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("member_id", member_id);
    return query;
  }

}
