package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberRegistrationResponse extends HeadingBaseResponse<Object>{
  
  private String member_id;
  private BigDecimal score;
}
