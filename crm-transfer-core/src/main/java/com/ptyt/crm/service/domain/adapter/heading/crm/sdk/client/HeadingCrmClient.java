package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.client;

import java.util.HashMap;
import java.util.Map;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.HeadingBaseRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.MemberGetRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.HeadingBaseResponse;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.MemberGetResponse;
import com.ptyt.crm.service.domain.util.UnicodeUtil;
import com.ptyt.crm.service.domain.util.http.HttpResult;
import com.ptyt.crm.service.domain.util.http.HttpsClient;

import io.swagger.models.HttpMethod;

public class HeadingCrmClient {
  private static Logger logger = LoggerFactory.getLogger(HeadingCrmClient.class);

  private String gatewayUrl = "http://api.u.hd123.com";
  private String userName;
  private String password;

  public HeadingCrmClient(String url, String userName, String password) {
    if (url != null)
      this.gatewayUrl = url;
    this.userName = userName;
    this.password = password;
  }

  public static void main(String[] args) throws Exception {
    String url = "https://mbr-test.qianfan123.com/testbn01";
    HeadingCrmClient client = new HeadingCrmClient(url, "guest", "guest");
    MemberGetRequest request = new MemberGetRequest();
    request.setIdent_code("13353798985");
    request.setIdent_type("mobile");
    MemberGetResponse response = client.excute(request);
    System.out.println("##############" + JSON.toJSONString(response));
  }

  public <T extends HeadingBaseResponse> T excute(HeadingBaseRequest<T> request) throws Exception {
    request.check();
    T response = null;
    String requestBody = request.getBody() == null ? "{}"
        : JSONObject.toJSONString(request.getBody());
    try {
      HttpsClient httpsClient = new HttpsClient();
      Map<String, String> headers = new HashMap<>();
      headers.put("Authorization", calcBasicAuthStr());
      headers.put("User-Agent", "notify");
      headers.put("Content-Type", "application/json");
      headers.put("trace_id", "YT" + System.currentTimeMillis());

      HttpResult resp = null;
      if (HttpMethod.GET.equals(request.getHttpRequestMethod())) {
        String url = gatewayUrl + request.getApiMethod();
        if (request.getQueryParam().isEmpty() == false) {
          StringBuffer queryParams = new StringBuffer();
          queryParams.append("?");
          for (String key : request.getQueryParam().keySet()) {
            queryParams.append(key).append("=").append(request.getQueryParam().get(key));
            queryParams.append("&");
          }
          url = url + queryParams.toString();
        }
        resp = httpsClient.doGet(url, headers);
      } else if (HttpMethod.POST.equals(request.getHttpRequestMethod())) {
        String url = gatewayUrl + request.getApiMethod();
        if (request.getQueryParam().isEmpty() == false) {
          StringBuffer queryParams = new StringBuffer();
          queryParams.append("?");
          for (String key : request.getQueryParam().keySet()) {
            queryParams.append(key).append("=").append(request.getQueryParam().get(key));
            queryParams.append("&");
          }
          url = url + queryParams.toString();
        }
        resp = httpsClient.doPost(url, requestBody, headers);
      }

      String responseBody = resp.getBody();

      JSONObject json = JSONObject.parseObject(responseBody);

      print(request.getApiMethod(), requestBody, responseBody);

      response = (T) JSONObject.toJavaObject(json, request.getResponseClass());

      return response;

    } catch (Exception e) {
      print(request.getApiMethod(), requestBody, null);
      logger.error(e.getMessage(), e);
      throw new Exception(e.getMessage(), e);
    }
  }

  private String calcBasicAuthStr() {
    String auth = userName + ":" + password;
    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes());
    return "Basic " + new String(encodedAuth);
  }

  private void print(String apiMethod, String requestBody, String reponse) {
    StringBuffer sb = new StringBuffer();
    sb.append("\r\n====================\r\n");
    sb.append("apiMethod：\r\n");
    sb.append("内容：").append(apiMethod).append("\r\n");
    sb.append("--------------------\r\n");
    sb.append("request：\r\n");
    sb.append("内容：").append(requestBody).append("\r\n");
    sb.append("--------------------\r\n");
    sb.append("response：\r\n");
    sb.append("内容：").append(UnicodeUtil.unicodeDecode(reponse)).append("\r\n");
    sb.append("====================\r\n");
    logger.info(sb.toString());
    System.out.println(sb.toString());
  }

}
