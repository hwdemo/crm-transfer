package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwCustomersGetResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwCustomersGetRequest implements QqwBaseRequest<QqwCustomersGetResponse> {

  private String channelNo;
  private String cardNo;

  @Override
  public String getApiMethod() {
    return "/standard/api/customers";
  }

  @Override
  public Object getBody() {
    return null;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<QqwCustomersGetResponse> getResponseClass() {
    return QqwCustomersGetResponse.class;
  }

  @Override
  public QqwCustomersGetResponse getMockResponse() {
    QqwCustomersGetResponse response=new QqwCustomersGetResponse();
    response.setCode("0");
    return response;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.GET;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("channelNo", channelNo);
    map.put("cardNo", cardNo);
    return map;
  }

}
