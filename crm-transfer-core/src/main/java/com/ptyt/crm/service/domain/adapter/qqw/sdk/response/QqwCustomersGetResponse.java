package com.ptyt.crm.service.domain.adapter.qqw.sdk.response;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwMember;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwCustomersGetResponse extends QqwBaseResponse<QqwMember>{
}
