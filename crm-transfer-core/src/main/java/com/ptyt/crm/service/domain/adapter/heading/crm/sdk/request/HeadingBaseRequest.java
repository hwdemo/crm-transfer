package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.HeadingBaseResponse;

import io.swagger.models.HttpMethod;

public interface HeadingBaseRequest<T extends HeadingBaseResponse> {
  public abstract HttpMethod getHttpRequestMethod();

  /** 命令 */
  public abstract String getApiMethod();

  /** 数据 */
  public abstract Object getBody();

  /** 查询参数 */
  public abstract Map<String, Object> getQueryParam();

  /** 检查数据的合法性 */
  public abstract void check();

  /** 获取响应对象类型 */
  public abstract Class<T> getResponseClass();

}