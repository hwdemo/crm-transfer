package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponHstQueryResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponHstQueryRequest implements HeadingBaseRequest<CouponHstQueryResponse> {
  private List<String> coupon_codes;
  private int count = 100;
  private String member_code;
  private Date tran_begin_date;
  private Date tran_end_date;

  @Override
  public String getApiMethod() {
    return "/crm/couponservice/couponhst/query";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("coupon_codes", coupon_codes.toArray());
    query.put("count", count);
    query.put("member_code", member_code);
    return query;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<CouponHstQueryResponse> getResponseClass() {
    return CouponHstQueryResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    return query;
  }

}
