package com.ptyt.crm.service.domain.infrastructure.context;

public class TracingContext {
    private static final ThreadLocal<String> TRACING_THREAD_LOCAL = new ThreadLocal();

    public TracingContext() {
    }

    public static void setTracingId(String tracingId) {
        TRACING_THREAD_LOCAL.set(tracingId);
    }

    public static String getTracingId() {
        return (String) TRACING_THREAD_LOCAL.get();
    }

}
