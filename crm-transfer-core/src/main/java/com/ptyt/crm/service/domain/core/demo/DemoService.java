package com.ptyt.crm.service.domain.core.demo;

import com.ptyt.crm.service.api.base.CrmResponse;
import com.ptyt.crm.service.api.demo.Demo;
import com.ptyt.crm.service.domain.dao.demo.DemoDao;
import com.ptyt.crm.service.domain.dao.demo.PDemo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DemoService {

    @Autowired
    DemoDao demoDao;

    public CrmResponse save(String tenantId, Demo demo) {
        PDemo pDemo = new PDemo();
        pDemo.setTenantId(tenantId);
        demoDao.save(pDemo);
        return new CrmResponse();
    }
}
