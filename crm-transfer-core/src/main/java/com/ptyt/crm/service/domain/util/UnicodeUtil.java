
package com.ptyt.crm.service.domain.util;

public class UnicodeUtil {

  /**
   * 把非中文字符转成unicode码
   * 
   * @param str
   * @return
   */
  public static String chineseCharacter2Unicode(String str) {
    if (str == null)
      return null;
    String result = "";
    for (int i = 0; i < str.length(); i++) {
      int chr = (char) str.charAt(i);
      if (isChineseCharacter(str.charAt(i))) {
        String hexString = Integer.toHexString(chr);
        // Now we need to zero pad it if you actually want the full 4 chars
        if (hexString.length() == 1) {
          hexString = "000" + hexString;
        } else if (hexString.length() == 2) {
          hexString = "00" + hexString;
        } else if (hexString.length() == 3) {
          hexString = "0" + hexString;
        }
        result += "\\u" + hexString;
      } else {
        result += str.charAt(i);
      }
    }

    return result;
  }

  /***
   * 验证字符是否为汉字字符(包含中文汉字和中文符号)
   * 
   * @param c
   * @return
   */
  private static boolean isChineseCharacter(char c) {
    if (c >= 128 && c <= 171941) {
      return true;
    }
    return false;
  }

  public static String unicodeDecode(String theString) {
    char aChar;
    int len = theString.length();
    StringBuffer outBuffer = new StringBuffer(len);
    for (int x = 0; x < len;) {
      aChar = theString.charAt(x++);
      if (aChar == '\\') {
        aChar = theString.charAt(x++);

        if (aChar == 'u') {
          // Read the xxxx
          int value = 0;
          for (int i = 0; i < 4; i++) {
            aChar = theString.charAt(x++);
            switch (aChar) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
              value = (value << 4) + aChar - '0';
              break;
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
              value = (value << 4) + 10 + aChar - 'a';
              break;
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
              value = (value << 4) + 10 + aChar - 'A';
              break;
            default:
              throw new IllegalArgumentException("Malformed   \\uxxxx   encoding.");
            }
          }
          outBuffer.append((char) value);
        } else {
          if (aChar == 't')
            aChar = '\t';
          else if (aChar == 'r')
            aChar = '\r';
          else if (aChar == 'n')
            aChar = '\n';
          else if (aChar == 'f')
            aChar = '\f';
          outBuffer.append(aChar);
        }
      } else
        outBuffer.append(aChar);
    }
    return outBuffer.toString();
  }

}
