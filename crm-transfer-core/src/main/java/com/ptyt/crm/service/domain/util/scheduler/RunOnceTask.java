package com.ptyt.crm.service.domain.util.scheduler;

import java.util.Date;

public abstract class RunOnceTask extends TimerTask {
    public RunOnceTask() {
        super(null);
    }

    public RunOnceTask(Date firstRunTime) {
        super(firstRunTime);
    }

    @Override
    public String getName() {
        return "runoncetask-" + getId();
    }

    // 返回当前时间，表示立刻执行
    public Date getNextTimeAfter(Date current, Date lastRun) {
        if (lastRun != null)
            return null;

        return maxTime(current, getFirstRunTime());
    }


}

