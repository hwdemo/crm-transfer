package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwPullOnlineCreditRecordResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwPullOnlineCreditRecordRequest implements QqwBaseRequest<QqwPullOnlineCreditRecordResponse> {

  private Long pullStartTime;
  private Integer pullIndex = 1;
  private Integer pullSize = 100;

  @Override
  public String getApiMethod() {
    return "/off2on/credit/pullOnlineCreditRecord";
  }

  @Override
  public Object getBody() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("pullStartTime", pullStartTime);
    map.put("pullIndex", pullIndex);
    map.put("pullSize", pullSize);

    return map;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<QqwPullOnlineCreditRecordResponse> getResponseClass() {
    return QqwPullOnlineCreditRecordResponse.class;
  }

  @Override
  public QqwPullOnlineCreditRecordResponse getMockResponse() {
    QqwPullOnlineCreditRecordResponse response = new QqwPullOnlineCreditRecordResponse();
    response.setCode("0");
    return response;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> map = new HashMap<String, Object>();
    return map;
  }

}
