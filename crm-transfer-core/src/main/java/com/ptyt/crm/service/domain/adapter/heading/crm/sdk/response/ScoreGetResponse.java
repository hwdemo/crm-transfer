package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScoreGetResponse extends HeadingBaseResponse<Object>{
  
  private BigDecimal balance;
  private String expire_date;
  private BigDecimal expire_score;
}
