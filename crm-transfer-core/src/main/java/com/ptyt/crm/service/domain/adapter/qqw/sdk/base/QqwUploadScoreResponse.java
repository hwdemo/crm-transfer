package com.ptyt.crm.service.domain.adapter.qqw.sdk.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class QqwUploadScoreResponse implements Serializable {
    private static final long serialVersionUID = -3491395742213377117L;
    private String flowNo;
}
