package com.ptyt.crm.service.domain.adapter.qqw.sdk.base;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwMember {
  private String offlineId;
  private String phone;
  private String vipCardNo;
  private Integer vipType;
  private Date expireTime;
  private String fullName;
  private String province;
  private String city;
  private String district;
  private String address;
  private String sex;
  private String customerLogo;
  private Date birthday;
  private String nickName;
  private String email;
  private String cardNo;
  private Date registryTime;

}
