package com.ptyt.crm.service.domain.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ObjectUtils;

public class DefaultSerializer {

	private static DefaultSerializer instance = null;

	public static DefaultSerializer getInstance() {
		if (instance == null)
			instance = new DefaultSerializer();
		return instance;
	}

	public Map<String, String> serialize(Object config) {
		Map<String, String> result = new HashMap();

		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(config.getClass());
			PropertyDescriptor[] props = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor prop : props) {
				if ("class".equals(prop.getName()))
					continue;
				Object value = prop.getReadMethod().invoke(config, new Object[] {});
				result.put(prop.getName(), valueToString(value));
			}
		} catch (Exception e) {
			throw new IllegalStateException("序列化 " + config + " 出错。", e);
		}
		return result;
	}

	private PropertyDescriptor findProp(PropertyDescriptor[] props, String propName) {
		for (PropertyDescriptor prop : props) {
			if (ObjectUtils.equals(prop.getName(), propName))
				return prop;
		}
		return null;
	}

	public <T> T deserialize(Map<String, String> configs, Class<T> configClass) {
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(configClass);
			PropertyDescriptor[] props = beanInfo.getPropertyDescriptors();

			Object config = configClass.newInstance();
			Set<String> propNames = configs.keySet();
			for (String propName : propNames) {
				PropertyDescriptor prop = findProp(props, propName);
				if (prop != null) {
					Object value = stringToValue(configs.get(propName), prop.getPropertyType());
					prop.getWriteMethod().invoke(config, new Object[] { value });
				}
			}
			return (T) config;
		} catch (Exception e) {
			throw new IllegalStateException("反序列化 " + configClass + " 出错。", e);
		}
	}

	private String valueToString(Object value) {
		if (value == null)
			return null;
		else if (value instanceof String)
			return (String) value;
		else if (value instanceof Date)
			return new SimpleDateFormat(DATE_FORMAT).format((Date) value);
		else
			return value.toString();
	}

	private Object stringToValue(String str, Class valueType) {
		if (str == null) {
			return null;
		} else if ("int".equals(valueType.getName())) {
			return Integer.parseInt(str);
		} else if ("short".equals(valueType.getName())) {
			return Short.parseShort(str);
		} else if ("long".equals(valueType.getName())) {
			return Long.parseLong(str);
		} else if ("double".equals(valueType.getName())) {
			return Double.parseDouble(str);
		} else if ("float".equals(valueType.getName())) {
			return Float.parseFloat(str);
		} else if ("boolean".equals(valueType.getName())) {
			return Boolean.parseBoolean(str);
		} else if (String.class.equals(valueType)) {
			return str;
		} else if (Date.class.equals(valueType)) {
			try {
				return new SimpleDateFormat(DATE_FORMAT).parse(str);
			} catch (ParseException e) {
				throw new IllegalArgumentException("将 " + str + " 解析为日期错误。", e);
			}
		} else if (Integer.class.equals(valueType)) {
			return Integer.valueOf(Integer.parseInt(str));
		} else if (Short.class.equals(valueType)) {
			return Short.valueOf(Short.parseShort(str));
		} else if (Long.class.equals(valueType)) {
			return Long.valueOf(Long.parseLong(str));
		} else if (Double.class.equals(valueType)) {
			return Double.valueOf(Double.parseDouble(str));
		} else if (Float.class.equals(valueType)) {
			return Float.valueOf(Float.parseFloat(str));
		} else if (BigDecimal.class.equals(valueType)) {
			return BigDecimal.valueOf(Double.parseDouble(str));
		} else if (Boolean.class.equals(valueType)) {
			return Boolean.valueOf(Boolean.parseBoolean(str));
		} else {
			throw new UnsupportedOperationException("不支持字段类型：" + valueType);
		}
	}

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
