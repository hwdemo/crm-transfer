package com.ptyt.crm.service.domain.infrastructure.context;

public class TenantContext {
    private static final ThreadLocal<String> holder = new ThreadLocal();
    public TenantContext() {
    }

    public static void setTenantId(String tenantId) {
        holder.set(tenantId);
    }

    public static String getTenantId() {
        return (String) holder.get();
    }

    public static void clear() {
        holder.remove();
    }
}
