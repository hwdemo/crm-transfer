package com.ptyt.crm.service.domain.dao.mapping;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ptyt.crm.service.domain.dao.base.BaseDao;
import com.ptyt.crm.service.domain.dao.base.EntityQuery;
import com.ptyt.crm.service.domain.dao.base.TX;

@Repository
public class MappingDao extends BaseDao {
  @TX
  public void save(List<PMapping> records) {
    for (PMapping record : records) {
      record.setCreated(new Date());
      em.persist(record);
    }
    em.flush();
  }

  public PMapping getSrcCode(String tenantId, String type, String sourceType, String sourceId) {
    EntityQuery q = new EntityQuery("query").from(PMapping.class.getName(), "o");
    q.where("o.tenantId= :tenantId").p("tenantId", tenantId);
    q.where("o.type= :type").p("type", type);
    q.where("o.sourceType= :sourceType").p("sourceType", sourceType);
    q.where("o.sourceId= :sourceId").p("sourceId", sourceId);
    List<PMapping> records = q.list(em, 0, 1);
    return records.isEmpty() ? null : records.get(0);
  }

  public PMapping getByTarCode(String tenantId, String type, String targetType, String targetId) {
    EntityQuery q = new EntityQuery("query").from(PMapping.class.getName(), "o");
    q.where("o.tenantId= :tenantId").p("tenantId", tenantId);
    q.where("o.type= :type").p("type", type);
    q.where("o.targetType= :targetType").p("targetType", targetType);
    q.where("o.targetId= :targetId").p("targetId", targetId);
    List<PMapping> records = q.list(em, 0, 1);
    return records.isEmpty() ? null : records.get(0);
  }
}
