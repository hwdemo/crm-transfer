package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import java.util.List;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Coupon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponGetResponse extends HeadingBaseResponse<Object> {

  private List<Coupon> coupons;

}
