package com.ptyt.crm.service.domain.core.job;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.ptyt.crm.service.api.syncrecord.SyncRecordResult;
import com.ptyt.crm.service.api.syncrecord.SyncWaitRecord;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.commons.Member;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.HeadingMember;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwCreditRecord;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwUser;
import com.ptyt.crm.service.domain.core.config.ConfigService;
import com.ptyt.crm.service.domain.core.syncrecord.SyncRecordService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class QqwSyncRecordProcessor {
  @Autowired
  SyncRecordService syncRecordService;
  @Autowired
  HeadingCrmServices headingCrmServices;
  @Autowired
  ConfigService configService;
  @Autowired
  QqwServices qqwServices;
  @Autowired
  CrmSyncRecordProcessor crmSyncRecordProcessor;

  public SyncRecordResult processSyncQqwScoreChanged(String tenantId, AuthInfo authInfo,
      AuthInfo qqwAuthInfo, SyncWaitRecord record) {
    try {

      JSONObject content = JSONObject.parseObject(record.getContent());
      QqwCreditRecord r = JSONObject.toJavaObject(content, QqwCreditRecord.class);

      String mobile = r.getPhone();

      BigDecimal occer = new BigDecimal(r.getCredit());
      if (Arrays.asList(5, 12, 26).contains(r.getIntegralObtainType())) {
        occer = occer.negate();
      }
      QqwUser qqwMember = qqwServices.getUser(qqwAuthInfo, mobile);
      if (qqwMember == null) {
        throw new Exception("查询会员" + mobile + "不存在");
      }
      CrmResponse<Member> response = headingCrmServices.getMemberByMobile(authInfo, mobile);
      if (!response.isSuccess()) {
        throw new Exception("查询会员" + mobile + "失败" + response.getMessage());
      }
      if (response.getData() == null) {
        throw new Exception("查询会员" + mobile + "不存在");
      }

      CrmResponse<Void> response1 = headingCrmServices.adjustScore(authInfo,
          response.getData().getMemberId(), r.getFlowNo(), r.getOccurrenceTime(), occer);
      if (!response1.isSuccess()) {
        throw new Exception(
            "调整会员" + response.getData().getMemberId() + "积分失败" + response1.getMessage());
      }
      return new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), true, null);
    } catch (Exception e) {
      log.error("", e);
      return new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), false,
          e.getMessage());
    }
  }

  public SyncRecordResult processSyncQqwMember(String tenantId, AuthInfo authInfo,
      AuthInfo qqwAuthInfo, SyncWaitRecord record) {
    try {
      QqwUser qqwMember = qqwServices.getUser(qqwAuthInfo, record.getBusinessKey());
      if (qqwMember == null) {
        throw new Exception("查询会员" + record.getBusinessKey() + "不存在");
      }
      CrmResponse<Member> response = headingCrmServices.getMemberByMobile(authInfo,
          record.getBusinessKey());
      if (!response.isSuccess()) {
        throw new Exception("查询会员" + record.getBusinessKey() + "失败" + response.getMessage());
      }
      String memberId = null;
      if (response.getData() == null) {
        // 注册

        Member member = new Member();
        member.setMobile(qqwMember.getPhone());
        member.setRegisterTime(qqwMember.getRegistryTime());
        CrmResponse<HeadingMember> response1 = headingCrmServices.registerMemberByMobile(authInfo,
            member);
        if (!response1.isSuccess()) {
          throw new Exception("注册会员" + record.getBusinessKey() + "失败" + response1.getMessage());
        }
        memberId = response1.getData().getMember_id();
      } else {
        memberId = response.getData().getMemberId();
      }
      // 保存到积分同步记录
      SyncWaitRecord scoreRecord = new SyncWaitRecord();
      scoreRecord.setType(SyncWaitRecord.TYPE_SCORE);
      scoreRecord.setBusinessKey(memberId);
      scoreRecord.setRemark("初始化全球蛙积分");

      SyncRecordResult recordResult = crmSyncRecordProcessor.processSyncScore(tenantId, authInfo,
          qqwAuthInfo, scoreRecord);
      if (recordResult.isSuccess() == false) {
        throw new Exception(recordResult.getMessage());
      }
      // syncRecordService.saveSyncWaitRecord(tenantId, scoreRecord);
      return new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), true, null);
    } catch (Exception e) {
      log.error("", e);
      return new SyncRecordResult(record.getId(), record.getBusinessKey(), new Date(), false,
          e.getMessage());
    }
  }
}
