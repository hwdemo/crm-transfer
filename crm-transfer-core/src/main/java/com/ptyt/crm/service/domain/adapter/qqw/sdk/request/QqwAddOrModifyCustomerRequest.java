package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwAddOrModifyCustomerResponse;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwScoreCreateCreditRecordResponse;
import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwAddOrModifyCustomerRequest
    implements QqwBaseRequest<QqwAddOrModifyCustomerResponse> {

  private String channelNo;
  private String storeNo;
  private String phone;
  private int monthNum;

  @Override
  public String getApiMethod() {
    return "/standard/api/customer/addOrModifyCustomer";
  }

  @Override
  public Object getBody() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("channelNo", channelNo);
    map.put("phone", phone);
    map.put("monthNum", monthNum);
    if (storeNo != null)
      map.put("storeNo", storeNo);
    return map;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<QqwAddOrModifyCustomerResponse> getResponseClass() {
    return QqwAddOrModifyCustomerResponse.class;
  }

  @Override
  public QqwAddOrModifyCustomerResponse getMockResponse() {
    QqwAddOrModifyCustomerResponse response=new QqwAddOrModifyCustomerResponse();
    response.setCode("0");
    return response;
  }
  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    // TODO Auto-generated method stub
    return null;
  }

}
