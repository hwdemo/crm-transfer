package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response;

import java.util.List;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.HeadingCouponHst;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponHstQueryResponse extends HeadingBaseResponse<Object> {

  private List<HeadingCouponHst> hsts;

}
