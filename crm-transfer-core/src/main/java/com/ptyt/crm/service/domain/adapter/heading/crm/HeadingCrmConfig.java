package com.ptyt.crm.service.domain.adapter.heading.crm;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HeadingCrmConfig implements Serializable{
  private static final long serialVersionUID = 1L;

  public static final String NAME = "heading.crm";

  private String tenantId;
  private String gatewayUrl;
  private String userName;
  private String password;
}
