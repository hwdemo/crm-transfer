package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponGetResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponGetRequest implements HeadingBaseRequest<CouponGetResponse> {
  private List<String> coupon_codes;

  @Override
  public String getApiMethod() {
    return "/crm/couponservice/bbw/coupons/by_code";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("coupon_codes", coupon_codes.toArray());
    return query;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<CouponGetResponse> getResponseClass() {
    return CouponGetResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    return query;
  }

}
