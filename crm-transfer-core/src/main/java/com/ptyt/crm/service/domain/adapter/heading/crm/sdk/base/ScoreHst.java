package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScoreHst {
  private String action;
  private BigDecimal balance;
  private BigDecimal occur;
  private BigDecimal original;
  private Date tran_time;
  private String orderNo;
  private String remark;
}
