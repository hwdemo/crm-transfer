package com.ptyt.crm.service.domain.adapter.qqw.sdk.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwOff2onUserCreateResponse extends QqwBaseResponse<Object>{
}
