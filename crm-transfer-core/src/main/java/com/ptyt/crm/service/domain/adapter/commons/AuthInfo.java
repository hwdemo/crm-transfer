package com.ptyt.crm.service.domain.adapter.commons;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthInfo {
    public static final String CONFIG_KEY_APPKEY = "appKey";
    public static final String CONFIG_KEY_SECRETKEY = "secretKey";
    public static final String CONFIG_KEY_SESSIONKEY = "sessionKey";
    public static final String CONFIG_KEY_REFRESHTOKEN = "refreshToken";
    public static final String CONFIG_KEY_GATEWAYURL = "gatewayUrl";

    private String tenantId;
    private String platformId;
    private boolean mock = false;
    private Map<String, String> configs = new HashMap<String, String>();
}
