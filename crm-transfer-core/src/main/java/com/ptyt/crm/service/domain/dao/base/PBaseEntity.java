package com.ptyt.crm.service.domain.dao.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class PBaseEntity extends PEntity {
  private static final long serialVersionUID = -3738964349862529867L;

  private Date created;
  private Date modified;
  private String creator;
  private String modifier;

  @Column(name = "created", nullable = false)
  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  @Column(name = "modified", nullable = false)
  public Date getModified() {
    return modified;
  }

  public void setModified(Date modified) {
    this.modified = modified;
  }

  @Column(name = "creator", length = 64)
  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }

  @Column(name = "modifier", length = 64)
  public String getModifier() {
    return modifier;
  }

  public void setModifier(String modifier) {
    this.modifier = modifier;
  }

  public void onCreated(String operator) {
    this.created = new Date();
    this.creator = operator;
    this.modified = new Date();
    this.modifier = operator;
  }

  public void onModified(String operator) {
    this.modified = new Date();
    this.modifier = operator;
  }
}
