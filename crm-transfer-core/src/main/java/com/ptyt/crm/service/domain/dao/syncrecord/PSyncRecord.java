package com.ptyt.crm.service.domain.dao.syncrecord;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.ptyt.crm.service.domain.dao.base.PEntity;

@Entity
@Table(name = "ct_sync_record", indexes = {
        @Index(name = "index_ct_sync_record_1", columnList = "time"), @Index(name = "index_ct_sync_record_2", columnList = "tenant_id,type,business_key")})
public class PSyncRecord extends PEntity {
    private static final long serialVersionUID = -6347861086924401865L;

    private String tenantId;
    private String type;
    private String businessKey;
    private String content;
    private Date time;
    private String message;

    /**
     * 租户ID
     */
    @Column(name = "tenant_id", length = 128, nullable = false)
    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    @Column(name = "type", length = 64, nullable = false)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "business_key", length = 64, nullable = false)
    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    @Lob
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "time")
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Column(name = "message", length = 1024)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
