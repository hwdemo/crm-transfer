package com.ptyt.crm.service.domain.adapter.qqw;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class QqwJobConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String NAME = "qqw.job";
    private Date QqwPullOnlineCreditRecordJobLastTime;
}
