package com.ptyt.crm.service.domain.adapter.qqw.sdk.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.qqw.sdk.base.QqwCreditInitial;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwQueryUserCreditRecordResponse;
import com.ptyt.crm.service.domain.adapter.qqw.sdk.response.QqwScoreCreateCreditRecordResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QqwQueryUserCreditSummaryRequest
    implements QqwBaseRequest<QqwQueryUserCreditRecordResponse> {

  private String offlineUserId;

  @Override
  public String getApiMethod() {
    return "/off2on/credit/queryUserCreditSummary";
  }

  @Override
  public Object getBody() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("offlineUserId", offlineUserId);
    return map;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<QqwQueryUserCreditRecordResponse> getResponseClass() {
    return QqwQueryUserCreditRecordResponse.class;
  }

  @Override
  public QqwQueryUserCreditRecordResponse getMockResponse() {
    QqwQueryUserCreditRecordResponse response = new QqwQueryUserCreditRecordResponse();
    response.setCode("0");
    return response;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    // TODO Auto-generated method stub
    return null;
  }

}
