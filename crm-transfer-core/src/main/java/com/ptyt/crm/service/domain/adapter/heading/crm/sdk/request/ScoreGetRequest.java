package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.ScoreGetResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScoreGetRequest implements HeadingBaseRequest<ScoreGetResponse> {
  private String member_code;
  private Boolean query_expire;

  @Override
  public String getApiMethod() {
    return "/crm/scoreservice/balance2";
  }

  @Override
  public Object getBody() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<ScoreGetResponse> getResponseClass() {
    return ScoreGetResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.GET;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("member_code", member_code);
    if (query_expire != null)
      query.put("query_expire", query_expire);
    return query;
  }

}
