package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CouponAcquireResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CouponAcquireRequest implements HeadingBaseRequest<CouponAcquireResponse> {
  private String activity_id;
  private String member_id;
  private String mobile;
  private BigDecimal quantity;
  private String store_id;
  private String tran_id;
  private String union_id;

  @Override
  public String getApiMethod() {
    return "/crm/couponservice/coupon/acquire";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("activity_id", activity_id);
    query.put("member_id", member_id);
    query.put("mobile", mobile);
    query.put("quantity", quantity);
    query.put("store_id", store_id);
    query.put("tran_id", tran_id);
    query.put("union_id", union_id);
    return query;
  }

  @Override
  public void check() {
    // TODO Auto-generated method stub

  }

  @Override
  public Class<CouponAcquireResponse> getResponseClass() {
    return CouponAcquireResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    return query;
  }

}
