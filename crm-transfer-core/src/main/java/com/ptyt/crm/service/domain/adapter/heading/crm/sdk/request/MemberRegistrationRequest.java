package com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.MemberRegistrationResponse;

import io.swagger.models.HttpMethod;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberRegistrationRequest implements HeadingBaseRequest<MemberRegistrationResponse> {
  private Ident ident;
  private String mobile;
  private String gender;
  private String name;
  private String store_id;
  private Date birthday;
  private Date registerTime;

  @Override
  public String getApiMethod() {
    return "/crm/identservice/ident/register";
  }

  @Override
  public Object getBody() {
    Map<String, Object> query = new HashMap<String, Object>();
    query.put("ident", JSON.toJSON(ident));
    query.put("mobile", mobile);
    query.put("gender", gender);
    query.put("name", name);
    query.put("store_id", store_id);
    query.put("birthday", birthday);
    query.put("register_time", registerTime);
    return query;
  }

  @Override
  public void check() {

  }

  @Override
  public Class<MemberRegistrationResponse> getResponseClass() {
    return MemberRegistrationResponse.class;
  }

  @Override
  public HttpMethod getHttpRequestMethod() {
    return HttpMethod.POST;
  }

  @Override
  public Map<String, Object> getQueryParam() {
    Map<String, Object> query = new HashMap<String, Object>();
    return query;
  }

}
