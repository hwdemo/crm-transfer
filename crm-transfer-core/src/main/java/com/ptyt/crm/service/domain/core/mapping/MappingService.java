package com.ptyt.crm.service.domain.core.mapping;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.ptyt.crm.service.api.mapping.Mapping;
import com.ptyt.crm.service.domain.dao.mapping.MappingDao;
import com.ptyt.crm.service.domain.dao.mapping.PMapping;

@Service
public class MappingService {

  @Autowired
  MappingDao mappingDao;

  public Mapping getSrcCode(String tenantId, String type, String platformId, String code) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(platformId, platformId);
    Assert.notNull(code, code);
    PMapping perz = mappingDao.getSrcCode(tenantId, type, platformId, code);
    if (perz == null) {
      return null;
    }
    Mapping record = new Mapping();
    BeanUtils.copyProperties(perz, record);
    return record;
  }

  public Mapping getByTarCode(String tenantId, String type, String platformId, String code) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(platformId, platformId);
    Assert.notNull(code, code);
    PMapping perz = mappingDao.getByTarCode(tenantId, type, platformId, code);
    if (perz == null) {
      return null;
    }
    Mapping record = new Mapping();
    BeanUtils.copyProperties(perz, record);
    return record;
  }

  public void save(String tenantId, List<Mapping> records) {
    Assert.notNull(tenantId, "tenantId");
    Assert.notNull(records, "records");

    List<PMapping> perzs = new ArrayList<PMapping>();
    for (Mapping record : records) {
      PMapping perz = new PMapping();
      BeanUtils.copyProperties(record, perz);
      perz.setTenantId(tenantId);
      perzs.add(perz);
    }
    mappingDao.save(perzs);
  }
}
