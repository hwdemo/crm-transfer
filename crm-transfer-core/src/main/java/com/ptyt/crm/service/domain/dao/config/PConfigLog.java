package com.ptyt.crm.service.domain.dao.config;

import com.ptyt.crm.service.domain.dao.base.PEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "ct_config_log")
public class PConfigLog extends PEntity {

  private static final long serialVersionUID = 4518033052010019179L;
  private String appName;
  private String env;
  private String name;
  private String message;
  private Date ftime;

  /** 应用名 */
  @Column(name = "app_name", length = 64, nullable = false)
  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  /** 环境 */
  @Column(name = "env", length = 64, nullable = false)
  public String getEnv() {
    return env;
  }

  public void setEnv(String env) {
    this.env = env;
  }

  /** 配置名 */
  @Column(name = "name", length = 128, nullable = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  /** 配置名 */
  @Column(name = "message", length = 1024)
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  /** 时间 */
  @Column(name = "ftime", nullable = false)
  public Date getFtime() {
    return ftime;
  }

  public void setFtime(Date ftime) {
    this.ftime = ftime;
  }
}
