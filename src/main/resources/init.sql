-- 全球蛙
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'true','mock',NOW(),'qqw','test01');
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'1','gatewayUrl',NOW(),'qqw','test01');
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'1','systemNo',NOW(),'qqw','test01');
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'1','tokenTime',NOW(),'qqw','test01');
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'1','clientId',NOW(),'qqw','test01');
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'1','channelNo',NOW(),'qqw','test01');
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'1','secret',NOW(),'qqw','test01');

-- 营销
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'testbn01','tenantId',NOW(),'heading.crm','test01');
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'https://mbr-test.qianfan123.com','gatewayUrl',NOW(),'heading.crm','test01');
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'guest','userName',NOW(),'heading.crm','test01');
INSERT into `ct_config`(uuid,data,fkey,modified,name,tenant_id) values (UUID(),'guest','password',NOW(),'heading.crm','test01');
