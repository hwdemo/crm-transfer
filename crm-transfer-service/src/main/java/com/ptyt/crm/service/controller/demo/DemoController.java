package com.ptyt.crm.service.controller.demo;

import com.ptyt.crm.service.api.base.CrmResponse;
import com.ptyt.crm.service.api.demo.Demo;
import com.ptyt.crm.service.api.demo.DemoApi;
import com.ptyt.crm.service.domain.core.demo.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController implements DemoApi {

    @Autowired
    DemoService demoService;

    @Override
    public CrmResponse<String> save(String tenantId, Demo demo, String operator) {
        demoService.save(tenantId, demo);
        return new CrmResponse();
    }
}
