package com.ptyt.crm.service.quartz;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
public class JobServices {

    public void submitJob(Scheduler jobContainer, String tenantId, Class jobClass, String jobName,
                          String defaultCron, Boolean defaultEnabled) throws SchedulerException {
        submitJob(jobContainer, tenantId, jobClass, jobName, defaultCron, defaultEnabled, null, 5);
    }

    public void delete(Scheduler jobContainer, String tenantId, String jobName)
            throws SchedulerException {
        JobKey key = new JobKey(jobName, tenantId);
        JobDetail detail = jobContainer.getJobDetail(key);
        if (detail != null) {
            log.info("刪除任务：" + key.getGroup() + "_" + key.getName());
            jobContainer.deleteJob(key);
        }
    }


    public void submitJob(Scheduler jobContainer, String tenantId, Class jobClass, String jobName,
                          String defaultCron, Boolean defaultEnabled, Map<String, String> params, int priority) throws SchedulerException {
        JobKey key = new JobKey(jobName, tenantId);
        JobDetail detail = jobContainer.getJobDetail(key);
        boolean enabled = defaultEnabled;
        String cron = defaultCron;
        if (detail != null) {
            List triggers = jobContainer.getTriggersOfJob(key);
            if (Boolean.FALSE.equals(enabled)) { // 已经取消了
                log.info("刪除任务：" + key.getGroup() + "_" + key.getName());
                jobContainer.deleteJob(key);
                return;
            } else {
                for (Object trigger : triggers) {
                    if (trigger instanceof CronTrigger) {
                        CronTrigger cronTrigger = (CronTrigger) trigger;
                        Trigger.TriggerState triggerState = jobContainer.getTriggerState(cronTrigger.getKey());
                        if (Trigger.TriggerState.ERROR.equals(triggerState)) {
                            jobContainer.deleteJob(key); // 重启
                        } else {
                            if (cronTrigger.getCronExpression().equals(cron)) {
                                return;
                            }
                            log.info("刪除任务：" + key.getGroup() + "_" + key.getName());
                            jobContainer.deleteJob(key); // 周期变化了
                        }
                    }
                }
            }
        }

        if (Boolean.FALSE.equals(enabled))
            return;

        Date taskStartDate = new Date();
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cron)
                .withMisfireHandlingInstructionFireAndProceed();
        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(jobName, tenantId)
                .withSchedule(cronScheduleBuilder).startAt(taskStartDate).withPriority(priority).build();
        JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobName, tenantId)
                .storeDurably(true).usingJobData("tenant", tenantId).build();
        if (params != null && params.isEmpty() == false) {
            jobDetail.getJobDataMap().putAll(params);
        }
        log.info("提交任务：" + key.getGroup() + "_" + key.getName());
        jobContainer.scheduleJob(jobDetail, cronTrigger);
        return;
    }

}
