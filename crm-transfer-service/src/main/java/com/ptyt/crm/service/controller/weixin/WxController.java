package com.ptyt.crm.service.controller.weixin;

import com.ptyt.crm.service.api.base.CrmResponse;
import com.ptyt.crm.service.api.weixin.WxApi;
import com.ptyt.crm.service.api.weixin.WxUserInfo;
import com.ptyt.crm.service.domain.core.weixin.WxService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class WxController implements WxApi {
    @Autowired
    WxService wxService;

    @Override
    public CrmResponse<WxUserInfo> getOpenId(String aTenantId, String code) throws Exception {
        CrmResponse<WxUserInfo> response = new CrmResponse<>();
        response.setData(wxService.getOpenId(aTenantId, code));
        return response;
    }
}
