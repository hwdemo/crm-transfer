package com.ptyt.crm.service.controller.step;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.ptyt.crm.service.api.base.CrmResponse;
import com.ptyt.crm.service.api.wxstep.Step;
import com.ptyt.crm.service.api.wxstep.StepApi;
import com.ptyt.crm.service.api.wxstep.StepInfo;
import com.ptyt.crm.service.api.wxstep.StepInfoGetRequest;
import com.ptyt.crm.service.api.wxstep.StepPresentRequest;
import com.ptyt.crm.service.domain.core.step.StepService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class StepController implements StepApi {

  @Autowired
  private StepService stepService;

  @Override
  public CrmResponse present(String tenantId, StepPresentRequest request) throws Exception {
    CrmResponse response = new CrmResponse<>();
    try {

      Date endTime = new Date();
      endTime.setHours(12);
      endTime.setMinutes(0);
      endTime.setSeconds(0);

      if (new Date().after(endTime)) {
        response.setSuccess(false);
        response.setMessage("活动已结束，感谢您的参与！");
        return response;
      }

      Step present = new Step();
      present.setTenantId(tenantId);
      // 1天一条记录
      present.setId(request.getOpenId() + DateFormatUtils.format(request.getTime(), "yyyyMMdd"));
      present.setPlatformId("weapp");
      present.setUserId(request.getOpenId());
      present.setUserName(request.getNickName());
      present.setStep(request.getStep());
      present.setPresent(request.getStep());
      present.setTime(request.getTime());
      stepService.save(tenantId, present, request.getNickName());
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      response.setSuccess(false);
      response.setMessage("系统异常");
    }

    return response;
  }

  @Override
  public CrmResponse<StepInfo> getStepInfo(String tenantId, StepInfoGetRequest request)
      throws Exception {
    CrmResponse<StepInfo> resonse = new CrmResponse<>();
    try {
      // 先保存最新数据
      saveStep(tenantId, request);

      Date startTime = new Date();
      startTime.setHours(0);
      startTime.setMinutes(0);
      startTime.setSeconds(0);
      Date endTime = DateUtils.addDays(startTime, 1);
      endTime = DateUtils.addSeconds(endTime, -1);

      // 今日步数：当前用户，今日的步数。
      // 今日排名：当前用户，今日的步数排名
      // 今日捐赠：当前用户，今日捐赠的步数
      // 捐赠总数：所有捐赠的用户，总捐赠数

      // 捐赠信息
      BigDecimal total = BigDecimal.ZERO;
      BigDecimal present = BigDecimal.ZERO;
      // 运动信息
      BigDecimal sort = BigDecimal.ZERO;
      BigDecimal step = request.getStep();

      // 查今日步数情况
      List<Step> presents = stepService.query(tenantId, null, startTime, endTime);
      for (Step p : presents) {
        if (p.getPresent() != null) {
          total = total.add(p.getPresent());
        }
        if (ObjectUtils.equals(p.getUserId(), request.getOpenId())) {
          if (p.getPresent() != null)
            present = present.add(p.getPresent());
        }
      }

      sort = getTodaySort(tenantId, request.getOpenId(), presents);

      StepInfo stepInfo = new StepInfo();
      stepInfo.setOpenId(request.getOpenId());
      stepInfo.setStep(step);
      stepInfo.setSort(sort);
      stepInfo.setPresent(present);
      stepInfo.setTotal(total);

      resonse.setData(stepInfo);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      resonse.setSuccess(false);
      resonse.setMessage("系统异常，刷新重试！！！");
    }
    return resonse;
  }

  private void saveStep(String tenantId, StepInfoGetRequest request) {
    if (request.getStep() == null)
      return;
    Step present = new Step();
    present.setTenantId(tenantId);
    present.setId(request.getOpenId() + DateFormatUtils.format(new Date(), "yyyyMMdd"));

    present.setPlatformId("weapp");
    present.setUserId(request.getOpenId());
    present.setUserName(request.getNickName());
    present.setStep(request.getStep());
    present.setTime(new Date());
    stepService.save(tenantId, present, request.getOpenId());
  }

  private BigDecimal getTodaySort(String tenantId, String openId, List<Step> today) {
    BigDecimal sort = BigDecimal.ZERO;
    // 今日运动情况
    today.sort(new Comparator<Step>() {
      @Override
      public int compare(Step o1, Step o2) {
        return o2.getStep().compareTo(o1.getStep());
      }
    });

    // 只算有效的
    List<Step> valids = new ArrayList<Step>();
    for (Step step : today) {
      if ("weapp".equals(step.getPlatformId()))
        valids.add(step);
    }

    int i = 0;
    for (Step p : valids) {
      i++;
      if (ObjectUtils.equals(p.getUserId(), openId)) {
        sort = new BigDecimal(i);
        if (BigDecimal.ZERO.compareTo(p.getStep()) == 0)
          return BigDecimal.ZERO;
      }
    }

    return sort;
  }
}
