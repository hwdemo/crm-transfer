package com.ptyt.crm.service.quartz;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import com.ptyt.crm.service.domain.core.job.*;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.ptyt.crm.service.api.syncrecord.SyncWaitRecord;
import com.ptyt.crm.service.domain.dao.tenant.PTenant;
import com.ptyt.crm.service.domain.dao.tenant.TenantDao;

@Configuration
public class QuartzConfig {
  @Autowired
  DataSource dataSource;

  @Autowired
  TenantDao tenantDao;

  @Bean
  public SchedulerFactoryBean schedulerFactoryBean(QuartzJobFactory quartzJobFactory)
      throws Exception {
    SchedulerFactoryBean factoryBean = new SchedulerFactoryBean();
    factoryBean.setJobFactory(quartzJobFactory);
    factoryBean.setConfigLocation(new ClassPathResource("quartz.properties"));
    factoryBean.setDataSource(dataSource);
    factoryBean.afterPropertiesSet();

    submitJobs(factoryBean);

    factoryBean.getScheduler().start();
    return factoryBean;
  }

  private void submitJobs(SchedulerFactoryBean factoryBean) throws SchedulerException {
    try {
      List<PTenant> tenants = tenantDao.getAll();
      for (PTenant tenant : tenants) {
        submitJob(factoryBean.getScheduler(), tenant.getTenantId());
      }
    } catch (Exception e) {
      throw new IllegalArgumentException("启动job失败：" + e.getMessage(), e);
    }
  }

  public void submitJob(Scheduler jobContainer, String tenantId) throws SchedulerException {
    // submitJob(jobContainer, tenantId, SyncRecordJob.class,
    // SyncRecordJob.TASK_ID + SyncWaitRecord.TYPE_MEMBER,
    // "0 0/1 * * * ?", true, SyncRecordJob.MEMBER_MAP);
    submitJob(jobContainer, tenantId, SyncRecordJob.class,
        SyncRecordJob.TASK_ID + SyncWaitRecord.TYPE_SCORE, "0 0/1 * * * ?", true,
        SyncRecordJob.SCORE_MAP);
    submitJob(jobContainer, tenantId, SyncRecordJob.class,
        SyncRecordJob.TASK_ID + SyncWaitRecord.TYPE_QQW_MEMBER, "0 0/1 * * * ?", true,
        SyncRecordJob.QQW_MEMBER_MAP);
    submitJob(jobContainer, tenantId, SyncRecordJob.class,
        SyncRecordJob.TASK_ID + SyncWaitRecord.TYPE_QQW_SCORE, "0 0/1 * * * ?", true,
        SyncRecordJob.QQW_SCORE_MAP);

    submitJob(jobContainer, tenantId, QqwTokenRefreshJob.class, QqwTokenRefreshJob.TASK_ID,
        "0 0/1 * * * ?", true, new HashMap<>());

    submitJob(jobContainer, tenantId, QqwPullOnlineCreditRecordJob.class,
        QqwPullOnlineCreditRecordJob.TASK_ID, "0 0/5 * * * ?", true, new HashMap<>());

    submitJob(jobContainer, tenantId, SystemStepPresentJob.class, SystemStepPresentJob.TASK_ID,
        "0 0/5 * * * ?", true, new HashMap<>());
    submitJob(jobContainer, tenantId, WXRefreshTokenJob.class, WXRefreshTokenJob.TASK_ID,
            "0 0/1 * * * ?", true, new HashMap<>());
  }

  private void submitJob(Scheduler jobContainer, String tenantId, Class jobClass, String jobName,
      String defaultCron, Boolean defaultEnabled, Map<String, String> params)
      throws SchedulerException {
    new JobServices().submitJob(jobContainer, tenantId, jobClass, jobName, defaultCron,
        defaultEnabled, params, 5);
  }

  @Bean
  public Scheduler scheduler(SchedulerFactoryBean schedulerFactoryBean) throws Exception {
    Scheduler scheduler = schedulerFactoryBean.getScheduler();
    scheduler.start();
    return scheduler;
  }

  @Bean
  public Properties quartzProperties() throws IOException {
    PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
    propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));

    // 在quartz.properties中的属性被读取并注入后再初始化对象
    propertiesFactoryBean.afterPropertiesSet();
    return propertiesFactoryBean.getObject();
  }
}
