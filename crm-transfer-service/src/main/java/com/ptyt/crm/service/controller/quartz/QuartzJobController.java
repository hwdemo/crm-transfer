package com.ptyt.crm.service.controller.quartz;

import com.ptyt.crm.service.api.quartz.QuartzJobApi;
import com.ptyt.crm.service.api.quartz.QuartzTaskInfoVo;
import com.ptyt.crm.service.api.quartz.QuatrzTaskInfoVoPaging;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@Slf4j
public class QuartzJobController implements QuartzJobApi {

  @Autowired
  private Scheduler scheduler;

  @Override
  public QuatrzTaskInfoVoPaging list(String tenant, int page, int pageSize) {
    QuatrzTaskInfoVoPaging resultVO = new QuatrzTaskInfoVoPaging();
    try {
      List<QuartzTaskInfoVo> list = new ArrayList<>();
      for (String groupJob : scheduler.getJobGroupNames()) {
        for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.<JobKey> groupEquals(groupJob))) {
          if (!tenant.equals(jobKey.getGroup())) {
            continue;
          }
          List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
          for (Trigger trigger : triggers) {
            Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
            JobDetail jobDetail = scheduler.getJobDetail(jobKey);
            String cronExpression = "", createTime = "";
            if (trigger instanceof CronTrigger) {
              CronTrigger cronTrigger = (CronTrigger) trigger;
              cronExpression = cronTrigger.getCronExpression();
              createTime = cronTrigger.getDescription();
            }
            QuartzTaskInfoVo info = new QuartzTaskInfoVo();
            info.setJobName(jobKey.getName());
            info.setTenant(jobKey.getGroup());
            info.setJobDescription(jobDetail.getDescription());
            info.setJobStatus(triggerState.name());
            info.setCronExpression(cronExpression);
            info.setCreateTime(createTime);
            info.setPreviousFireTime(trigger.getPreviousFireTime());
            info.setNextFireTime(trigger.getNextFireTime());
            list.add(info);
          }
        }
      }
      resultVO.setTotal(list.size());
      resultVO.setRows(
          list.stream().skip((page - 1) * pageSize).limit(pageSize).collect(Collectors.toList()));
    } catch (SchedulerException e) {
      log.error("分页查询定时任务失败，page={},size={},e={}", page, pageSize, e);
    }

    return resultVO;

  }

  @Override
  public void addJob(String tenant, String jobName, String jobClass, String cronExpression,
      String jobDescription) throws Exception {
    try {
      log.info("添加jobName={},tenant={},jobClass={},cronExpression={},jobDescription={}", jobName,
          tenant, jobClass, cronExpression, jobDescription);
      if (checkExists(jobName, tenant)) {
        log.error("Job已经存在, jobName={},tenant={}", jobName, tenant);
        throw new Exception(String.format("Job已经存在, jobName={},tenant={}", jobName, tenant));
      }
      CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression)
          .withMisfireHandlingInstructionFireAndProceed();
      CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(jobName, tenant)
          .withSchedule(cronScheduleBuilder).startAt(new Date()).build();
      Class<? extends Job> clazz = (Class<? extends Job>) Class.forName(jobClass);
      JobDetail jobDetail = JobBuilder.newJob(clazz).withIdentity(jobName, tenant)
          .storeDurably(true).usingJobData("tenant", tenant).build();
      scheduler.scheduleJob(jobDetail, cronTrigger);
    } catch (SchedulerException | ClassNotFoundException e) {
      log.error("添加job失败, jobName={},tenant={},e={}", jobName, tenant, e);
      throw new Exception("类名不存在或执行表达式错误");
    }

  }

  @Override
  public void edit(String tenant, String jobName, String jobClass, String cronExpression,
      String jobDescription) throws Exception {
    try {
      log.info("修改jobName={},tenant={},cronExpression={},jobDescription={}", jobName, tenant,
          cronExpression, jobDescription);
      if (!checkExists(jobName, tenant)) {
        log.error("Job不存在, jobName={},tenant={}", jobName, tenant);
        throw new Exception(String.format("Job不存在, jobName={},tenant={}", jobName, tenant));
      }
      JobKey jobKey = new JobKey(jobName, tenant);
      CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression)
          .withMisfireHandlingInstructionFireAndProceed();
      CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(jobName, tenant)
          .withSchedule(cronScheduleBuilder).startAt(new Date()).build();
      JobDetail jobDetail = scheduler.getJobDetail(jobKey);
      jobDetail.getJobBuilder().withDescription(jobDescription);
      HashSet<Trigger> triggerSet = new HashSet<>();
      triggerSet.add(cronTrigger);
      scheduler.scheduleJob(jobDetail, triggerSet, true);
    } catch (SchedulerException e) {
      log.error("修改job失败, jobName={},tenant={},e={}", jobName, tenant, e);
      throw new Exception("类名不存在或执行表达式错误");
    }
  }

  @Override
  public void delete(String tenant, String jobName) throws Exception {

    try {
      log.info("删除jobName={},tenant={}", jobName, tenant);
      TriggerKey triggerKey = TriggerKey.triggerKey(jobName, tenant);
      if (checkExists(jobName, tenant)) {
        scheduler.pauseTrigger(triggerKey);
        scheduler.unscheduleJob(triggerKey);
        // 删除任务
        scheduler.deleteJob(JobKey.jobKey(jobName,tenant));
      }
    } catch (SchedulerException e) {
      log.error("删除job失败, jobName={},tenant={},e={}", jobName, tenant, e);
      throw new Exception(e.getMessage());
    }

  }

  @Override
  public void pause(String tenant, String jobName) throws Exception {
    try {
      log.info("暂停jobName={},tenant={}", jobName, tenant);
      TriggerKey triggerKey = TriggerKey.triggerKey(jobName, tenant);
      if (!checkExists(jobName, tenant)) {
        log.error("Job不存在, jobName={},tenant={}", jobName, tenant);
        throw new Exception(String.format("Job不存在, jobName={},tenant={}", jobName, tenant));
      }
      scheduler.pauseTrigger(triggerKey);
    } catch (SchedulerException e) {
      log.error("暂停job失败, jobName={},tenant={},e={}", jobName, tenant, e);
      throw new Exception(e.getMessage());
    }

  }

  @Override
  public void resume(String tenant, String jobName) throws Exception {
    try {
      log.info("重启jobName={},tenant={}", jobName, tenant);
      TriggerKey triggerKey = TriggerKey.triggerKey(jobName, tenant);
      if (!checkExists(jobName, tenant)) {
        log.error("Job不存在, jobName={},tenant={}", jobName, tenant);
        throw new Exception(String.format("Job不存在, jobName={},tenant={}", jobName, tenant));
      }
      scheduler.resumeTrigger(triggerKey);
    } catch (SchedulerException e) {
      log.error("重启job失败, jobName={},tenant={},e={}", jobName, tenant, e);
      throw new Exception(e.getMessage());
    }

  }

  @Override
  public void trigger(String tenant, String jobName) throws Exception {
    try {
      log.info("立即执行jobName={},tenant={}", jobName, tenant);
      if (!checkExists(jobName, tenant)) {
        log.error("Job不存在, jobName={},tenant={}", jobName, tenant);
        throw new Exception(String.format("Job不存在, jobName={},tenant={}", jobName, tenant));
      }
      JobKey jobKey = new JobKey(jobName, tenant);
      scheduler.triggerJob(jobKey);
    } catch (SchedulerException e) {
      log.error("立即执行job失败, jobName={},tenant={},e={}", jobName, tenant, e);
      throw new Exception(e.getMessage());
    }
  }

  /**
   * 验证是否存在
   *
   * @param jobName
   * @param tenant
   * @throws SchedulerException
   */
  private boolean checkExists(String jobName, String tenant) throws SchedulerException {
    TriggerKey triggerKey = TriggerKey.triggerKey(jobName, tenant);
    return scheduler.checkExists(triggerKey);
  }
}
