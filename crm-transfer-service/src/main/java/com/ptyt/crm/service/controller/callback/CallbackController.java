package com.ptyt.crm.service.controller.callback;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.ptyt.crm.service.api.base.CrmResponse;
import com.ptyt.crm.service.api.callback.CallbackApi;
import com.ptyt.crm.service.api.coupon.CouponMapping;
import com.ptyt.crm.service.api.syncrecord.SyncWaitRecord;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmConfig;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.qqw.QqwConfig;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;
import com.ptyt.crm.service.domain.adapter.util.AuthInfoUtil;
import com.ptyt.crm.service.domain.core.callback.CallbackService;
import com.ptyt.crm.service.domain.core.config.ConfigService;
import com.ptyt.crm.service.domain.core.coupon.CouponService;
import com.ptyt.crm.service.domain.core.syncrecord.SyncRecordService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class CallbackController implements CallbackApi {

  public static String TOPIC_MEMBER_CREATED = "member.created";
  public static String TOPIC_SCORE_CHANGED = "score.changed";

  public static String TOPIC_QQW_MEMBER_CREATED = "qqw.member.created";
  public static String TOPIC_QQW_SCORE_CHANGED = "qqw.score.changed";

  @Autowired
  SyncRecordService syncRecordService;
  @Autowired
  CallbackService callbackService;

  @Autowired
  QqwServices qqwServices;

  @Autowired
  ConfigService configService;

  @Autowired
  CouponService couponService;

  @Autowired
  HeadingCrmServices headingCrmServices;

  @Override
  public CrmResponse<Void> test(String tenantId, HttpServletRequest request) {
    return CrmResponse.ok();
  }

  @Override
  public CrmResponse<Void> receiveMessage(String tenantId, HttpServletRequest request) {
    try {
      InputStream is = request.getInputStream();
      List<String> lines = IOUtils.readLines(is, "UTF-8");
      if (lines.isEmpty()) {
        throw new Exception("请求body不能为空");
      }
      String bodyStr = StringUtils.join(lines, "");
      log.info("接收请求信息:" + bodyStr);
      JSONObject body = JSONObject.parseObject(bodyStr);
      String topic = body.getString("topic");
      if (StringUtils.isBlank(topic)) {
        return CrmResponse.fail("缺少topic");
      }
      SimpleDateFormat fromat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String time = body.getString("time");
      Date created = null;
      if (StringUtils.isNotBlank(time)) {
        try {
          created = fromat.parse(time);
        } catch (Exception e) {
          log.error("", e);
        }
      }
      if (TOPIC_MEMBER_CREATED.equals(topic)) {
        processMemberCreated(tenantId, body.getJSONObject("content"));
      } else if (TOPIC_SCORE_CHANGED.equals(topic)) {
        processScoreCreated(tenantId, body.getJSONObject("content"), created);
      } else if ("coupon.consumed".equals(topic)) {
        processCounponConsume(tenantId, body, created);
      } else {
        return CrmResponse.fail("不支持的topic:" + topic);
      }
    } catch (Exception e) {
      log.error("", e);
      return CrmResponse.fail(e.getMessage());
    }
    return CrmResponse.ok();
  }

  private void processCounponConsume(String tenantId, JSONObject body, Date created)
      throws Exception {
    JSONObject content = body.getJSONObject("content");
    log.info("接收到回调信息：" + content.toJSONString());
    if (content == null) {
      throw new Exception("content不能为空");
    }

    String coupon_code = content.getString("coupon_code");
    if (StringUtils.isBlank(coupon_code)) {
      throw new Exception("coupon_code");
    }

    QqwConfig qqwConfig = configService.getConfig(tenantId, QqwConfig.NAME, QqwConfig.class);
    if (qqwConfig == null || StringUtils.isBlank(qqwConfig.getGatewayUrl())) {
      log.error("未配置全球蛙授权");
      throw new Exception("未配置海鼎授权");
    }

    HeadingCrmConfig headingCrmConfig = configService.getConfig(tenantId, HeadingCrmConfig.NAME,
        HeadingCrmConfig.class);
    if (headingCrmConfig == null || StringUtils.isBlank(headingCrmConfig.getGatewayUrl())) {
      log.error("未配置全球蛙授权");
      throw new Exception("未配置海鼎授权");
    }

    // 取券映射关系
    CouponMapping couponMapping = couponService.getByTarCode(tenantId, "HEADING", coupon_code);

    String qqwCounponCode = couponMapping == null ? null : couponMapping.getSourceCode();
    if (qqwCounponCode == null) {
      throw new Exception("全球蛙券关系不存在。");
    }
    if ("已核销".equals(couponMapping.getTargetStatus()) == false) { // 不是核销，先保存为核销
      couponMapping.setTargetStatus("已核销");
      couponService.save(tenantId, Arrays.asList(couponMapping));
    }

    // AuthInfo headingAuth = AuthInfoUtil.buildCrmAuthInfo(headingCrmConfig);
    // com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse<List<HeadingCouponHst>>
    // hsts = headingCrmServices
    // .getCouponsHst(headingAuth, couponMapping.getPhone(),
    // couponMapping.getTargetCode());
    // if (hsts.isSuccess() == false) {
    // throw new IllegalArgumentException(hsts.getMessage());
    // }
    //
    // if (hsts.getData() == null || hsts.getData().isEmpty()) {
    // throw new IllegalArgumentException("券流水为空");
    // }
    // boolean hasHst = false;
    // for (HeadingCouponHst hst : hsts.getData()) {
    // if ("verification".equals(hst.getAction()) == false)
    // continue;
    // hasHst = true;
    // String phone = couponMapping.getPhone();
    // String storeNo = hst.getVerify_store_id(); // FIXME
    // AuthInfo qqwAuthInfo = AuthInfoUtil.buildAuthInfo(tenantId, qqwConfig);
    // qqwServices.consumeCounpon(qqwAuthInfo, qqwCounponCode, hst.getTranId(),
    // storeNo);
    // }
    // if (hasHst == false) {
    // throw new IllegalArgumentException("券核销流水记录为空");
    // }

    AuthInfo qqwAuthInfo = AuthInfoUtil.buildAuthInfo(tenantId, qqwConfig);
    qqwServices.consumeCounpon(qqwAuthInfo, qqwCounponCode, null, null);
  }

  private void processScoreCreated(String tenantId, JSONObject content, Date created)
      throws Exception {
    if (content == null) {
      throw new Exception("content不能为空");
    }
    String memberId = content.getString("member_id");
    if (StringUtils.isBlank(memberId)) {
      throw new Exception("member_id");
    }
    BigDecimal scoreChangeValue = content.getBigDecimal("occur_value");
    if (scoreChangeValue == null) {
      throw new Exception("积分发生变更为空");
    }
    if (BigDecimal.ZERO.compareTo(scoreChangeValue) >= 0) {
      return;
    }
    SyncWaitRecord record = new SyncWaitRecord();
    record.setType(SyncWaitRecord.TYPE_SCORE);
    record.setBusinessKey(memberId);
    record.setCreated(created);
    content.put("notify_time", created);
    record.setContent(content.toJSONString());
    callbackService.processRecord(tenantId, record);
    // syncRecordService.saveSyncWaitRecord(tenantId, record);
  }

  private void processMemberCreated(String tenantId, JSONObject content) throws Exception {
    if (content == null) {
      throw new Exception("content不能为空");
    }
    String mobile = content.getString("mobile");
    if (StringUtils.isBlank(mobile)) {
      throw new Exception("mobile");
    }
    SyncWaitRecord record = new SyncWaitRecord();
    record.setType(SyncWaitRecord.TYPE_MEMBER);
    record.setBusinessKey(mobile);
    record.setContent(content.toJSONString());
    syncRecordService.saveSyncWaitRecord(tenantId, record);
  }
}
