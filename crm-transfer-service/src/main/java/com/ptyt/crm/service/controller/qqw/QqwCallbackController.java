package com.ptyt.crm.service.controller.qqw;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ptyt.crm.service.api.coupon.CouponMapping;
import com.ptyt.crm.service.api.mapping.Mapping;
import com.ptyt.crm.service.api.qqw.QqwAbortCouponRequest;
import com.ptyt.crm.service.api.qqw.QqwCallbackApi;
import com.ptyt.crm.service.api.qqw.QqwCallbackResponse;
import com.ptyt.crm.service.api.qqw.QqwQueryByPhoneCallbackData;
import com.ptyt.crm.service.api.qqw.QqwQueryByPhoneCallbackResponse;
import com.ptyt.crm.service.api.qqw.QqwQueryByPhoneRequest;
import com.ptyt.crm.service.api.qqw.QqwUserAcquireCouponRequest;
import com.ptyt.crm.service.api.qqw.QqwUserChangePhoneRequest;
import com.ptyt.crm.service.api.qqw.QqwUserCreateCallbackData;
import com.ptyt.crm.service.api.qqw.QqwUserCreateCallbackResponse;
import com.ptyt.crm.service.api.qqw.QqwUserCreateRequest;
import com.ptyt.crm.service.api.qqw.QqwUserModRequest;
import com.ptyt.crm.service.api.qqw.QqwUserModifyCallbackResponse;
import com.ptyt.crm.service.api.syncrecord.SyncErrorRecord;
import com.ptyt.crm.service.api.syncrecord.SyncWaitRecord;
import com.ptyt.crm.service.domain.adapter.commons.AuthInfo;
import com.ptyt.crm.service.domain.adapter.commons.Member;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmConfig;
import com.ptyt.crm.service.domain.adapter.heading.crm.HeadingCrmServices;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Coupon;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.HeadingMember;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.base.Score;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponAbortRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.request.CouponAcquireRequest;
import com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse;
import com.ptyt.crm.service.domain.adapter.qqw.QqwServices;
import com.ptyt.crm.service.domain.core.callback.CallbackService;
import com.ptyt.crm.service.domain.core.config.ConfigService;
import com.ptyt.crm.service.domain.core.coupon.CouponService;
import com.ptyt.crm.service.domain.core.job.CrmSyncRecordProcessor;
import com.ptyt.crm.service.domain.core.mapping.MappingService;
import com.ptyt.crm.service.domain.core.syncrecord.SyncRecordService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class QqwCallbackController implements QqwCallbackApi {
  @Autowired
  CallbackService callbackService;

  @Autowired
  ConfigService configService;

  @Autowired
  HeadingCrmServices headingCrmServices;

  @Autowired
  QqwServices qqwServices;

  @Autowired
  CouponService couponService;

  @Autowired
  MappingService mappingService;

  @Override
  public QqwQueryByPhoneCallbackResponse queryByPhone(String tenantId,
      QqwQueryByPhoneRequest request) throws Exception {
    try {
      log.info("查询会员请求：" + JSON.toJSONString(request));

      HeadingCrmConfig config = configService.getConfig(tenantId, HeadingCrmConfig.NAME,
          HeadingCrmConfig.class);
      if (config == null || StringUtils.isBlank(config.getGatewayUrl())) {
        log.error("未配置海鼎授权");
        return QqwQueryByPhoneCallbackResponse.fail("未配置海鼎授权");
      }

      AuthInfo authInfo = buildAuthInfo(config);

      com.ptyt.crm.service.domain.adapter.heading.crm.sdk.response.CrmResponse<Member> headingMemberResponse = headingCrmServices
          .getMemberByMobile(authInfo, request.getPhone());

      if (headingMemberResponse.isSuccess() == false) {
        log.error(headingMemberResponse.getMessage());
        return QqwQueryByPhoneCallbackResponse.fail(headingMemberResponse.getMessage());
      }

      QqwQueryByPhoneCallbackResponse response = new QqwQueryByPhoneCallbackResponse();
      if (headingMemberResponse.getData() == null) {
        log.info("会员：" + request.getPhone() + "线下不存在");
        return response;
      }

      Member member = headingMemberResponse.getData();
      QqwQueryByPhoneCallbackData data = new QqwQueryByPhoneCallbackData();
      data.setOfflineId(member.getMemberId());
      data.setPhone(member.getMobile());
      data.setRegistryTime(new Date());// TODO

      data.setOfflineMasterCardNo(null);
      data.setNickName(member.getName());
      data.setFullName(member.getName());
      data.setSex(null);
      data.setBirthday(member.getBirthday());
      // data.setEmail(member.get);
      // data.setProvince(province);
      // data.setCity(city);
      // data.setCounty(county);
      // data.setAddress(address);
      // data.setLogoUrl(logoUrl);
      response.setData(data);

      // 触发同步历史积分
      notifyInitialQQWScore(tenantId, request.getPhone(), authInfo,
          headingMemberResponse.getData().getMemberId());

      return response;
    } catch (Exception e) {
      log.error("", e);
      return QqwQueryByPhoneCallbackResponse.fail(e.getMessage());
    }
  }

  private void notifyInitialQQWScore(String tenantId, String phone, AuthInfo authInfo,
      String memberId) throws Exception {
    CrmResponse<Score> score = headingCrmServices.getScore(authInfo, memberId);
    if (score.isSuccess() == false) {
      throw new IllegalArgumentException("获取线下积分异常：" + score.getMessage());
    }

    if (score.getData() == null || score.getData().getBalance() == null
        || BigDecimal.ZERO.equals(score.getData().getBalance())) {
      log.info("会员：" + phone + "没有积分，忽略全球蛙初始积分...");
      return;
    }

    SyncWaitRecord record = new SyncWaitRecord();
    record.setType(SyncWaitRecord.TYPE_SCORE);
    record.setBusinessKey(phone);
    JSONObject content = new JSONObject();
    content.put("action", "initial");
    content.put("occur_value",
        score.getData() == null ? BigDecimal.ZERO : score.getData().getBalance());
    record.setContent(JSON.toJSONString(content));
    record.setRemark("全球蛙注册会员，初始化海鼎积分到全球蛙");
    syncRecordService.saveSyncWaitRecord(tenantId, record);
    // 应该优化：方案一：直接返回全球蛙会员，全球蛙直接按照线下会员初始化，方案二：查询当前会员，记录在同步记录里面，初始会员积分应该跟当前查询会员积分保持一致。
    log.info("会员：" + phone + "触发同步历史积分记录："
        + (score.getData() == null ? null : score.getData().getBalance()));
  }

  private AuthInfo buildAuthInfo(HeadingCrmConfig config) {
    AuthInfo authInfo = new AuthInfo();
    authInfo.setTenantId(config.getTenantId());
    authInfo.getConfigs().put("userName", config.getUserName());
    authInfo.getConfigs().put("password", config.getPassword());
    authInfo.getConfigs().put("gatewayUrl", config.getGatewayUrl());
    return authInfo;
  }

  @Override
  public QqwUserCreateCallbackResponse createUser(String tenantId, QqwUserCreateRequest request) {
    try {
      log.info("创建会员请求：" + JSON.toJSONString(request));

      HeadingCrmConfig config = configService.getConfig(tenantId, HeadingCrmConfig.NAME,
          HeadingCrmConfig.class);
      if (config == null || StringUtils.isBlank(config.getGatewayUrl())) {
        log.error("未配置海鼎授权");
        return QqwUserCreateCallbackResponse.fail("未配置海鼎授权");
      }

      AuthInfo authInfo = buildAuthInfo(config);

      Member member = new Member();
      member.setMobile(request.getPhone());
      member.setName(request.getFullName() == null ? request.getNickName() : request.getFullName());
      member.setRegisterTime(request.getRegistryTime());
      if ("-1".equals(request.getSex())) {
        member.setGender("未知");
      } else if ("0".equals(request.getSex())) {
        member.setGender("女");
      } else if ("1".equals(request.getSex())) {
        member.setGender("男");
      }
      member.setBirthday(request.getBirthday());
      CrmResponse<HeadingMember> headingMemberRes = headingCrmServices
          .registerMemberByMobile(authInfo, member);

      if (headingMemberRes.isSuccess() == false) {
        log.error(headingMemberRes.getMessage());
        return QqwUserCreateCallbackResponse.fail(headingMemberRes.getMessage());
      }

      QqwUserCreateCallbackResponse response = new QqwUserCreateCallbackResponse();
      if (headingMemberRes.getData() == null) {
        return response;
      }

      HeadingMember headingMember = headingMemberRes.getData();
      QqwUserCreateCallbackData data = new QqwUserCreateCallbackData();
      data.setOfflineId(headingMember.getMember_id());
      response.setData(data);

      // 触发同步历史积分
      notifyInitialQQWScore(tenantId, request.getPhone(), authInfo, headingMember.getMember_id());

      return response;
    } catch (Exception e) {
      log.error("", e);
      return QqwUserCreateCallbackResponse.fail(e.getMessage());
    }
  }

  @Override
  public QqwUserModifyCallbackResponse modifyUser(String tenantId, QqwUserModRequest request)
      throws Exception {
    log.info("修改会员请求：" + JSON.toJSONString(request));

    try {
      HeadingCrmConfig config = configService.getConfig(tenantId, HeadingCrmConfig.NAME,
          HeadingCrmConfig.class);
      if (config == null || StringUtils.isBlank(config.getGatewayUrl())) {
        log.error("未配置海鼎授权");
        return QqwUserModifyCallbackResponse.fail("未配置海鼎授权");
      }

      AuthInfo authInfo = buildAuthInfo(config);

      Member member = new Member();
      member.setMemberId(request.getOfflineId());
      member.setMobile(request.getPhone());
      member.setName(request.getFullName() == null ? request.getNickName() : request.getFullName());
      if ("-1".equals(request.getSex())) {
        member.setGender("未知");
      } else if ("0".equals(request.getSex())) {
        member.setGender("女");
      } else if ("1".equals(request.getSex())) {
        member.setGender("男");
      }
      member.setBirthday(request.getBirthday());
      member.setEmail(request.getEmail());
      CrmResponse headingMemberRes = headingCrmServices.modifyMember(authInfo, member, "全球蛙");

      if (headingMemberRes.isSuccess() == false) {
        log.error(headingMemberRes.getMessage());
        return QqwUserModifyCallbackResponse.fail(headingMemberRes.getMessage());
      }

      QqwUserModifyCallbackResponse response = new QqwUserModifyCallbackResponse();
      if (headingMemberRes.getData() == null) {
        return response;
      }

      return response;
    } catch (Exception e) {
      log.error("", e);
      return QqwUserModifyCallbackResponse.fail(e.getMessage());
    }

  }

  @Override
  public QqwUserModifyCallbackResponse changePhone(String tenantId,
      QqwUserChangePhoneRequest request) throws Exception {

    return QqwUserModifyCallbackResponse.fail("不支持修改手机号");
    // try {
    // HeadingCrmConfig config = configService.getConfig(tenantId,
    // HeadingCrmConfig.NAME,
    // HeadingCrmConfig.class);
    // if (config == null || StringUtils.isBlank(config.getGatewayUrl())) {
    // log.error("未配置海鼎授权");
    // return QqwUserModifyCallbackResponse.fail("未配置海鼎授权");
    // }
    //
    // AuthInfo authInfo = buildAuthInfo(config);
    //
    // CrmResponse<Member> memberReponse =
    // headingCrmServices.getMemberId(authInfo,
    // request.getOfflineId());
    //
    // if (memberReponse.isSuccess() == false) {
    // return QqwUserModifyCallbackResponse.fail(memberReponse.getMessage());
    // }
    //
    // Member headingMember = memberReponse.getData();
    // if (headingMember == null) {
    // return QqwUserModifyCallbackResponse.fail("海鼎不存在此会员，不支持修改");
    // }
    //
    // QqwUserModifyCallbackResponse response = new
    // QqwUserModifyCallbackResponse();
    // if (ObjectUtils.equals(headingMember.getMobile(), request.getNewPhone()))
    // {
    // log.info("会员：" + request.getOfflineId() + "手机号无变化");
    // return response;
    // }
    //
    // Member member = new Member();
    // member.setMemberId(request.getOfflineId());
    // member.setMobile(request.getNewPhone());
    // CrmResponse headingMemberRes = headingCrmServices.modifyMember(authInfo,
    // member, "全球蛙");
    //
    // if (headingMemberRes.isSuccess() == false) {
    // log.error(headingMemberRes.getMessage());
    // return QqwUserModifyCallbackResponse.fail(headingMemberRes.getMessage());
    // }
    //
    // return response;
    // } catch (Exception e) {
    // log.error("", e);
    // return QqwUserModifyCallbackResponse.fail(e.getMessage());
    // }

  }

  @Autowired
  SyncRecordService syncRecordService;

  @Autowired
  CrmSyncRecordProcessor crmSyncRecordProcessor;

  @Override
  public QqwCallbackResponse acquireCoupon(String tenantId, QqwUserAcquireCouponRequest request)
      throws Exception {
    log.info("接收到回调信息：" + JSON.toJSONString(request));

    // 请求海鼎注册
    QqwCallbackResponse response = acquireHDCoupon(tenantId, request);

    // 注册失败记录
    saveSyncRecord(tenantId, request, response);

    return response;

  }

  private void saveSyncRecord(String tenantId, QqwUserAcquireCouponRequest request,
      QqwCallbackResponse response) {
    try {
      String type = "领券";
      for (String counponCode : request.getCouponDetailNos()) {
        SyncErrorRecord errorRecord = syncRecordService.getErrorRecordByKey(tenantId, type,
            counponCode);
        if ("0".equals(response.getCode()) == false) { // 失败
          if (errorRecord == null) {
            errorRecord = new SyncErrorRecord();
            errorRecord.setType(type);
            errorRecord.setBusinessKey(counponCode);

            errorRecord.setContent(JSON.toJSONString(request));
            errorRecord.setMessage(response.getMessage());
            errorRecord.setRetries(1);
            errorRecord.setTime(new Date());
          } else {
            errorRecord.setContent(JSON.toJSONString(request));
            errorRecord.setMessage(response.getMessage());
            errorRecord.setRetries(errorRecord.getRetries() + 1);
            errorRecord.setTime(new Date());
          }
          syncRecordService.saveSyncErrorRecord(tenantId, errorRecord);
        } else { // 成功，删除错误记录
          if (errorRecord != null) {
            log.info("删除错误记录：" + JSON.toJSONString(errorRecord));
            syncRecordService.deleteErrorRecord(tenantId, type, Arrays.asList(errorRecord));
          }
        }
      }

    } catch (Exception e) {
      log.error("领券失败记录存储异常：" + e.getMessage(), e);
    }
  }

  private QqwCallbackResponse acquireHDCoupon(String tenantId,
      QqwUserAcquireCouponRequest request) {
    QqwCallbackResponse response = new QqwCallbackResponse<>();

    String phone = request.getPhone();
    if (phone == null) {
      response.setCode("-1");
      response.setMessage("手机号不能为空");
      log.error(response.getMessage());
      return response;
    }

    try {
      HeadingCrmConfig config = configService.getConfig(tenantId, HeadingCrmConfig.NAME,
          HeadingCrmConfig.class);
      if (config == null || StringUtils.isBlank(config.getGatewayUrl())) {
        response.setCode("-1");
        response.setMessage("未配置海鼎授权");
        log.error(response.getMessage());
        return response;
      }

      AuthInfo authInfo = buildAuthInfo(config);

      CrmResponse<Member> member = headingCrmServices.getMemberByMobile(authInfo, phone);
      if (member.isSuccess() == false) {
        log.error(member.getMessage());
        response.setCode("-1");
        response.setMessage("查询会员失败");
        return response;
      }

      if (member.getData() == null) {
        response.setCode("-1");
        response.setMessage("会员不存在！");
        log.error(response.getMessage());
        return response;
      }

      Mapping mapping = mappingService.getSrcCode(tenantId, "coupon.activity", "QQW",
          request.getCouponNo());
      String activity_id = (mapping == null) ? null : mapping.getTargetId();
      if (activity_id == null) {
        response.setCode("-1");
        response.setMessage("活动：" + request.getCouponNo() + "映射关系未配置，联系运营人员处理！");
        log.error(response.getMessage());
        return response;
      }

      Map<String, String> counponMap = new HashMap<String, String>();
      for (String counponCode : request.getCouponDetailNos()) {
        CouponMapping couponMapping = couponService.getSrcCode(tenantId, "QQW", counponCode);
        if (couponMapping != null && couponMapping.getTargetCode() != null) {
          log.info("券：" + counponCode + "已经领取过，忽略本次领取...");
          continue;
        }
        CouponAcquireRequest couponAcquireRequest = new CouponAcquireRequest();
        couponAcquireRequest.setActivity_id(activity_id);
        couponAcquireRequest.setMember_id(member.getData().getMemberId());
        couponAcquireRequest.setMobile(phone);
        couponAcquireRequest.setQuantity(BigDecimal.ONE);
        couponAcquireRequest.setStore_id(null);
        couponAcquireRequest.setTran_id("QQW" + counponCode);
        couponAcquireRequest.setUnion_id("QQW" + counponCode);
        CrmResponse<Coupon> counpon = headingCrmServices.acquireCoupon(authInfo,
            couponAcquireRequest);
        if (counpon.isSuccess() == false) {
          response.setCode("-1");
          response.setMessage(counpon.getMessage());
          return response;
        }
        counponMap.put(counponCode, counpon.getData().getCode());
      }

      // 记录券账户映射关系
      List<CouponMapping> couponMappings = new ArrayList<CouponMapping>();
      for (String counponCode : counponMap.keySet()) {
        CouponMapping couponMapping = new CouponMapping();
        couponMapping.setPhone(phone);
        couponMapping.setSourcePlatformId("QQW");
        couponMapping.setSourceTplId(request.getCouponNo());
        couponMapping.setSourceCode(counponCode);
        couponMapping.setTargetPlatformId("HEADING");
        couponMapping.setTargetTplId(activity_id);
        couponMapping.setTargetCode(counponMap.get(counponCode));
        couponMappings.add(couponMapping);
      }
      if (couponMappings.isEmpty() == false)
        couponService.save(tenantId, couponMappings);
      return response;
    } catch (Exception e) {
      log.error("", e);
      response.setCode("-1");
      response.setMessage(e.getMessage());
      return response;
    }
  }

  @Override
  public QqwCallbackResponse abortCoupon(String tenantId, QqwAbortCouponRequest request)
      throws Exception {
    log.info("接收到回调信息：" + JSON.toJSONString(request));
    QqwCallbackResponse response = new QqwCallbackResponse<>();
    try {
      HeadingCrmConfig config = configService.getConfig(tenantId, HeadingCrmConfig.NAME,
          HeadingCrmConfig.class);
      if (config == null || StringUtils.isBlank(config.getGatewayUrl())) {
        log.error("未配置海鼎授权");
        response.setCode("-1");
        response.setMessage("未配置海鼎授权");
        return response;
      }

      AuthInfo authInfo = buildAuthInfo(config);
      Mapping mapping = mappingService.getSrcCode(tenantId, "coupon.activity", "QQW",
          request.getCouponNo());
      String activity_id = mapping == null ? null : mapping.getTargetId();
      if (activity_id == null) {
        log.info("没有活动映射关系，作废成功");
        return response;
      }
      // FIXME 作废已经领取的券，要调整成异步的
      List<CouponMapping> coupons = couponService.getsByTplId(tenantId, "QQW",
          request.getCouponNo());
      for (CouponMapping coupon : coupons) {
        if ("已核銷".equals(coupon.getTargetStatus())) {
          log.info("券：" + coupon.getTargetCode() + "已核销，不支持作废...");
          continue;
        }
        CouponAbortRequest request2 = new CouponAbortRequest();
        request2.setCoupon_code(coupon.getTargetCode());
        CrmResponse<Void> crmResponse = headingCrmServices.abortCoupon(authInfo, request2);
        if (crmResponse.isSuccess() == false) {
          log.error(crmResponse.getMessage());
          response.setCode("-1");
          response.setMessage(crmResponse.getMessage());
          // TODO 记录错误记录
        }
      }

      return response;
    } catch (Exception e) {
      log.error("", e);
      response.setCode("-1");
      response.setMessage(e.getMessage());
      return response;
    }

  }

}
